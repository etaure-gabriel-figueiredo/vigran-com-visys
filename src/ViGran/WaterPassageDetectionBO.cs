﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPD_V1;

namespace BO
{
    class WaterPassageDetectionBO
    {
        private ImageProcessing imageProc;
        private double[] histFG;
        private double rawWpdIndex;
        private double expValue;
        private double indexConfidence;
        private double wpdIndex;

        public WaterPassageDetectionBO(ImageProcessing imageProc)
        {
            this.imageProc = imageProc;
        }

        public double calculateRawWpdIndex(Channel sensorChannel)
        {
            imageProc.histogram(ImageProcessing.Histogram.NORMAL, sensorChannel, out histFG);

            int startIntensity = 0;
            int endIntensity = imageProc.pixelDepth - 1;

            expValue = imageProc.calculateExpectedValue(Channel.GREEN, histFG, startIntensity, endIntensity);
            rawWpdIndex = (1 - expValue / (imageProc.pixelDepth - 1)) * 100;

            return rawWpdIndex;
        }

        public double calculateRawWpdIndexRelative(Channel sensorChannel)
        {
            imageProc.histogram(ImageProcessing.Histogram.NORMAL, sensorChannel, out histFG);

            int startIntensity = 0;
            int endIntensity = imageProc.pixelDepth - 1;

            expValue = imageProc.calculateExpectedValue(Channel.GREEN, histFG, startIntensity, endIntensity);
            rawWpdIndex = 100 * (imageProc.getMaximumChannelIntensity(Channel.GREEN) - expValue) / (imageProc.getMaximumChannelIntensity(Channel.GREEN) - imageProc.getMinimumChannelIntensity(Channel.GREEN));

            return rawWpdIndex;
        }

        private double getIndexConfidence(double minimumSpeed, double realSpeed)
        {
           indexConfidence = realSpeed / minimumSpeed;

           indexConfidence = indexConfidence > 1 ? 1 : indexConfidence;

           return indexConfidence;
        }

        public double calculateWpdIndex(Channel sensorChannel, double minimumSpeed, double realSpeed)
        {
            wpdIndex =  getIndexConfidence(minimumSpeed, realSpeed)*calculateRawWpdIndex(sensorChannel);
           
            return wpdIndex;
        }

        public double calculateWpdIndex(Channel sensorChannel)
        {
            wpdIndex = calculateRawWpdIndexRelative(sensorChannel);

            return wpdIndex;
        }
    }
}
