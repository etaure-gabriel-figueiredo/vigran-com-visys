﻿/*=============================================================================
  Copyright (C) 2012 Allied Vision Technologies.  All Rights Reserved.

  Redistribution of this file, in original or modified form, without
  prior written consent of Allied Vision Technologies is prohibited.

-------------------------------------------------------------------------------

  File:        MainForm.Designer.cs

  Description: Forms class for the GUI of the AsynchronousGrab example of
               VimbaNET.

-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

namespace WPD_V1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.chartWpdFG = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.picCannyEdge = new System.Windows.Forms.PictureBox();
            this.pictureBoxFG = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtProcessStatus = new System.Windows.Forms.TextBox();
            this.btnSendOfflineImage = new System.Windows.Forms.Button();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnChangeParameters = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtActDateTime = new System.Windows.Forms.TextBox();
            this.txtOPCGroupStatus = new System.Windows.Forms.TextBox();
            this.txtOPCServerStatus = new System.Windows.Forms.TextBox();
            this.txtCameraLinkStatus = new System.Windows.Forms.TextBox();
            this.txtFPS = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartWpdFG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCannyEdge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFG)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chartWpdFG
            // 
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.AxisX.Interval = 1D;
            chartArea1.AxisX.LabelAutoFitStyle = ((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles)(((((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.IncreaseFont | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.DecreaseFont) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep30) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep45) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep90)));
            chartArea1.AxisX.LabelStyle.Format = "#";
            chartArea1.AxisX.MajorGrid.IntervalOffset = 0D;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisX.Title = "Particle Size (mm)";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisX2.LabelStyle.Format = "0.000";
            chartArea1.AxisX2.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX2.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisX2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisX2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisX2.MinorGrid.Enabled = true;
            chartArea1.AxisX2.MinorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisX2.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisX2.MinorTickMark.Enabled = true;
            chartArea1.AxisY.LabelStyle.Format = "0.00";
            chartArea1.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY.Maximum = 100D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY.MinorGrid.Enabled = true;
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY.Title = "Ocurrence(%)";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY2.LabelStyle.Format = "0.0";
            chartArea1.AxisY2.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY2.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY2.MinorGrid.Enabled = true;
            chartArea1.AxisY2.MinorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY2.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            chartArea1.Name = "ChartArea1";
            chartArea2.AxisX.Interval = 1D;
            chartArea2.AxisX.LabelAutoFitStyle = ((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles)(((((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.IncreaseFont | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.DecreaseFont) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep30) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep45) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep90)));
            chartArea2.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisX.MajorTickMark.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisX.Title = "Particle Size (mm)";
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.AxisY.LabelStyle.Format = "0.00";
            chartArea2.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisY.Maximum = 100D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.AxisY.MinorGrid.Enabled = true;
            chartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisY.Title = "Ocurrence(%)";
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.Name = "ChartArea2";
            this.chartWpdFG.ChartAreas.Add(chartArea1);
            this.chartWpdFG.ChartAreas.Add(chartArea2);
            this.chartWpdFG.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.chartWpdFG.Location = new System.Drawing.Point(3, 306);
            this.chartWpdFG.Margin = new System.Windows.Forms.Padding(0);
            this.chartWpdFG.Name = "chartWpdFG";
            series1.ChartArea = "ChartArea1";
            series1.IsVisibleInLegend = false;
            series1.Label = "#VAL{N2}";
            series1.Name = "BarChart";
            series2.ChartArea = "ChartArea2";
            series2.Label = "#VAL{N2}";
            series2.Legend = "Legend1";
            series2.Name = "BarChart2";
            this.chartWpdFG.Series.Add(series1);
            this.chartWpdFG.Series.Add(series2);
            this.chartWpdFG.Size = new System.Drawing.Size(1056, 375);
            this.chartWpdFG.TabIndex = 24;
            title1.DockedToChartArea = "ChartArea1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            title1.IsDockedInsideChartArea = false;
            title1.Name = "Title1";
            title1.Text = "Horizontal Size Distribution";
            title2.DockedToChartArea = "ChartArea2";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            title2.IsDockedInsideChartArea = false;
            title2.Name = "Title2";
            title2.Text = "Vertical Size Distribution";
            this.chartWpdFG.Titles.Add(title1);
            this.chartWpdFG.Titles.Add(title2);
            // 
            // picCannyEdge
            // 
            this.picCannyEdge.Location = new System.Drawing.Point(531, 6);
            this.picCannyEdge.Name = "picCannyEdge";
            this.picCannyEdge.Size = new System.Drawing.Size(528, 297);
            this.picCannyEdge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCannyEdge.TabIndex = 22;
            this.picCannyEdge.TabStop = false;
            // 
            // pictureBoxFG
            // 
            this.pictureBoxFG.Location = new System.Drawing.Point(3, 6);
            this.pictureBoxFG.Name = "pictureBoxFG";
            this.pictureBoxFG.Size = new System.Drawing.Size(528, 297);
            this.pictureBoxFG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFG.TabIndex = 21;
            this.pictureBoxFG.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txtProcessStatus);
            this.groupBox2.Controls.Add(this.btnSendOfflineImage);
            this.groupBox2.Controls.Add(this.btnStartStop);
            this.groupBox2.Controls.Add(this.btnChangeParameters);
            this.groupBox2.Location = new System.Drawing.Point(3, 684);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1056, 70);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operation";
            // 
            // txtProcessStatus
            // 
            this.txtProcessStatus.BackColor = System.Drawing.SystemColors.Info;
            this.txtProcessStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcessStatus.Location = new System.Drawing.Point(6, 19);
            this.txtProcessStatus.Name = "txtProcessStatus";
            this.txtProcessStatus.Size = new System.Drawing.Size(1045, 23);
            this.txtProcessStatus.TabIndex = 28;
            this.txtProcessStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSendOfflineImage
            // 
            this.btnSendOfflineImage.Enabled = false;
            this.btnSendOfflineImage.Location = new System.Drawing.Point(90, 42);
            this.btnSendOfflineImage.Name = "btnSendOfflineImage";
            this.btnSendOfflineImage.Size = new System.Drawing.Size(81, 23);
            this.btnSendOfflineImage.TabIndex = 27;
            this.btnSendOfflineImage.Text = "Nova Imagem";
            this.btnSendOfflineImage.UseVisualStyleBackColor = true;
            this.btnSendOfflineImage.Click += new System.EventHandler(this.btnSendOfflineImage_Click);
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(90, 42);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(82, 23);
            this.btnStartStop.TabIndex = 26;
            this.btnStartStop.Text = "Start";
            this.btnStartStop.UseVisualStyleBackColor = true;
            // 
            // btnChangeParameters
            // 
            this.btnChangeParameters.Location = new System.Drawing.Point(6, 42);
            this.btnChangeParameters.Name = "btnChangeParameters";
            this.btnChangeParameters.Size = new System.Drawing.Size(78, 23);
            this.btnChangeParameters.TabIndex = 22;
            this.btnChangeParameters.Text = "Parameters";
            this.btnChangeParameters.UseVisualStyleBackColor = true;
            this.btnChangeParameters.Click += new System.EventHandler(this.btnChangeParameters_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.txtActDateTime);
            this.groupBox1.Controls.Add(this.txtOPCGroupStatus);
            this.groupBox1.Controls.Add(this.txtOPCServerStatus);
            this.groupBox1.Controls.Add(this.txtCameraLinkStatus);
            this.groupBox1.Controls.Add(this.txtFPS);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupBox1.Location = new System.Drawing.Point(3, 755);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1056, 64);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "System Information";
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(792, 38);
            this.txtUser.Multiline = true;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUser.Size = new System.Drawing.Size(258, 20);
            this.txtUser.TabIndex = 21;
            this.txtUser.Text = "Usuário: Especialista";
            this.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtActDateTime
            // 
            this.txtActDateTime.BackColor = System.Drawing.SystemColors.Info;
            this.txtActDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActDateTime.Location = new System.Drawing.Point(270, 38);
            this.txtActDateTime.Multiline = true;
            this.txtActDateTime.Name = "txtActDateTime";
            this.txtActDateTime.ReadOnly = true;
            this.txtActDateTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtActDateTime.Size = new System.Drawing.Size(516, 20);
            this.txtActDateTime.TabIndex = 20;
            this.txtActDateTime.Text = "Data e Hora:";
            this.txtActDateTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtOPCGroupStatus
            // 
            this.txtOPCGroupStatus.BackColor = System.Drawing.Color.LimeGreen;
            this.txtOPCGroupStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOPCGroupStatus.ForeColor = System.Drawing.Color.Black;
            this.txtOPCGroupStatus.Location = new System.Drawing.Point(792, 15);
            this.txtOPCGroupStatus.Name = "txtOPCGroupStatus";
            this.txtOPCGroupStatus.ReadOnly = true;
            this.txtOPCGroupStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtOPCGroupStatus.Size = new System.Drawing.Size(258, 23);
            this.txtOPCGroupStatus.TabIndex = 19;
            this.txtOPCGroupStatus.Text = "Grupo OPC: OK";
            this.txtOPCGroupStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtOPCServerStatus
            // 
            this.txtOPCServerStatus.BackColor = System.Drawing.Color.LimeGreen;
            this.txtOPCServerStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOPCServerStatus.ForeColor = System.Drawing.Color.Black;
            this.txtOPCServerStatus.Location = new System.Drawing.Point(270, 15);
            this.txtOPCServerStatus.Name = "txtOPCServerStatus";
            this.txtOPCServerStatus.ReadOnly = true;
            this.txtOPCServerStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtOPCServerStatus.Size = new System.Drawing.Size(516, 23);
            this.txtOPCServerStatus.TabIndex = 18;
            this.txtOPCServerStatus.Text = "Servidor OPC ...: OK";
            this.txtOPCServerStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCameraLinkStatus
            // 
            this.txtCameraLinkStatus.BackColor = System.Drawing.Color.LimeGreen;
            this.txtCameraLinkStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCameraLinkStatus.ForeColor = System.Drawing.Color.Black;
            this.txtCameraLinkStatus.Location = new System.Drawing.Point(6, 15);
            this.txtCameraLinkStatus.Name = "txtCameraLinkStatus";
            this.txtCameraLinkStatus.ReadOnly = true;
            this.txtCameraLinkStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCameraLinkStatus.Size = new System.Drawing.Size(258, 23);
            this.txtCameraLinkStatus.TabIndex = 17;
            this.txtCameraLinkStatus.Text = "Camera: OK";
            this.txtCameraLinkStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFPS
            // 
            this.txtFPS.BackColor = System.Drawing.SystemColors.Info;
            this.txtFPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFPS.Location = new System.Drawing.Point(6, 38);
            this.txtFPS.Multiline = true;
            this.txtFPS.Name = "txtFPS";
            this.txtFPS.ReadOnly = true;
            this.txtFPS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFPS.Size = new System.Drawing.Size(258, 20);
            this.txtFPS.TabIndex = 15;
            this.txtFPS.Text = "FPPS: 30";
            this.txtFPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(177, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 23);
            this.button1.TabIndex = 29;
            this.button1.Text = "Video";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(1148, 822);
            this.Controls.Add(this.chartWpdFG);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.picCannyEdge);
            this.Controls.Add(this.pictureBoxFG);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 350);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VEGA/ENC/ViWPD (Water Passage Detection)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartWpdFG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCannyEdge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFG)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataVisualization.Charting.Chart chartWpdFG;
        private System.Windows.Forms.PictureBox picCannyEdge;
        private System.Windows.Forms.PictureBox pictureBoxFG;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtProcessStatus;
        private System.Windows.Forms.Button btnSendOfflineImage;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnChangeParameters;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtActDateTime;
        private System.Windows.Forms.TextBox txtOPCGroupStatus;
        private System.Windows.Forms.TextBox txtOPCServerStatus;
        private System.Windows.Forms.TextBox txtCameraLinkStatus;
        private System.Windows.Forms.TextBox txtFPS;
        private System.Windows.Forms.Button button1;
    }
}

