﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace WPD_V1
{
    /*Scoria information*/
    public class Slag
    {
        const Double initialIndex = 1;
        Double indexInstantaneous;
        Double index;
        Double realInitialIndex;

        Double thresholdHigh = 0.991;

        public Double[,] calculationImage;
        public Bitmap outputImage;

        public ErrorCode error;

        public List<SlagIndex> instantaneousSlagRegistry;
        public List<SlagIndex> slagRegistry;
        public SlagIndex slagFinal;

        public Slag(ref Bitmap img)
        {
            /*O valor 2 indica que nao foi efetuada nenhum calculo do indice de escoria*/
            this.indexInstantaneous = initialIndex;
            this.index = initialIndex;
            this.realInitialIndex = initialIndex;
            this.slagFinal = new SlagIndex(1, DateTime.Now);

            this.outputImage = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
            this.calculationImage = new Double[img.Width, img.Height];

            error = ErrorCode.NO_ERROR;

            this.instantaneousSlagRegistry = new List<SlagIndex>();
            this.instantaneousSlagRegistry.Add(slagFinal); /*Garante o primeiro valor na lista*/

            this.slagRegistry = new List<SlagIndex>(); /*Garante o primeiro valor na lista*/
            this.slagRegistry.Add(slagFinal);

        }

        public void reset()
        {
            this.indexInstantaneous = initialIndex;
            this.index = initialIndex;
            this.slagFinal = new SlagIndex(1, DateTime.Now);

            error = ErrorCode.NO_ERROR;

            this.instantaneousSlagRegistry.Clear();            
            this.slagRegistry.Clear();

            /*Ao resetar garante um valor inicial*/
            SlagIndex slagAux = new SlagIndex(1, DateTime.Now);
            this.instantaneousSlagRegistry.Add(slagAux);
            this.slagRegistry.Add(slagAux);
        }

        public Double InstantaneousRegistryAVG( int slagSampleNumber )
        {
            int i;
            Double average = 0;

            if (instantaneousSlagRegistry.Count() >= slagSampleNumber)
            {
                /*Calcula a media com as ultimas scoria_sample_number amostras*/
                for ( i = 1; i <= slagSampleNumber; i++ )
                {
                    average += instantaneousSlagRegistry.ElementAt( instantaneousSlagRegistry.Count() - i ).value;
                }

                return average / slagSampleNumber;
            }
            else if (instantaneousSlagRegistry.Count() > 0)
            {
                return instantaneousSlagRegistry.ElementAt(instantaneousSlagRegistry.Count()-1).value;
            }
            else
            {
                return this.index;
            }
                
        }

        private void refreshSlagIndex( )
        {
            /*Desconsidera os ruidos na medicao do índice de escoria
              e nao permite que ele aumente*/

            /*ATUALIZAÇÃO DO ÍNDICE DE ESCÓRIA FINAL*/
            //Double avgIndex = this.InstantaneousRegistryAVG(10);
            Double avgIndex = this.InstantaneousRegistryAVG(5); //testes pré-implantacao 02/12/15
            
            if ((error == ErrorCode.NO_ERROR) && (avgIndex < this.index))
            {
                this.index = avgIndex;
                this.slagFinal = new SlagIndex(avgIndex, this.instantaneousSlagRegistry.Last().dateTime);
               
            }

            /*ATUALIZAÇÃO DO ÍNDICE DE ESCÓRIA FILTRADO PELO MÍNIMO*/
            if (this.slagRegistry.Count > 0)
            {
                if ((error == ErrorCode.NO_ERROR) && (this.instantaneousSlagRegistry.Last().value < this.slagRegistry.Last().value))
                {
                    this.slagRegistry.Add(this.instantaneousSlagRegistry.Last());
                }
            }
        }

        public void setIntantaneousCalculatedVolume(Double calcIndex, DateTime date_time)
        {
            if (calcIndex < this.thresholdHigh)
            {
                this.indexInstantaneous = calcIndex;

                SlagIndex curr = new SlagIndex(calcIndex, date_time);
                instantaneousSlagRegistry.Add(curr);

                refreshSlagIndex();
            }
        }

        /*Retorna o ultimo elemento*/
        public double getIntantaneousIndex()
        {
            return this.indexInstantaneous;
        }

        public Double getIndex()
        {
            //refreshSlagIndex();

            return this.index;
        }

        public void setRealInitialIndex()
        {
            if (this.slagRegistry.Count > 0 )
            {
                this.realInitialIndex = this.slagRegistry.Last().value;
            }
            
        }

        public Double getRealInitialIndex()
        {
            return this.realInitialIndex;
        }
    }
}
