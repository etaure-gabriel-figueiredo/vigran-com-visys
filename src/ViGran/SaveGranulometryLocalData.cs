﻿using System.Xml;
using System;
using System.Collections.Generic;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml.Linq;
using System.Linq;
using WPD_V1;
using Xml;
using System.Text;

namespace BasicUtil
{
    public class SaveGranulometryLocalData
    {
        string systemName;
        private string savePath;
        
        private XmlFile docFiltered;
        private XmlFile docDaily;
        private XmlFile doc;
        private XmlFile docFilteredNotAdjusted;
        private XmlFile docDailyNotAdjusted;
        private XmlFile docNotAdjusted;
        private HandleXml temporaryDataXml;

        private DateTime? dateTimeOfLastBatch = null;
        public List<double> GrainSizeRangeList { get; set; }

        public double[] PartialHorizontalMeanOfTheDayAdjusted { get; set; }
        public double[] PartialVerticalMeanOfTheDayAdjusted { get; set; }
        public double[] PartialHorizontalMeanOfTheDayNotAdjusted { get; set; }
        public double[] PartialVerticalMeanOfTheDayNotAdjusted { get; set; }
        public int MeanOfTheDayCounter { get; set; } = 0;

        public SaveGranulometryLocalData(string savePath, string adjustedFolderName, string notAdjustedFolderName, string systemName, List<double> grainSizeRangeList)
        {
            this.systemName = systemName;
            this.savePath = savePath;

            DateTime now = DateTime.Now;
            doc = new XmlFile(savePath + @"\" + adjustedFolderName + @"\", "RAW", Periodicity.Daily, now);
            docFiltered = new XmlFile(savePath + @"\" + adjustedFolderName + @"\", "BATCH", Periodicity.Daily, now);
            docDaily = new XmlFile(savePath + @"\" + adjustedFolderName + @"\", "DAILY", Periodicity.Monthly, now);
            docNotAdjusted = new XmlFile(savePath + @"\" + notAdjustedFolderName + @"\", "RAW", Periodicity.Daily, now);
            docFilteredNotAdjusted = new XmlFile(savePath + @"\" + notAdjustedFolderName + @"\", "BATCH", Periodicity.Daily, now);
            docDailyNotAdjusted = new XmlFile(savePath + @"\" + notAdjustedFolderName + @"\", "DAILY", Periodicity.Monthly, now);

            GrainSizeRangeList = grainSizeRangeList;
            PartialHorizontalMeanOfTheDayAdjusted = new double[GrainSizeRangeList.Count];
            PartialVerticalMeanOfTheDayAdjusted = new double[GrainSizeRangeList.Count];
            PartialHorizontalMeanOfTheDayNotAdjusted = new double[GrainSizeRangeList.Count];
            PartialVerticalMeanOfTheDayNotAdjusted = new double[GrainSizeRangeList.Count];

            temporaryDataXml = new HandleXml(this.savePath, "temporaryData");
            temporaryDataXml.LoadOptionalFile();
            if (temporaryDataXml.doc.HasChildNodes)
            {
                MeanOfTheDayCounter = int.Parse(temporaryDataXml.getNodeValue("meanOfTheDayCounter"));
                DateTime d;
                dateTimeOfLastBatch = DateTime.TryParse(temporaryDataXml.getNodeValue("dateTimeOfLastBatch"), out d) ? (DateTime?)d : null;
                PartialHorizontalMeanOfTheDayAdjusted = Array.ConvertAll(temporaryDataXml.getNodeValues("partialHorizontalMeanOfTheDayAdjusted"), delegate (string s) { return double.Parse(s); });
                PartialVerticalMeanOfTheDayAdjusted = Array.ConvertAll(temporaryDataXml.getNodeValues("partialVerticalMeanOfTheDayAdjusted"), delegate (string s) { return double.Parse(s); });
                PartialHorizontalMeanOfTheDayNotAdjusted = Array.ConvertAll(temporaryDataXml.getNodeValues("partialHorizontalMeanOfTheDayNotAdjusted"), delegate (string s) { return double.Parse(s); });
                PartialVerticalMeanOfTheDayNotAdjusted = Array.ConvertAll(temporaryDataXml.getNodeValues("partialVerticalMeanOfTheDayNotAdjusted"), delegate (string s) { return double.Parse(s); });
            }
            else
            {
                string popupMessage = "Informações do processo salvas em:  " + this.savePath;
                ShowMsg(popupMessage);

                XmlNode docNode = temporaryDataXml.doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                temporaryDataXml.doc.AppendChild(docNode);

                XmlNode rootNode = temporaryDataXml.doc.CreateElement("tempData");

                XmlNode meanOfTheDayCounterNode = temporaryDataXml.doc.CreateElement("meanOfTheDayCounter");
                meanOfTheDayCounterNode.InnerText = "0";
                rootNode.AppendChild(meanOfTheDayCounterNode);

                XmlNode dateTimeOfLastBatchNode = temporaryDataXml.doc.CreateElement("dateTimeOfLastBatch");
                dateTimeOfLastBatchNode.InnerText = "-1";
                rootNode.AppendChild(dateTimeOfLastBatchNode);

                temporaryDataXml.doc.AppendChild(rootNode);
            }
        }

        public void AddBatchInfoEvaluation(double[] meanHorizontalSizeDistributionAdjusted, double[] meanVerticalSizeDistributionAdjusted, double[] meanHorizontalSizeDistributionNotAdjusted, double[] meanVerticalSizeDistributionNotAdjusted, DateTime batchStart, DateTime batchEnd)
        {
            /*Adiciona o registro do encruamento ajustado*/
            List<double[]> listHorizontalSizeDistribution = new List<double[]>();
            List<double[]> listVerticalSizeDistribution = new List<double[]>();
            listHorizontalSizeDistribution.Add(meanHorizontalSizeDistributionAdjusted);
            listVerticalSizeDistribution.Add(meanVerticalSizeDistributionAdjusted);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("batchInfo");
                writer.WriteAttributeString("batchStart", batchStart.ToString("dd/MM/yyyy HH:mm:ss"));
                writer.WriteAttributeString("batchEnd", batchEnd.ToString("dd/MM/yyyy HH:mm:ss"));

                AppendDistributionNodeFaixas(writer, null, listHorizontalSizeDistribution, listVerticalSizeDistribution);
                writer.WriteEndElement();
            }
            docFiltered.AppendNode(sb.ToString(), batchEnd);

            /*Adiciona o registro do encruamento não ajustado*/
            listHorizontalSizeDistribution = new List<double[]>();
            listVerticalSizeDistribution = new List<double[]>();
            listHorizontalSizeDistribution.Add(meanHorizontalSizeDistributionNotAdjusted);
            listVerticalSizeDistribution.Add(meanVerticalSizeDistributionNotAdjusted);
            sb.Clear();
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("batchInfo");
                writer.WriteAttributeString("batchStart", batchStart.ToString("dd/MM/yyyy HH:mm:ss"));
                writer.WriteAttributeString("batchEnd", batchEnd.ToString("dd/MM/yyyy HH:mm:ss"));

                AppendDistributionNodeFaixas(writer, null, listHorizontalSizeDistribution, listVerticalSizeDistribution);
                writer.WriteEndElement();
            }
            docFilteredNotAdjusted.AppendNode(sb.ToString(), batchEnd);
        }

        public void AddBatchInfoInstantaneous(List<DateTime> samplesTimestamp, List<double[]> histogramListXAdjusted, List<double[]> HistogramListYAdjusted, List<double[]> HistogramListXNotAdjusted, List<double[]> HistogramListYNotAdjusted, DateTime batchStart, DateTime batchEnd)
        {
            /*Configurações do xml writer*/
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("batchInfo");
                writer.WriteAttributeString("batchStart", batchStart.ToString("dd/MM/yyyy HH:mm:ss"));
                writer.WriteAttributeString("batchEnd", batchEnd.ToString("dd/MM/yyyy HH:mm:ss"));

                AppendDistributionNodeFaixas(writer, samplesTimestamp, histogramListXAdjusted, HistogramListYAdjusted);
                writer.WriteEndElement();
            }
            doc.AppendNode(sb.ToString(), batchEnd);

            sb.Clear();
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("batchInfo");
                writer.WriteAttributeString("batchStart", batchStart.ToString("dd/MM/yyyy HH:mm:ss"));
                writer.WriteAttributeString("batchEnd", batchEnd.ToString("dd/MM/yyyy HH:mm:ss"));

                AppendDistributionNodeFaixas(writer, samplesTimestamp, HistogramListXNotAdjusted, HistogramListYNotAdjusted);
                writer.WriteEndElement();
            }
            docNotAdjusted.AppendNode(sb.ToString(), batchEnd);
        }

        private void AddBatchInfoDaily(DateTime dateTimeOfLastBatch)
        {
            //Insere em uma lista
            List<double[]> listHorizontalSizeDistributionAdjusted = new List<double[]>();
            List<double[]> listVerticalSizeDistributionAdjusted = new List<double[]>();
            listHorizontalSizeDistributionAdjusted.Add(PartialHorizontalMeanOfTheDayAdjusted);
            listVerticalSizeDistributionAdjusted.Add(PartialVerticalMeanOfTheDayAdjusted);

            AppendDailyInfo(docDaily, dateTimeOfLastBatch, listHorizontalSizeDistributionAdjusted, listVerticalSizeDistributionAdjusted);

            //Insere em uma lista
            List<double[]> listHorizontalSizeDistributionNotAdjusted = new List<double[]>();
            List<double[]> listVerticalSizeDistributionNotAdjusted = new List<double[]>();
            listHorizontalSizeDistributionNotAdjusted.Add(PartialHorizontalMeanOfTheDayNotAdjusted);
            listVerticalSizeDistributionNotAdjusted.Add(PartialVerticalMeanOfTheDayNotAdjusted);

            AppendDailyInfo(docDailyNotAdjusted, dateTimeOfLastBatch, listHorizontalSizeDistributionNotAdjusted, listVerticalSizeDistributionNotAdjusted);

        }

        private void AppendDailyInfo(XmlFile xmlFile, DateTime dateTimeOfLastBatch, List<double[]> listHorizontalSizeDistribution, List<double[]> listVerticalSizeDistribution)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            StringBuilder sb = new StringBuilder();
            using(XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                writer.WriteStartElement("dayInfo");
                writer.WriteAttributeString("data", dateTimeOfLastBatch.ToString("dd/MM/yyyy"));
                AppendDistributionNodeFaixas(writer, null, listHorizontalSizeDistribution, listVerticalSizeDistribution);
                writer.WriteEndElement();
            }
            xmlFile.AppendNode(sb.ToString(), dateTimeOfLastBatch);
            
        }

        private void AppendDistributionNodeFaixas(XmlWriter writer, List<DateTime> samplesTimestamp, List<double[]> horizontalSizeDistribution, List<double[]> verticalSizeDistribution)
        {
            for (int i = 0; i < horizontalSizeDistribution.Count; i++)
            {
                writer.WriteStartElement("distributionData");
                if (samplesTimestamp != null)
                {
                    writer.WriteAttributeString("timestamp", samplesTimestamp[i].ToString("HH:mm:ss"));
                }

                for (int j = 0; j < GrainSizeRangeList.Count; j++)
                {
                    writer.WriteStartElement("binData");

                    if (j == 0)
                        writer.WriteAttributeString("binInterval", string.Format("0 - {0}", GrainSizeRangeList[j]));
                    else if (j == GrainSizeRangeList.Count - 1)
                        writer.WriteAttributeString("binInterval", string.Format("{0} - ", GrainSizeRangeList[j - 1]));
                    else
                        writer.WriteAttributeString("binInterval", string.Format("{0} - {1}", GrainSizeRangeList[j - 1], GrainSizeRangeList[j]));

                    writer.WriteAttributeString("horizontalDistribution", horizontalSizeDistribution[i][j].ToString("0.000000"));

                    writer.WriteAttributeString("verticalDistribution", verticalSizeDistribution[i][j].ToString("0.000000"));

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        private void ShowMsg(string msg)
        {
            System.Windows.Forms.MessageBox.Show(msg, this.systemName);
        }

        public void CalculateAndSaveDailyIfNewDay(DateTime batchEnd, List<double[]> histogramListXAdjusted, List<double[]> histogramListYAdjusted, List<double[]> histogramListXNotAdjusted, List<double[]> histogramListYNotAdjusted)
        {
            if (dateTimeOfLastBatch != null && batchEnd.Date != dateTimeOfLastBatch.Value.Date)
            {
                AddBatchInfoDaily(dateTimeOfLastBatch.Value);

                PartialHorizontalMeanOfTheDayAdjusted = new double[GrainSizeRangeList.Count];
                PartialVerticalMeanOfTheDayAdjusted = new double[GrainSizeRangeList.Count];
                PartialHorizontalMeanOfTheDayNotAdjusted = new double[GrainSizeRangeList.Count];
                PartialVerticalMeanOfTheDayNotAdjusted = new double[GrainSizeRangeList.Count];
                MeanOfTheDayCounter = 0;
            }

            PartialHorizontalMeanOfTheDayAdjusted = Statistics.DailyMeanFromPartial(MeanOfTheDayCounter, PartialHorizontalMeanOfTheDayAdjusted, histogramListXAdjusted);
            PartialVerticalMeanOfTheDayAdjusted = Statistics.DailyMeanFromPartial(MeanOfTheDayCounter, PartialVerticalMeanOfTheDayAdjusted, histogramListYAdjusted);
            PartialHorizontalMeanOfTheDayNotAdjusted = Statistics.DailyMeanFromPartial(MeanOfTheDayCounter, PartialHorizontalMeanOfTheDayNotAdjusted, histogramListXNotAdjusted);
            PartialVerticalMeanOfTheDayNotAdjusted = Statistics.DailyMeanFromPartial(MeanOfTheDayCounter, PartialVerticalMeanOfTheDayNotAdjusted, histogramListYNotAdjusted);
            MeanOfTheDayCounter += histogramListXAdjusted.Count;
            dateTimeOfLastBatch = batchEnd;
        }

        public void SaveTemporaryData()
        {
            XmlNode rootNode = temporaryDataXml.doc.GetElementsByTagName("tempData").Item(0);
            rootNode.RemoveAll();

            XmlNode dateTimeOfLastBatchNode = temporaryDataXml.doc.CreateElement("dateTimeOfLastBatch");
            dateTimeOfLastBatchNode.InnerText = dateTimeOfLastBatch.ToString();
            rootNode.AppendChild(dateTimeOfLastBatchNode);

            XmlNode meanOfTheDayCounterNode = temporaryDataXml.doc.CreateElement("meanOfTheDayCounter");
            meanOfTheDayCounterNode.InnerText = MeanOfTheDayCounter.ToString();
            rootNode.AppendChild(meanOfTheDayCounterNode);

            foreach (double value in PartialHorizontalMeanOfTheDayAdjusted)
            {
                XmlNode PartialHorizontalMeanOfTheDayAdjustedNode = temporaryDataXml.doc.CreateElement("partialHorizontalMeanOfTheDayAdjusted");
                PartialHorizontalMeanOfTheDayAdjustedNode.InnerText = value.ToString("0.000000");
                rootNode.AppendChild(PartialHorizontalMeanOfTheDayAdjustedNode);
            }

            foreach (double value in PartialVerticalMeanOfTheDayAdjusted)
            {
                XmlNode PartialVerticalMeanOfTheDayAdjustedNode = temporaryDataXml.doc.CreateElement("partialVerticalMeanOfTheDayAdjusted");
                PartialVerticalMeanOfTheDayAdjustedNode.InnerText = value.ToString("0.000000");
                rootNode.AppendChild(PartialVerticalMeanOfTheDayAdjustedNode);
            }

            foreach (double value in PartialHorizontalMeanOfTheDayNotAdjusted)
            {
                XmlNode PartialHorizontalMeanOfTheDayNotAdjustedNode = temporaryDataXml.doc.CreateElement("partialHorizontalMeanOfTheDayNotAdjusted");
                PartialHorizontalMeanOfTheDayNotAdjustedNode.InnerText = value.ToString("0.000000");
                rootNode.AppendChild(PartialHorizontalMeanOfTheDayNotAdjustedNode);
            }

            foreach (double value in PartialVerticalMeanOfTheDayNotAdjusted)
            {
                XmlNode PartialVerticalMeanOfTheDayNotAdjustedNode = temporaryDataXml.doc.CreateElement("partialVerticalMeanOfTheDayNotAdjusted");
                PartialVerticalMeanOfTheDayNotAdjustedNode.InnerText = value.ToString("0.000000");
                rootNode.AppendChild(PartialVerticalMeanOfTheDayNotAdjustedNode);
            }

            temporaryDataXml.doc.Save(temporaryDataXml.fullFileName);
        }
    }

}
