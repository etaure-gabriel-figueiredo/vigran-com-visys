﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    class SlagScreenBo
    {
        double[] slagAdjust;
        int realZeroIndex;

        public SlagScreenBo()
        {
            slagAdjust = new double[] { 0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66 };

            /*O índice 0 se refre a erro de enxofre negativo*/
            realZeroIndex = 1;
        }
        public SlagScreenBo(double[] rangeIndexAdjSulfur)
        {
            slagAdjust = rangeIndexAdjSulfur;
            //slagAdjust = new double[] { 0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66 };

            /*O índice 0 se refre a erro de enxofre negativo*/
            realZeroIndex = 1;
        }

        public float calculateSulfurError(float sulfurTarget, float sulfurAnalysis)
        {
            float sulfurError;
            //float coef = 1.4f;
            float coef = 1f;

            sulfurError = (int)Math.Truncate(sulfurTarget*coef - sulfurAnalysis);

            return sulfurError;
        }

        /*Considera o índice slag já transformado para faixa de o a 100*/
        public double adjustSlag(double slag, float sulfurTarget, float sulfurAnalysis)
        {
            float sulfurError;
            double slagAdjusted;
            int sulfurAjustedIndex;

            sulfurError = calculateSulfurError(sulfurTarget, sulfurAnalysis);

            if( sulfurError < 0 ) /*menor que zero*/
            {
                sulfurAjustedIndex = -1;
            }
            else if(sulfurError > slagAdjust.Count() - 1) /*Maior que o máximo valor de ajuste*/
            {
                sulfurAjustedIndex = slagAdjust.Count() - 1;
            }
            else
            {
                sulfurAjustedIndex = (int)Math.Round(sulfurError);
            }


            slagAdjusted = ((slag - slagAdjust[realZeroIndex + sulfurAjustedIndex]) / (100.0 - slagAdjust[realZeroIndex + sulfurAjustedIndex])) * 100.0;

            slagAdjusted = slagAdjusted < 0 ? 0 : slagAdjusted;

            return slagAdjusted;
        }

        public int adjustSlagPerLadle(double slag, double initialSlag, float sulfurTarget, float sulfurAnalysis)
        {
            float sulfurError;
            double slagAdjusted;
            int sulfurAjustedIndex;

            sulfurError = calculateSulfurError(sulfurTarget, sulfurAnalysis);

            if (sulfurError < 0) /*menor que zero*/
            {
                sulfurAjustedIndex = -1;
            }
            else if (sulfurError > slagAdjust.Count() - 1) /*Maior que o máximo valor de ajuste*/
            {
                sulfurAjustedIndex = slagAdjust.Count() - 1;
            }
            else
            {
                sulfurAjustedIndex = (int)Math.Round(sulfurError);
            }


            slagAdjusted = ((slag - slagAdjust[realZeroIndex + sulfurAjustedIndex]) / (initialSlag)) * 100.0;

            slagAdjusted = slagAdjusted < 0 ? 0 : slagAdjusted;
            slagAdjusted = slagAdjusted > 100 ? 100 : slagAdjusted;

            return (int)Math.Round(slagAdjusted);
        }
    }
}
