﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    public enum ErrorCode
    {
        NO_ERROR            = 0,   /*No error in the system*/
        NO_ROI              = 1,   /*No ROI calculated -> Recalculate whith new frames*/
        SUB_ROI             = 2,   /*Not implemented*/
        SUPER_ROI           = 3,   /*Not implemented*/
        NO_IMAGE_NFORMATION = 4,   /*No iron material to calculate*/
    }
}
