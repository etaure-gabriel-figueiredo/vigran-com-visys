﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.Structure;

namespace WPD_V1
{
    class ImageProcessing
    {
        public Color[] myColorPalette = { Color.Black, Color.DarkBlue, Color.Blue, Color.DarkGreen, Color.Green, Color.Yellow, Color.DarkOrange, Color.Orange, Color.OrangeRed, Color.Red };

        public enum Histogram
        {
            ABSOLUTE,
            NORMAL
        };

        public enum Segmentation
        {
            ORIGINAL,
            BINARY
        };

        public class Statistics
        {
            public Double average = 0;
            public Double standard_deviation = 0;
        }

        /*------------------ Class attributes ------------------*/

        /*Pixel depth in captured images*/
        public UInt16 pixelDepth;

        /*Bitmaps shared with graphical interface*/
        Bitmap inputImageClr, outputImageClr;
        Bitmap inputImageNir, outputImageNir;

        /*Image processing bitmaps*/
        Bitmap nirSeg;
        Bitmap redSeg;
        Bitmap greenSeg;
        Bitmap bin;
        Bitmap roi;
        Bitmap centroi;

        /*ROI parameters*/
        Rectangle rectInitialRoi;
        Rectangle rectRoi;
        UInt32 roiRefValue;

        /*Slag information*/
        Double maxSlagMesure;

        Slag slagInformation;        

        bool flagDetectRoi = true;

        int minimumValue;
        int maximumValue;
        
        /*------------------ Class constructor and acess methods ------------------*/
        public ImageProcessing(ref Bitmap imgCLR, ref Bitmap imgNIR, ref Bitmap imgCLRV, ref Bitmap imgNIRV, Rectangle rectInitialRoi)
        {
            /*Original images with high pixel resolution -> 48 bits per pixel -> 16 bits per channel */
            this.inputImageClr = imgCLR;
            this.inputImageNir = imgNIR;

            if (inputImageClr.PixelFormat == PixelFormat.Format48bppRgb || inputImageNir.PixelFormat == PixelFormat.Format16bppGrayScale)
            {
                this.pixelDepth = 65535; /*16 bits per channel*/
            }
            else
            {
                this.pixelDepth = 256; /*8 bits per channel*/
            }

            minimumValue = -1;
            maximumValue = -1;

            /*Display images with inferior pixel resolution -> 32 bits per pixel -> 8 bits per channel */
            this.outputImageClr = imgCLRV;
            this.outputImageNir = imgNIRV;

            /*Processing images with high pixel resolution -> 48 bits per pixel -> 16 bits per channel */
            nirSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            redSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            greenSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            bin = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            roi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);
            centroi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);

         
            /*Scoria information objects*/
            this.slagInformation = new Slag(ref inputImageNir);

            /*Sets initial roi rectangle*/
            this.rectInitialRoi = rectInitialRoi;
       
            /*Defines Color Palette to be like temperature palette*/
            Array.Reverse(myColorPalette);
        }

        public ImageProcessing(ref Bitmap imgNIR, ref Bitmap imgNIRV, Rectangle rectInitialRoi)
        {
            /*Original images with high pixel resolution -> 48 bits per pixel -> 16 bits per channel */
          
            this.inputImageNir = imgNIR;

            if (inputImageNir.PixelFormat == PixelFormat.Format16bppGrayScale)
            {
                this.pixelDepth = 65535; /*16 bits per channel*/
            }
            else
            {
                this.pixelDepth = 256; /*8 bits per channel*/
            }

            /*Display images with inferior pixel resolution -> 32 bits per pixel -> 8 bits per channel */
            this.outputImageNir = imgNIRV;

            /*Processing images with high pixel resolution -> 48 bits per pixel -> 16 bits per channel */
            nirSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            greenSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            redSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            bin = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
            roi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);
            centroi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);


            /*Scoria information objects*/
            this.slagInformation = new Slag(ref inputImageNir);

            /*Sets initial roi rectangle*/
            this.rectInitialRoi = rectInitialRoi;

            /*Defines Color Palette to be like temperature palette*/
            Array.Reverse(myColorPalette);
        }

        public Bitmap InputImage
        {
            get { return inputImageNir; }
            set
            {
                inputImageNir = value;

                /*Processing images with high pixel resolution -> 48 bits per pixel -> 16 bits per channel */
                nirSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
                redSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
                greenSeg = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
                bin = new Bitmap(inputImageNir.Width, inputImageNir.Height, inputImageNir.PixelFormat);
                roi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);
                centroi = new Bitmap(inputImageNir.Width, inputImageNir.Height, outputImageNir.PixelFormat);
            }
        }

        public void setInitialRoi(Rectangle initRoi)
        {
            this.rectInitialRoi = initRoi;
        }

        public Bitmap getBitmap(Channel id_canal)
        {
            switch  (id_canal)
            {
                case Channel.CLR:
                    return inputImageClr;
                case Channel.NIR:
                    return inputImageNir;
                case Channel.SEG:
                    return nirSeg;
                case Channel.GREEN:
                    return greenSeg;
                case Channel.BIN:
                    return bin;
                case Channel.ROI:
                    return roi;
                case Channel.CENTROI:
                    return centroi;
                case Channel.SLG:
                    return slagInformation.outputImage;
                case Channel.VCLR:
                    return outputImageClr;
                case Channel.VNIR:
                    return outputImageNir;
                case Channel.RED:
                    return redSeg;
                default:
                    return null;
            }
        }

        public Slag getInfoSlag()
        {
            return slagInformation;
        }

        public void setFlagDetectROI(bool value)
        {
            flagDetectRoi = value;
        }

        public void adjustRoi()
        {
            setFlagDetectROI(true);

            this.roi = centroi.Clone(new Rectangle(0, 0, centroi.Width, centroi.Height), centroi.PixelFormat);

            setFlagDetectROI(false);
        }

        public void setROI(Bitmap ROI, UInt32 ref_value)
        {
            this.roiRefValue = ref_value;
            this.roi = ROI;
     
            this.centroi = roi.Clone(new Rectangle(0, 0, roi.Width, roi.Height), roi.PixelFormat);

            slagInformation.error = maxSlagMesure == 0 ? ErrorCode.NO_ROI : ErrorCode.NO_ERROR;
            flagDetectRoi = false;
        }

        /*=========================== Image Processing Functions ===========================*/
     
       public void saveROI(String file_name)
       {
           roi.Save(file_name);
       }
        
       public void loadROI(Bitmap ROI_in, UInt32 ref_value)
       {           
           setROI(ROI_in, ref_value);
       }

       public Double calculateExpectedValue(Channel id_channel, Double[] probFunc, int startIntensity, int endIntensity)
       {
           Double probSum = 0;
           int channelIntensity;
           Double expValue = 0;

           if (id_channel == Channel.GREEN)
           {
               for (int i = startIntensity; i < endIntensity; i++)
               {
                   channelIntensity = i;
                   probSum += probFunc[i];
                   expValue += channelIntensity * probFunc[i];
               }

               expValue = expValue/probSum;
           }


           return expValue;
       }

        public void histogram( Histogram histogram_type, Channel id_channel, out Double [] hist)
        {
             hist = new Double[pixelDepth];
            //hist.Initialize();

            UInt32 i,j;
            UInt32 total = 0;

            if (id_channel == Channel.NIR)
            {
                unsafe
                {
                    BitmapData wData_NIR = inputImageNir.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, inputImageNir.PixelFormat);

                    /*This time we convert the IntPtr to a ptr*/
                    UInt16* scan0_NIR = (UInt16*)wData_NIR.Scan0.ToPointer();

                    for (i = 0; i < wData_NIR.Width*wData_NIR.Height; ++i)
                    {    
                            UInt16* data_NIR = scan0_NIR + i;

                            UInt16 ind = *data_NIR;

                            hist[ind] = hist[ind] + 1;

                            total++;            
                    }

                    inputImageNir.UnlockBits(wData_NIR);
                }
            }
            else if (id_channel == Channel.GREEN)
            {
                    ushort ind;

                    minimumValue = pixelDepth;
                    maximumValue = 0;

                    for (i = 0; i < inputImageClr.Height; ++i)
                    {    
                        for(j=0; j < inputImageClr.Width; j++)
                        {
                        //ind = inputImageClr.GetPixel((int)j, (int)i).B;
                        ind = inputImageClr.GetPixel((int)j, (int)i).G;
                        hist[ind] = hist[ind] + 1;

                            total++;  
    
                            /*Calcula os mínimos e máximos do canal*/
                            if (ind > maximumValue)
                            {
                                maximumValue = ind;
                            }

                            if (ind < minimumValue)
                            {
                                minimumValue = ind;
                            }
                        }
                    }
            }
            else if (id_channel == Channel.SEG)
            {
                ushort ind;

                minimumValue = pixelDepth;
                maximumValue = 0;

                for (i = 0; i < greenSeg.Height; ++i)
                {
                    for (j = 0; j < greenSeg.Width; j++)
                    {
                        //ind = inputImageClr.GetPixel((int)j, (int)i).B;
                        ind = greenSeg.GetPixel((int)j, (int)i).G;
                        hist[ind] = hist[ind] + 1;

                        total++;

                        /*Calcula os mínimos e máximos do canal*/
                        if (ind > maximumValue)
                        {
                            maximumValue = ind;
                        }

                        if (ind < minimumValue)
                        {
                            minimumValue = ind;
                        }
                    }
                }
            }



            if (histogram_type == Histogram.NORMAL)
            { 
                for(i=0; i < pixelDepth; i++)
                {
                    hist[i] = hist[i]/total;
                }
            }
        }

        public int getMinimumChannelIntensity(Channel id_channel)
        {
            if (id_channel == Channel.GREEN)
            {
                return minimumValue;
            }else
            {
                return -1;
            }

        }

        public int getMaximumChannelIntensity(Channel id_channel)
        {
            if (id_channel == Channel.GREEN)
            {
                return maximumValue;
            }
            else
            {
                return -1;
            }
        }

        /*Determina o centro da panela através da média ponderada dos pixels pela temperatura*/
        public Point findCenter(Bitmap image)
        {
            UInt32 i,j;
            float sumWeightsX = 0, sumWeightsY = 0, auxX=0, auxY=0;

            BitmapData wData = image.LockBits(rectInitialRoi, ImageLockMode.ReadOnly, image.PixelFormat);

            unsafe
            {
                /*This time we convert the IntPtr to a ptr*/
                UInt16* scan0_NIR = (UInt16*)wData.Scan0.ToPointer();

                for (i = 0; i < wData.Width; ++i)
                {
                    for (j = 0; j < wData.Height; ++j)
                    {
                        UInt16* data_NIR = scan0_NIR + i + j * image.Width;

                        /*Deve-se somar o ponto inicial da ROI inicial porque i e j são relativos ao retângulos*/
                        auxX += (rectInitialRoi.Left + i) * (*data_NIR);
                        auxY += (rectInitialRoi.Top + j) * (*data_NIR);

                        sumWeightsX += (*data_NIR);
                        sumWeightsY += (*data_NIR);
                    }
                }

                image.UnlockBits(wData);
            }

            return new Point((int)Math.Round( auxX / sumWeightsX), (int)Math.Round( auxY / sumWeightsY));
        }

        public bool inRectangle(int x, int y, Rectangle rect)
        {
            if( x >= rect.Left && x <= (rect.Left + rect.Width) && y >= rect.Top && y <= (rect.Top + rect.Height) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void setCentralizedRoi()
        {
            PointF roiCenter = this.findCenter(this.roi);
            PointF panCenter = this.findCenter(this.inputImageNir);
            PointF delta = new Point(0,0);

            delta.X =  roiCenter.X - panCenter.X;
            delta.Y =  roiCenter.Y - panCenter.Y;

            int i, j;
            int newI, newJ;

            unsafe
            {
                BitmapData wData_ROI = roi.LockBits(new Rectangle(0, 0, roi.Width, roi.Height), ImageLockMode.ReadOnly, roi.PixelFormat);
                UInt32* scan0_ROI = (UInt32*)wData_ROI.Scan0.ToPointer();

                BitmapData wData_CENTROI = centroi.LockBits(new Rectangle(0, 0, centroi.Width, centroi.Height), ImageLockMode.ReadWrite, centroi.PixelFormat);
                UInt32* scan0_CENTROI = (UInt32*)wData_CENTROI.Scan0.ToPointer();

                for (i = 0; i < roi.Width; ++i)
                {
                    for (j = 0; j < roi.Height; ++j)
                    {
                        newI = i + (int)delta.X;
                        newJ = j + (int)delta.Y;

                        UInt32* data_ROI = scan0_ROI + i + j * roi.Width;
                        //UInt32* data_ROI = scan0_CENTROI + i + j * roi.Width;

                        if (newI > 0 && newJ > 0 && newI < centroi.Width && newJ < centroi.Height)
                        {
                            UInt32* data_CENTROI = scan0_CENTROI + newI + newJ * centroi.Width;

                            *data_CENTROI = *data_ROI;
                        }
                    }
                }

                roi.UnlockBits(wData_ROI);
                centroi.UnlockBits(wData_CENTROI);
            }
        }

        public Double[] histogramInRoi(Histogram histogram_type, Channel id_channel)
        {
            Double[] hist = new Double[pixelDepth];
            //hist.Initialize();

            UInt32 i;
            UInt32 total = 0;

            if (id_channel == Channel.NIR)
            {
                BitmapData wData_NIR = inputImageNir.LockBits(rectInitialRoi, ImageLockMode.ReadOnly, inputImageNir.PixelFormat);
                BitmapData wData_ROI = roi.LockBits(rectInitialRoi, ImageLockMode.ReadOnly, roi.PixelFormat);

                unsafe
                {
                    /*This time we convert the IntPtr to a ptr*/
                    UInt16* scan0_NIR = (UInt16*)wData_NIR.Scan0.ToPointer();
                    UInt32* scan0_ROI = (UInt32*)wData_ROI.Scan0.ToPointer();

                    for (i = 0; i < rectInitialRoi.Width * rectInitialRoi.Height; i++)
                    {
                        UInt16* data_NIR = scan0_NIR + i;
                        UInt32* data_ROI = scan0_ROI + i;

                        if (*data_ROI == roiRefValue)
                        {
                            UInt16 ind = *data_NIR;

                            hist[ind] = hist[ind] + 1;

                            total++;
                        }
                    } 
                }

                inputImageNir.UnlockBits(wData_NIR);
                roi.UnlockBits(wData_ROI);
            }

            if (histogram_type == Histogram.NORMAL)
            {
                for (i = 0; i < pixelDepth; i++)
                {
                    hist[i] = hist[i] / total;
                }
            }

            return hist;
        }

        public Statistics AvgStdByHistogram( Double[] hist)
        {
            UInt32 i;
            Statistics retorno = new Statistics();
    
            Double total = 0;

            /*calcula a media da UInt16ensidade de pixels da imagem*/
            for(i=0; i < pixelDepth; i++)
            {
                retorno.average = retorno.average + hist[i]*i;

                total = total + hist[i];
            }
    
            /*calcula a variancia da UInt16ensidade de pixels da imagem*/
            for(i=0; i < pixelDepth; i++)
            {
                retorno.standard_deviation = retorno.standard_deviation + Math.Pow((i-retorno.average),2)*hist[i];
            }
    
            retorno.average = retorno.average/total;
            retorno.standard_deviation = Math.Sqrt(retorno.standard_deviation/(total-1));

            return retorno;
        }

        void segmentByChannel(Channel id_channel, UInt16 min, UInt16 max)
        {
            int i;
            int j;

            if( id_channel == Channel.NIR )
            {
                BitmapData wData_NIR = inputImageNir.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, inputImageNir.PixelFormat);
                BitmapData wData_SEG = nirSeg.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, nirSeg.PixelFormat);
                BitmapData wData_BIN = bin.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, bin.PixelFormat);

                unsafe
                {

                    /*This time we convert the IntPtr to a ptr*/
                    UInt16* scan0_NIR = (UInt16*)wData_NIR.Scan0.ToPointer();
                    UInt16* scan0_SEG = (UInt16*)wData_SEG.Scan0.ToPointer();
                    UInt16* scan0_BIN = (UInt16*)wData_BIN.Scan0.ToPointer();

                    for (i = 0; i < wData_NIR.Width*wData_NIR.Height; ++i)
                    {
                            UInt16* data_NIR = scan0_NIR + i ;
                            UInt16* data_SEG = scan0_SEG + i ;
                            UInt16* data_BIN = scan0_BIN + i ;

                            /*Segmenta a imagem pela temperatura (imagem NIR)*/
                            if (*data_NIR >= min && *data_NIR <= max)
                            {
                                *data_SEG = *data_NIR;
                                *data_BIN = 1;
                            }
                            else
                            {
                                *data_SEG = 0;
                                *data_BIN = 0;
                            }

                        
                    }

                    inputImageNir.UnlockBits(wData_NIR);
                    nirSeg.UnlockBits(wData_SEG);
                    bin.UnlockBits(wData_BIN);
                }

            }else if(id_channel == Channel.GREEN)
            {
                ushort intensityValeu;

                for (i = 0; i < inputImageClr.Height; ++i)
                {
                    for (j = 0; j < inputImageClr.Width; j++)
                    {
                        intensityValeu = inputImageClr.GetPixel((int)j, (int)i).G;

                        /*Calcula os mínimos e máximos do canal*/
                        if (intensityValeu > min && intensityValeu < max)
                        {
                            greenSeg.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, intensityValeu, intensityValeu, intensityValeu));
                            bin.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, pixelDepth-1, pixelDepth-1, pixelDepth-1));
                        }
                        else
                        {
                            greenSeg.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, 0, 0, 0));
                            bin.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, 0, 0, 0));
                        }
                    }
                }
            }else if(id_channel == Channel.RED)
            {
                ushort intensityValeu;

                for (i = 0; i<inputImageClr.Height; ++i)
                {
                    for (j = 0; j<inputImageClr.Width; j++)
                    {
                        intensityValeu = inputImageClr.GetPixel((int)j, (int)i).R;

                        /*Calcula os mínimos e máximos do canal*/
                        if (intensityValeu > min && intensityValeu<max)
                        {
                            redSeg.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, intensityValeu, intensityValeu, intensityValeu));
                            bin.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, pixelDepth-1, pixelDepth-1, pixelDepth-1));
                        }
                        else
                        {
                            redSeg.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, 0, 0, 0));
                            bin.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth-1, 0, 0, 0));
                        }
                    }
                }
            }

        }

        Double sumPixelsRight(Channel id_channel, UInt32 pixel_x, UInt32 pixel_y, UInt32 elements_number_sum)
        {
            int i;

            int pos_high_sum = (int)(pixel_x + elements_number_sum);

            UInt64 soma = 0;

            if(id_channel == Channel.BIN)
            {
                BitmapData wData_BIN = bin.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, bin.PixelFormat);

                if(pos_high_sum > wData_BIN.Width)
                {
                   pos_high_sum = wData_BIN.Width;
                }

                unsafe
                {

                    /*This time we convert the IntPtr to a ptr*/
                    UInt16* scan0_BIN = (UInt16*)wData_BIN.Scan0.ToPointer();

                    for (i = (int)pixel_x; i < pos_high_sum; ++i)
                    {
                        UInt16* data_BIN = scan0_BIN + i + pixel_y * bin.Width;

                        soma = soma + *data_BIN;
                    }

                    bin.UnlockBits(wData_BIN);
                }
            }

            return soma;
        }

        void fillInsideROI(Channel id_channel, Double perc_largura_imagem)
        {
            UInt16 i, j;

            /*calcula quantos pixels 0 podem existir para considerar o pixel como pertencente a ROI*/
            UInt16 elements_number_sum = (UInt16)Math.Round(inputImageClr.Width * perc_largura_imagem);

            UInt16 desired_pixels_right = (UInt16)Math.Round(0.2 * elements_number_sum);

            if(id_channel == Channel.NIR)
            {
                BitmapData wData_NIR = inputImageNir.LockBits(rectInitialRoi, ImageLockMode.ReadOnly, inputImageNir.PixelFormat);
                BitmapData wData_SEG = nirSeg.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, nirSeg.PixelFormat);

                maxSlagMesure = 0;

                unsafe
                {

                    /*This time we convert the IntPtr to a ptr*/
                    UInt16* scan0_NIR = (UInt16*)wData_NIR.Scan0.ToPointer();
                    UInt16* scan0_SEG = (UInt16*)wData_SEG.Scan0.ToPointer();
                    

                    for (i = 0; i < wData_NIR.Width; ++i)
                    {
                        for (j = 0; j < wData_NIR.Height; ++j)
                        {

                            UInt16* data_NIR = scan0_NIR + i + j * inputImageNir.Width;
                            UInt16* data_SEG = scan0_SEG + i + j * nirSeg.Width;

                            if (sumPixelsRight(Channel.BIN, i, j, elements_number_sum) > desired_pixels_right)
                            {
                                *data_SEG = *data_NIR;

                                BitmapData wData_BIN = bin.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, bin.PixelFormat);
                                UInt16* scan0_BIN = (UInt16*)wData_BIN.Scan0.ToPointer();

                                UInt16* data_BIN = scan0_BIN + i + j * bin.Width;

                                *data_BIN = 1;

                                bin.UnlockBits(wData_BIN);

                                /*contabiliza a area/mascara da ROI*/
                                maxSlagMesure = maxSlagMesure + 1;
                            }
                            else
                            {
                                *data_SEG = 0;

                                BitmapData wData_BIN = bin.LockBits(rectInitialRoi, ImageLockMode.ReadWrite, bin.PixelFormat);
                                UInt16* scan0_BIN = (UInt16*)wData_BIN.Scan0.ToPointer();

                                UInt16* data_BIN = scan0_BIN + i + j * bin.Width;

                                *data_BIN = 0;

                                bin.UnlockBits(wData_BIN);
                            }
                        }
     
                    }

                    inputImageNir.UnlockBits(wData_NIR);
                    nirSeg.UnlockBits(wData_SEG);

                    /*Checa a ROI calculada*/

                    slagInformation.error = maxSlagMesure == 0 ? ErrorCode.NO_ROI : ErrorCode.NO_ERROR;
                }
            }
        }

        private void segmentByMetricInformation(double pixelPerMm, double metricDimensionMm, double percentualMetricGap)
        {
            int centerWidthPixel = (int)Math.Round(inputImageClr.Width / 2.0 + 10);
            int roiWidthPixel = (int)Math.Round(metricDimensionMm * (1 + percentualMetricGap) * pixelPerMm);

            int startWidth = (int)Math.Round(centerWidthPixel - (roiWidthPixel / 2.0));

            if (startWidth < 0)
                startWidth = 0;

            if (roiWidthPixel > inputImageClr.Width)
                roiWidthPixel = inputImageClr.Width;

            rectRoi = new Rectangle(new Point(startWidth, 0), new Size(roiWidthPixel, inputImageClr.Height));
        }

        public void detectROI(Channel id_canal, UInt16 min, UInt16 max)
        {          
            /*Limiarizacao pela imagem NIR*/   
            segmentByChannel(id_canal, min, max);

            /*Preenche os buracos na segmentacao*/
            /*Nao passar de 5% (0.05)*/
            Double percImageWidth = 0.015;

            fillInsideROI(id_canal, percImageWidth);
        }

        public void detectROI(Channel id_canal, double pixelPerMm, double metricDimensionMm)
        {
            if (id_canal == Channel.GREEN)
            {
                double percentualMetricGap = 0;
                segmentByMetricInformation(pixelPerMm, metricDimensionMm, percentualMetricGap); 

                for (int i = 0; i < greenSeg.Height; ++i)
                {
                    for (int j = 0; j < greenSeg.Width; j++)
                    {

                        if (j >= rectRoi.X && j < rectRoi.X + rectRoi.Width)
                        {
                            byte intensityValeu = inputImageClr.GetPixel((int)j, (int)i).G;

                            greenSeg.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth - 1, intensityValeu, intensityValeu, intensityValeu));
                            bin.SetPixel((int)j, (int)i, Color.FromArgb(pixelDepth - 1, pixelDepth - 1, pixelDepth - 1, pixelDepth - 1));
                        }
                    }
                }
            }
        }

        public void processRedChanel()
        {
            this.segmentByChannel(Channel.RED, 0, 255);
        }

        public void CannyEdgeDetector(float thresholdHigh, float thresholdLow, int apertureSize, bool l2Gradient)
        {
            //Canny cannyData = new Canny(this.inputImageNir, thresholdHigh, thresholdLow, gaussianMaskSize, sigmaforGaussianKernel);
            //this.bin = cannyData.DisplayImage(cannyData.EdgeMap);


            Image<Gray, Byte> imageCV = new Image<Gray, byte>(this.inputImageNir.Clone(rectInitialRoi, inputImageNir.PixelFormat));
            Image<Gray, Byte> imageCanny = new Image<Gray, byte>(rectInitialRoi.Width, rectInitialRoi.Height, new Gray(0));

            CvInvoke.Canny(imageCV, imageCanny, thresholdLow, thresholdHigh, apertureSize, l2Gradient);
            this.bin = imageCanny.ToBitmap();
        }   
    }

}
