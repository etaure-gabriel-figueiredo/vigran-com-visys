﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;


[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config")]
namespace WPD_V1
{
    public static class Logger
    {
        private static ILog log = log4net.LogManager.GetLogger("Logger");

        public static void Debug(string message)
        {
            log.Debug(message);
        }

        public static void Info(string message)
        {
            log.Info(message);
        }

        public static void Warn(string message)
        {
            log.Warn(message);
        }

        public static void Warn(string message, Exception ex)
        {
            log.Warn(message, ex);
        }

        public static void Error(string message)
        {
            log.Error(message);
        }

        public static void Error(string message, Exception ex)
        {
            log.Error(message, ex);
        }

        public static void Fatal(string message)
        {
            log.Fatal(message);
        }

        public static void Fatal(string message, Exception ex)
        {
            log.Fatal(message, ex);
        }

    }
}
