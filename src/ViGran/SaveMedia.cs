﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
using System.Drawing.Imaging;

namespace BasicUtil
{
    class SaveMedia
    {
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        /// <summary>
        /// Salva imagem usando qualidade como parâmetro
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="image"></param>
        /// <param name="compressionQuality">Percentagem de qualidade da imagem</param>
        static public void saveImageQuality(string folderName, Bitmap image, Int64 compressionQuality)
        {
            ImageCodecInfo myImageCodecInfo = GetEncoderInfo("image/jpeg");
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameter  myEncoderParameter = new EncoderParameter(myEncoder, compressionQuality);
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            myEncoderParameters.Param[0] = myEncoderParameter;

            image.Save(folderName + ".jpg", myImageCodecInfo, myEncoderParameters);
        }

        /// <summary>
        /// Salva imagem usando nível de compressão como parâmetro
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="image"></param>
        /// <param name="compressionQuality">Percentagem de compressão da imagem</param>
        static public void saveImageCompression(string folderName, Bitmap image, Int64 compressionQuality)
        {
            ImageCodecInfo myImageCodecInfo = GetEncoderInfo("image/jpeg");
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Compression;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, compressionQuality);
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            myEncoderParameters.Param[0] = myEncoderParameter;

            image.Save(folderName + ".jpg", myImageCodecInfo, myEncoderParameters);
        }

        static public void zipFiles(string startPath, string zipPath)
        {
            if (!File.Exists(zipPath))
            {
                ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Optimal, false);
            }
        }

        static public void deleteFiles(string folderPath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }

        static public void deleteFiles(string folderPath, string ext)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Extension.Equals(ext))
                {
                    file.Delete();
                }
            }
        }

        public static void Compress(string folderPath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (FileInfo fileToCompress in di.GetFiles())
            {
                using (FileStream originalFileStream = fileToCompress.OpenRead())
                {
                    if ((File.GetAttributes(fileToCompress.FullName) &
                       FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                    {
                        using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                        {
                            using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                               CompressionMode.Compress))
                            {
                                originalFileStream.CopyTo(compressionStream);
                            }
                        }
                        //FileInfo info = new FileInfo(directoryPath + @"\" + fileToCompress.Name + ".gz");
                        //Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                        //fileToCompress.Name, fileToCompress.Length.ToString(), info.Length.ToString());
                    }

                }
            }
        }
    }
}
