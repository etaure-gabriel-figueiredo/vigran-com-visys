﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ViSys.Modules.Core
{
    public class DataExtractor
    {
        private Bitmap mbpimg = null;
        private Image<Gray, byte> imgChanB;
        private Image<Gray, byte> imgChanG;
        private Image<Gray, byte> imgChanR;
        private Image<Bgr, byte> imgInput;

        public DataExtractor(Bitmap img)
        {
            // ASR BEGIN *****************************
            this.mbpimg = img;
            // ASR END *****************************

            //Original ************************* 
            //this.mbpimg = new Bitmap(img);
            //Original ************************* 
            //this.mbpimg = (Bitmap)img.Clone();
            this.imgInput = new Image<Bgr, byte>((Bitmap)img.Clone());
        }

        public List<float[]> RGBHistogram()
        {

            imgChanR = this.imgInput[2];
            imgChanG = this.imgInput[1];
            imgChanB = this.imgInput[0];

            List<float[]> mhistrgbarray = new List<float[]>();

            // ASR BEGIN *****************************
            DenseHistogram hist = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            hist.Calculate(new Image<Gray, byte>[] { imgChanR }, false, null);
            float[] histData = new float[256];
            hist.CopyTo(histData);

            //DenseHistogram hist2 = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            //hist2.Calculate(new Image<Gray, byte>[] { imgChanG }, false, null);
            //float[] histData2 = new float[256];
            //hist2.CopyTo(histData2);

            //DenseHistogram hist3 = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            //hist3.Calculate(new Image<Gray, byte>[] { imgChanB }, false, null);
            //float[] histData3 = new float[256];
            //hist3.CopyTo(histData3);

            mhistrgbarray.Add(histData);
            mhistrgbarray.Add(histData);
            mhistrgbarray.Add(histData);

            // ASR END *****************************

            //Original ************************* 
            //DenseHistogram hist = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            //hist.Calculate(new Image<Gray, byte>[] { imgChanR }, false, null);
            //float[] histData = new float[256];
            //hist.CopyTo(histData);

            //DenseHistogram hist2 = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            //hist2.Calculate(new Image<Gray, byte>[] { imgChanG }, false, null);
            //float[] histData2 = new float[256];
            //hist2.CopyTo(histData2);

            //DenseHistogram hist3 = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            //hist3.Calculate(new Image<Gray, byte>[] { imgChanB }, false, null);
            //float[] histData3 = new float[256];
            //hist3.CopyTo(histData3);

            //mhistrgbarray.Add(histData);
            //mhistrgbarray.Add(histData2);
            //mhistrgbarray.Add(histData3);

            //Original ************************* 

            return mhistrgbarray;
        }

        public List<int> NumberOfPixels()
        {

            List<int> features = new List<int>();

            // Lock the bitmap's bits.  
            Rectangle rect = new Rectangle(0, 0, this.mbpimg.Width, this.mbpimg.Height);
            BitmapData bmpData = this.mbpimg.LockBits(rect, ImageLockMode.ReadWrite, this.mbpimg.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = bmpData.Stride * this.mbpimg.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] r = new byte[bytes / 3];
            byte[] g = new byte[bytes / 3];
            byte[] b = new byte[bytes / 3];

            // Copy the RGB values into the array.
            Marshal.Copy(ptr, rgbValues, 0, bytes);

            int count = 0;
            int whitePixels = 0;
            int BlackPixels = 0;
            int stride = bmpData.Stride;

            for (int column = 0; column < bmpData.Height; column++)
            {
                for (int row = 0; row < bmpData.Width; row++)
                {
                    b[count] = (byte)(rgbValues[(column * stride) + (row * 3)]);
                    g[count] = (byte)(rgbValues[(column * stride) + (row * 3) + 1]);
                    r[count] = (byte)(rgbValues[(column * stride) + (row * 3) + 2]);

                    if (b[count] == 255 && g[count] == 255 && r[count] == 255)
                    {
                        whitePixels++;
                    }

                    if (b[count] == 0 && g[count] == 0 && r[count] == 0)
                    {
                        BlackPixels++;
                    }

                    count++;
                }
            }

            features.Add(whitePixels);
            features.Add(BlackPixels);
            features.Add(count);

            return features;
        }
    }
}
