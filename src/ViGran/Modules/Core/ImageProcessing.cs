﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ViSys.Modules.Core
{
    public class ImageProcessing
    {
        public Bitmap moriginalimg = null;
        public Image<Bgr, byte> memguimg= null;
        public Image<Gray, byte> mgimg = null;
        Image<Bgr, byte> i;

        public int threshold = 100;
        public int maxValue = 255;

        public ImageProcessing(Bitmap bpParam, int tHold, int mValue)
        {
            this.memguimg = new Image<Bgr, byte>(bpParam);            
            this.moriginalimg = bpParam;
            this.threshold = tHold;
            this.maxValue = mValue;
        }

        public ImageProcessing(Bitmap bpParam)
        {
            this.memguimg = new Image<Bgr, byte>(bpParam);
            this.moriginalimg = bpParam;
        }

        public Bitmap InsertShape()
        {
            Image<Bgr, byte> copiedImage = new Image<Bgr, byte>(this.memguimg.Width, this.memguimg.Height);
            this.memguimg.CopyTo(copiedImage);
            CvInvoke.Circle(copiedImage, new Point((int)this.memguimg.Width/2, (int)this.memguimg.Height/2), 30, new MCvScalar(255, 255, 255));
            return copiedImage.Bitmap;
        }

        public Bitmap BasicThresh()
        {
            this.mgimg = new Image<Gray, byte>(this.memguimg.Width, this.memguimg.Height);
            CvInvoke.Threshold(this.memguimg.Convert<Gray, byte>(), this.mgimg, threshold, maxValue, Emgu.CV.CvEnum.ThresholdType.Binary);
            return this.mgimg.Bitmap;
        }

        public static bool JustAGenericMethod(Bitmap genercBitmap)
        {            
            return true;
        }

        private Image<Bgr, byte> getRoi(Bitmap img, Rectangle rect)
        {
            i = new Image<Bgr, byte>(img);

            i.ROI = rect;
            Image<Bgr, byte> temp = i.CopyBlank();
            i.CopyTo(temp);
            i.ROI = Rectangle.Empty;

            return temp;
        }

        /// <summary>
        /// Retorna apenas a ROI para exibição
        /// </summary>
        /// <param name="img"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        public Bitmap showMiniature(Bitmap img, Rectangle rect)
        {
            return (getRoi(img, rect).Bitmap);
        }


        /// <summary>
        /// Função que extrai a média dos bits da ROI selecionada.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        public int getMean(Bitmap img, Rectangle rect)
        {
            Image<Gray, byte>[] grayChannels = getRoi(img, rect).Split();

            MCvScalar t1 = CvInvoke.Mean(getRoi(img, rect));
            MCvScalar t2 = CvInvoke.Mean(grayChannels[0]);

            int t = 2;
            return (t);
        }


        private void calcHist(Bitmap img_x)
        {
            Bitmap img = new Bitmap(img_x);

            Image<Gray, byte> i = new Image<Gray, byte>(img);


            DenseHistogram hist = new DenseHistogram(65535, new RangeF(7000, 20000));
            hist.Calculate(new Image<Gray, byte>[] { i }, true, null);

            float[] histData = new float[65535];

            hist.CopyTo(histData);
        }

        private void normHist(Bitmap img)
        {
            Mat i_out = new Mat();
            Image<Gray, byte> i = new Image<Gray, byte>(img);

            CvInvoke.EqualizeHist(i, i_out);
        }
    }
}
