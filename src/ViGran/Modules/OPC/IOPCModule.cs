﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViSys.Modules.OPC
{
    /// <summary>
    /// Interface para as classes assistentes DAAssistant e UAAssistant.
    /// </summary>
    public interface IOPCModule
    {
        /// <summary>
        /// Evento que é disparado quando há alteração no valor das tags monitoradas.
        /// </summary>
        event EventHandler<object> Update;

        /// <summary>
        /// Evento que é disparado quando uma escrita assíncrona é concluída.
        /// </summary>
        event EventHandler<object> WritingDone;

        /// <summary>
        /// Evento que é disparado quando uma leitura assíncrona é concluída.
        /// </summary>
        event EventHandler<object> ReadingDone;

        /// <summary>
        /// Conecta-se ao servidor OPC.
        /// </summary>
        /// /// <param name="programID">Nome da aplicação que executa o servidor. </param>
        /// /// <param name="node">IP ou Endpoint do servidor. </param>
        void Connect(string programID, string node);

        /// <summary>
        /// Adiciona tags ao grupo de leitura ou escrita, determinado a partir do enumerador OPCItemUse.
        /// </summary>
        /// /// <param name="items"> Lista com os nomes das tags. </param>
        /// /// <param name="use"> Finalidade das tags na lista: escrita ou leitura. </param>
        void AddItems(List<string> items, OPCModuleFactory.OPCItemUse use);

        /// <summary>
        /// Lê o valor da tag de forma síncrona.
        /// </summary>
        /// <param name="id"> Nome da tag. </param>
        object Read(string id);

        /// <summary>
        /// Lê o valor da tag de forma assíncrona. O evento ReadingDone é disparado quando a leitura é concluída.
        /// </summary>
        /// <param name="id"> Nome da tag. </param>
        void ReadAsync(string id);

        /// <summary>
        /// Escreve na tag de forma síncrona.
        /// </summary>
        /// <param name="value"> Novo valor. </param>
        /// <param name="id"> Nome da tag. </param>
        bool Write(object value, string id);

        /// <summary>
        /// Escreve na tag de forma assíncrona. O evento WritingDone é disparado quando a escrita é concluída.
        /// </summary>
        /// <param name="value"> Novo valor. </param>
        /// <param name="id"> Nome da tag. </param>
        void WriteAsync(object value, string id);

        /// <summary>
        /// Inicia o monitoramento das tags de leitura. O evento Update é disparado quando houver alteração nas tags monitoradas.
        /// </summary>
        void Monitor();        
    }
}
