﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnifiedAutomation.UaBase;
using UnifiedAutomation.UaClient;

namespace ViSys.Modules.OPC.UA
{
    class UAAssistant : IOPCModule
    {
        public event EventHandler<object> ReadingDone;
        public event EventHandler<object> Update;
        public event EventHandler<object> WritingDone;

        private Session mSession = null;
        private Subscription mSubscription = null;
        List<string> ReadingNodes = new List<string>();
        List<string> Writingnodes = new List<string>();
        List<int> hashCodes = new List<int>();  
        private BuiltInType m_TypeItem1;

        private UInt16 mNameSpaceIndex = 0;

        public void AddItems(List<string> items, OPCModuleFactory.OPCItemUse use)
        {
            if (use.Equals(OPCModuleFactory.OPCItemUse.READ)) this.ReadingNodes = items;
            if (use.Equals(OPCModuleFactory.OPCItemUse.WRITE)) this.Writingnodes = items;
        }

        public void Connect(string programID, string node)
        {

            try
            {
                Console.WriteLine(System.Reflection.Assembly.GetExecutingAssembly());
                ApplicationLicenseManager.AddProcessLicenses(System.Reflection.Assembly.GetExecutingAssembly(),
                    "Modules.OPC.UA.License.License.lic");

                this.mSession = new Session();
                // "opc.tcp://localhost:49320"
                this.mSession.Connect(node, SecuritySelection.None);
                ushort i;
                for (i = 0; i < this.mSession.NamespaceUris.Count; i++)
                {                    
                    if (this.mSession.NamespaceUris[i] == programID)
                    {                       
                        this.mNameSpaceIndex = i;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }                     
        }

        public object Read(string id)
        {
            ReadValueIdCollection nodesToRead = new ReadValueIdCollection();
            nodesToRead.Add(new ReadValueId()
            {
                NodeId = new NodeId(id, mNameSpaceIndex),
                AttributeId = Attributes.Value
            });

            List<DataValue> results = this.mSession.Read(nodesToRead);
            return results[0].Value;
        }

        public void ReadAsync(string id)
        {
            ReadValueIdCollection nodesToReadAsync = new ReadValueIdCollection();
            nodesToReadAsync.Add(new ReadValueId()
            {
                NodeId = new NodeId(id, mNameSpaceIndex),                
                AttributeId = Attributes.Value
            });

            List<object> lstObjects = new List<object>();
            lstObjects.Add(id);
            mSession.BeginRead(nodesToReadAsync, 0, TimestampsToReturn.Both, null, AsyncReadComplete, lstObjects);
        }

        private void AsyncReadComplete(IAsyncResult result)
        {
            List<DataValue> results = null;
            try
            {
                results = mSession.EndRead(result);                                                         
                if (ReadingDone != null) ReadingDone(this, results[0].Value); 
            }
            catch (Exception exception)
            {
                ExceptionDlg.Show("AsyncReadComplete failed", exception);
            }
        }

        public bool Write(object value, string id)
        {
            if (m_TypeItem1 == BuiltInType.Null)
            {
                // Read variable data types from server
                ReadDataTypes(id);
            }
            DataValue val1 = new DataValue();
            val1.Value = TypeUtils.Cast(value, m_TypeItem1);

            List<WriteValue> nodesToWrite = new List<WriteValue>();
            nodesToWrite.Add(new WriteValue()
            {
                NodeId = new NodeId(id, mNameSpaceIndex),
                AttributeId = Attributes.Value,
                Value = val1
            });

            List<StatusCode> results = mSession.Write(nodesToWrite);

            return true;
        }

        public void WriteAsync(object value, string id)
        {
            
            ReadDataTypes(id);            

            DataValue val1 = new DataValue();
            val1.Value = TypeUtils.Cast(value, m_TypeItem1);                       

            List<WriteValue> nodesToWrite = new List<WriteValue>();
            nodesToWrite.Add(new WriteValue()
            {
                NodeId = new NodeId(id, mNameSpaceIndex),
                AttributeId = Attributes.Value,
                Value = val1
            });

            List<object> lstObjects = new List<object>();
            lstObjects.Add(id);

            mSession.BeginWrite(nodesToWrite, AsyncWriteComplete, lstObjects);
        }

        private void AsyncWriteComplete(IAsyncResult result)
        {
            List<StatusCode> results = null;

            try
            {
                results = mSession.EndWrite(result);              
                List<object> lstObjects = (List<object>)result.AsyncState; 

                for (int i = 0; i<Writingnodes.Count; i++)
                {
                    if (lstObjects[0].ToString().Equals(Writingnodes[i]))
                    {
                        object[] a = { i+1, result.IsCompleted};                
                        if (WritingDone != null) WritingDone(this, a);
                        break;
                    }
                }                
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void ReadDataTypes(string id)
        {
            // Add the two variable NodeIds to the list of nodes to read
            // NodeId is constructed from
            // - the identifier text in the text box
            // - the namespace index collected during the server connect
            ReadValueIdCollection nodesToRead = new ReadValueIdCollection();
            nodesToRead.Add(new ReadValueId()
            {
                NodeId = new NodeId(id, mNameSpaceIndex),
                AttributeId = Attributes.DataType
            });

            // Read the datatypes
            List<DataValue> results = null;

            try
            {
                results = mSession.Read(nodesToRead, 0, TimestampsToReturn.Neither, null);

                // check the result code
                if (StatusCode.IsGood(results[0].StatusCode))
                {
                    // The node succeeded - save buildInType for later use
                    m_TypeItem1 = TypeUtils.GetBuiltInType((NodeId)results[0].Value);
                }
                else
                {
                    throw new Exception("Read datatype failed for item ");
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message + " " + exception.StackTrace);
            }
        }

        public void Monitor()
        {            
            this.mSubscription = new Subscription(this.mSession);
            this.mSubscription.PublishingEnabled = true;
            this.mSubscription.PublishingInterval = 100;
            this.mSubscription.DataChanged += new DataChangedEventHandler(DataChange);
            this.mSubscription.Create();
            
            List<MonitoredItem> monitoredItems = new List<MonitoredItem>();
            for(int i = 0; i < this.ReadingNodes.Count; i++)
            {
                monitoredItems.Add(new DataMonitoredItem(new NodeId(ReadingNodes[i], mNameSpaceIndex)));
            }
            this.mSubscription.CreateMonitoredItems(monitoredItems);
        }

        private void DataChange(Subscription subscription, DataChangedEventArgs e)
        {
            List<object[]> updatenodesandvalues = new List<object[]>();            

            foreach (DataChange change in e.DataChanges)
            {           
                for (int i = 0; i<ReadingNodes.Count; i++)
                {
                    if ((string)change.MonitoredItem.NodeId.Identifier == ReadingNodes[i])
                    {
                        object[] a = { i, change.Value.WrappedValue };
                        updatenodesandvalues.Add(a);
                        break;
                    }
                }
            }

            if (Update != null) Update(this, updatenodesandvalues);
        }
    }
}
