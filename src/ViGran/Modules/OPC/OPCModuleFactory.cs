﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViSys.Modules.OPC.DA;
using ViSys.Modules.OPC.UA;


namespace ViSys.Modules.OPC
{
    /// <summary>
    /// Implementação do padrão Factory - Permite a seleção de qual implementação da interface IOPCModule será usada.
    /// </summary>
    public class OPCModuleFactory
    {
        /// <summary>
        /// Cria uma interface para as classes assistentes que implementam os métodos nativos das APIS dos padrões OPC.
        /// </summary>
        /// <param name="standard"> Nome da API.</param>
        public virtual IOPCModule Create(OPCStandard standard)
        {
            IOPCModule connection = null;

            switch (standard)
            {
                case OPCStandard.DA:
                    connection = new DAAssistant();
                    break;
                case OPCStandard.UA:
                    connection = new UAAssistant();
                    break;
            }

            return connection;
        }

        /// <summary>
        /// Enumerador com os nomes dos padrões OPC.
        /// </summary>
        public enum OPCStandard
        {
            DA,
            UA
        }

        /// <summary>
        /// Enumerador com a finalidade da Tag.
        /// </summary>
        public enum OPCItemUse
        {
            READ,
            WRITE
        }
    }
}
