﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViSys.Modules.OPC.DA
{
    class DAAssistant : IOPCModule
    {
        public event EventHandler<object> Update;
        public event EventHandler<object> WritingDone;
        public event EventHandler<object> ReadingDone;

        private OPCGroup ConnectedReadGroup;
        private OPCGroup ConnectedWriteGroup;
        private OPCServer ConnectedOPCServer;
        
        private Array OPCItemIDsRead = Array.CreateInstance(typeof(string), 10);
        private Array ItemServerHandlesRead = Array.CreateInstance(typeof(Int32), 10);
        private Array ItemServerErrorsRead = Array.CreateInstance(typeof(Int32), 10);
        private Array ClientHandlesRead = Array.CreateInstance(typeof(Int32), 10);
        private Array ItemServerReadValues = Array.CreateInstance(typeof(string), 10);

        private Array OPCItemIDsWrite = Array.CreateInstance(typeof(string), 10);
        private Array ItemServerHandlesWrite = Array.CreateInstance(typeof(Int32), 10);
        private Array ItemServerErrorsWrite = Array.CreateInstance(typeof(Int32), 10);
        private Array ClientHandlesWrite = Array.CreateInstance(typeof(Int32), 10);
      

        private Array RequestedDataTypes = Array.CreateInstance(typeof(Int16), 10);
        private Array AccessPaths = Array.CreateInstance(typeof(string), 10);
        private Array WriteItems = Array.CreateInstance(typeof(object), 10);


        private int ReadItemCount = 0;
        private int WriteItemCount = 0;

        public void AddItems(List<string> items, OPCModuleFactory.OPCItemUse use)
        {

            if (use.Equals(OPCModuleFactory.OPCItemUse.READ))
            {
                for(int i = 0; i < items.Count; i++)
                {
                    OPCItemIDsRead.SetValue(items[i], i+1);
                    ClientHandlesRead.SetValue(i+1, i+1);             
                    this.ReadItemCount++;
                }

                ConnectedReadGroup.OPCItems.AddItems(ReadItemCount, ref OPCItemIDsRead, ref ClientHandlesRead, out ItemServerHandlesRead, 
                    out ItemServerErrorsRead, RequestedDataTypes, AccessPaths);
            }

            if (use.Equals(OPCModuleFactory.OPCItemUse.WRITE))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    OPCItemIDsWrite.SetValue(items[i], i + 1);
                    ClientHandlesWrite.SetValue(i+1, i+1);
                    this.WriteItemCount++;
                }

                ConnectedWriteGroup.OPCItems.AddItems(WriteItemCount, ref OPCItemIDsWrite, ref ClientHandlesWrite, out ItemServerHandlesWrite,
                    out ItemServerErrorsWrite, RequestedDataTypes, AccessPaths);
            }


        }

        public void Connect(string programID, string node)
        {
            ConnectedOPCServer = new OPCAutomation.OPCServer();
            ConnectedOPCServer.Connect(programID, node);


            ConnectedReadGroup = ConnectedOPCServer.OPCGroups.Add("ReadGroup");            
            ConnectedReadGroup.AsyncReadComplete += new DIOPCGroupEvent_AsyncReadCompleteEventHandler(AsyncReadComplete);
            //ConnectedReadGroup.AsyncWriteComplete += new DIOPCGroupEvent_AsyncWriteCompleteEventHandler(AsyncWriteComplete);
            ConnectedReadGroup.UpdateRate = 100;
            ConnectedReadGroup.IsSubscribed = ConnectedReadGroup.IsActive;

            ConnectedWriteGroup = ConnectedOPCServer.OPCGroups.Add("WriteGroup");
            //ConnectedWriteGroup.AsyncReadComplete += new DIOPCGroupEvent_AsyncReadCompleteEventHandler(AsyncReadComplete);
            ConnectedWriteGroup.AsyncWriteComplete += new DIOPCGroupEvent_AsyncWriteCompleteEventHandler(AsyncWriteComplete);
            ConnectedWriteGroup.UpdateRate = 100;
            ConnectedWriteGroup.IsSubscribed = ConnectedWriteGroup.IsActive;
        }

        private void AsyncWriteComplete(int TransactionID, int NumItems, ref Array ClientHandles, ref Array Errors)
        {   
            for (int i = 1; i <= ClientHandles.Length; i++)
            {
                if ((int)ClientHandles.GetValue(i) != 0)
                {
                    if ((int)Errors.GetValue(i) == 0)
                    {
                        object[] a = { ClientHandles.GetValue(i), true };
                        if (WritingDone != null) WritingDone(this, a);
                    }
                    else
                    {
                        object[] a = { ClientHandles.GetValue(i), false };
                        if (WritingDone != null) WritingDone(this, a);
                    }
                }              
            }
        }

        private void AsyncReadComplete(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, 
            ref Array Qualities, ref Array TimeStamps, ref Array Errors)
        {
            if (ReadingDone != null) ReadingDone(this, ItemValues.GetValue(1));
        }

        private void DataChange(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, 
            ref Array Qualities, ref Array TimeStamps)
        {
            List<object[]> mylist = new List<object[]>();

            for(int i = 1; i <= ClientHandles.Length; i++)
            {
                if(ClientHandles.GetValue(i) != null)
                {
                    object[] a = { ClientHandles.GetValue(i), ItemValues.GetValue(i) };
                    mylist.Add(a);
                }
            }             

            if (Update != null) Update(this, mylist);
        }        

        public object Read(string id)
        {
            Array errors;
            object qualities = new object(); 
            object timestamps = new object();   
            
            Array localItemServerHandles = Array.CreateInstance(typeof(Int32), 2); 
            Array localItemServerReadValues = Array.CreateInstance(typeof(string), 2);

            int i = 1;
            for ( ; i < OPCItemIDsRead.Length; i++)
            {
                if (OPCItemIDsRead.GetValue(i).ToString().Equals(id))
                {
                    localItemServerHandles.SetValue(ItemServerHandlesRead.GetValue(i), 1);
                    break;
                }                
            }

            ConnectedReadGroup.SyncRead((short)OPCAutomation.OPCDataSource.OPCDevice, i, ref localItemServerHandles, 
                out localItemServerReadValues, out errors, out qualities, out timestamps); 

            return localItemServerReadValues.GetValue(1);
        }

        public void ReadAsync(string id)
        {            
            Array Errors;
            int CancelId;

            Array localItemServerHandles = Array.CreateInstance(typeof(Int32), 2);

            int i = 1;
            for ( ; i < OPCItemIDsRead.Length; i++)
            {
                if (OPCItemIDsRead.GetValue(i).ToString().Equals(id))
                {
                    localItemServerHandles.SetValue(ItemServerHandlesRead.GetValue(i), 1);
                    break;
                }
            }
            ConnectedReadGroup.AsyncRead(i, ref localItemServerHandles, out Errors, 0, out CancelId);            
        }

        public bool Write(object value, string id)
        {
            Array localWriteItems = Array.CreateInstance(typeof(object), 10);

            int i = 1;
            for ( ; i < OPCItemIDsWrite.Length; i++)
            {
                if (OPCItemIDsWrite.GetValue(i).ToString().Equals(id))
                {
                    localWriteItems.SetValue(value, i);
                    break;
                }
            }
                        
            ConnectedWriteGroup.SyncWrite(i, ref ItemServerHandlesWrite, ref localWriteItems, out ItemServerErrorsWrite);
            return true;
        }

        public void WriteAsync(object value, string id)
        {            
            Array localWriteItems = Array.CreateInstance(typeof(object), 10);

            int i = 1;
            for ( ; i <= OPCItemIDsWrite.Length; i++)
            {
                if (OPCItemIDsWrite.GetValue(i).ToString().Equals(id))
                {
                    localWriteItems.SetValue(value, i);
                    break;
                }
            }
            int Second;
            ConnectedWriteGroup.AsyncWrite(i, ref ItemServerHandlesWrite, ref localWriteItems, out ItemServerErrorsWrite, 
                DateTime.Now.Second, out Second);   
        }

        public void Monitor()
        {
            ConnectedReadGroup.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(DataChange);
            //ConnectedWriteGroup.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(DataChange);
        }
    }
}
