﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AVT.VmbAPINET;
using ViSys.Modules.Camera.Common;

namespace ViSys.Modules.Camera.VimbaAPI
{
    class VimbaAssistant : VimbaHelper, ICameraModule
    {   
        private Bitmap bp = null;
        private List<Bitmap> lbps = new List<Bitmap>();
        private static readonly int timeOut = 500;
        private static readonly Frame frame = null;
        private static Frame[] frames = new Frame[30];

        public void GetFramesAsync()
        {
            this.StartContinuousImageAcquisition(NewFrameReceived);
        }       
        
        private void NewFrameReceived(object sender, FrameEventArgs args)
        {
            this.bp = new Bitmap(args.Image);
            VimbaHelper.ImageInUse = true;
            RaiseNewFrameEvent(new SysImage(this.bp));
        }     

        public void Open(string id)
        {
            this.OpenCamera(id);     
        }   

        public Bitmap Snapshot()
        {
            this.m_Camera.AcquireMultipleImages(ref frames, timeOut);
            foreach (Frame f in frames)
            {
                this.bp = new Bitmap((int)f.Width, (int)f.Height, PixelFormat.Format32bppRgb);
                f.Fill(ref this.bp);
                this.lbps.Add(this.bp);      
            }
            return this.lbps[lbps.Count-1];
        }
               
        public void LoadSettings(string path)
        {
            this.loadCameraSettings(path);      
        }

        public VimbaAssistant()
        {    
            Vimba vimba = new Vimba();
            vimba.Startup();
            this.m_Vimba = vimba;
        }  
    }
}
