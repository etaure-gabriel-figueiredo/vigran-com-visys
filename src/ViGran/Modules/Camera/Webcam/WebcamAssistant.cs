﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Windows.Forms;
using System.Threading;
using ViSys.Modules.Camera.Common;

namespace ViSys.Modules.Camera.Webcam
{
    class WebcamAssistant : MediaManager, ICameraModule
    {
        public Mat m = new Mat();

        private Capture cam;

        public void GetFramesAsync()
        {
            cam.ImageGrabbed += LoopCapture;
        }

        public void LoadSettings(string path)
        {
            //throw new NotImplementedException();
        }

        public void Open(string id)
        {
            cam = new Capture(Convert.ToInt32(id));
            cam.Start();
        }

        public Bitmap Snapshot()
        {
            return this.m.Bitmap;
        }

        private void LoopCapture(object sender, EventArgs e)
        {
            cam.Retrieve(m);

            if (!m.IsEmpty)
            {
                RaiseNewFrameEvent(new SysImage(m.Bitmap));
            }
        }
    }
}
