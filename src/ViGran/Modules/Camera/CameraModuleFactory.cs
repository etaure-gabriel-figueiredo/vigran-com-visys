﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViSys.Modules.Camera.VimbaAPI;
using ViSys.Modules.Camera.JAIAPI;
using ViSys.Modules.Camera.SaperaAPI;
using ViSys.Modules.Camera.XenethAPI;
using ViSys.Modules.Camera.Webcam;
using ViSys.Modules.Camera.Files;

namespace ViSys.Modules.Camera
{
    /// <summary>
    /// Implementação do padrão Factory - Permite a seleção de qual implementação da interface ICameraModule será usada.
    /// </summary>
    public class CameraModuleFactory
    {
        /// <summary>
        /// Cria uma interface para as classes assistentes que implementam os métodos nativos das APIS de câmeras.
        /// </summary>
        /// <param name="provider"> Nome da API - Use WEBCAM para a webcam onboard ou câmeras conectadas via USB.</param>
        public virtual ICameraModule Create(CameraAPIProvider provider)
        {                            
            ICameraModule connection = null;
           
            switch (provider)
            {
                case CameraAPIProvider.VIMBA:
                    connection = new VimbaAssistant();
                    break;
                case CameraAPIProvider.JAI:
                    connection = new JAIAssistant();
                    break;
                case CameraAPIProvider.SAPERA:
                    connection = new SaperaAssistant();
                    break;
                case CameraAPIProvider.XENETH:
                    connection = new XenethAssistant();
                    break;
                case CameraAPIProvider.WEBCAM:
                    connection = new WebcamAssistant();
                    break;
                case CameraAPIProvider.FILES:
                    connection = new FilesAssistant();
                    break;
            }
                        
            return connection;
        }

        /// <summary>
        /// Enumerador com os nomes das APIS implementadas até o momento por este módulo.
        /// </summary>
        public enum CameraAPIProvider
        {
            VIMBA,
            JAI,
            SAPERA, 
            XENETH,
            WEBCAM,
            FILES
        }
    }
}
