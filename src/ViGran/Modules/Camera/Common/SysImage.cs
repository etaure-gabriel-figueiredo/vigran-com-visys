﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ViSys.Modules.Camera.Common
{
    /// <summary>
    /// Classe para as imagens do ViSys.
    /// </summary>
    public class SysImage
    {
        bool isThermoImage = false;
        public Bitmap Image;
        //public Image<Bgr, Byte> ImageCV;
        //public Mat mat;
        public double Temp;
        public string Time;
        public Mat ImageMat;
        int width;
        int height;
        int[] raw;
        ushort[] imageBuffer = null;
        int acumulador;
        double tempMedia;
        string name;
        double
            max_,
            min_,
            max_i,
            min_i,
            ri;


        public SysImage(int w, int h, double mv, double min_v = 0)
        {
            this.width = w;
            this.height = h;
            this.max_ = mv;
            this.min_ = min_v;
            this.imageBuffer = new ushort[w * h];
            raw = new int[w * h];
            this.isThermoImage = true;
        }

        public SysImage(Bitmap img)
        {
            this.Image = img;
            //this.imagecv = new image<bgr, byte>(img);
            //this.imagemat = imagecv.mat;
            this.isThermoImage = false;
        }

        public void SetNormalParams(double ma_, double mi_)
        {
            this.max_i = ma_;
            this.min_i = mi_;
            this.ri = ma_ - mi_;
        }

        public int[] NormalizeImage(ushort[] buff)
        {
            for (int i = 0; i < buff.Length; i++)
            {
                raw[i] = (int)(((buff[i] - this.min_i) / this.ri) * this.max_);
            }
            return raw;
        }

        public SysImage DeepCopy()
        {
            SysImage other = (SysImage)this.MemberwiseClone();
            other.Image = new Bitmap(this.Image);
            return other;
        }

        public void Dispose()
        {
            if (this.Image != null) this.Image.Dispose();
        }
    }
}
