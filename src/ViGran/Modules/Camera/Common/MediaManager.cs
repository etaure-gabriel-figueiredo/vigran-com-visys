﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Threading;
using System.IO;
using System.Timers;
using System.Drawing.Imaging;
using WPD_V1;

namespace ViSys.Modules.Camera.Common
{
    /// <summary>
    /// Disponibiliza funcionalidades para arquivos de vídeo.
    /// </summary>
    public class MediaManager
    {
        public VideoWriter writer;
        private Thread mVideoProcessingVideo = null;
        public static bool signal = true;
        Mat m;
        Capture vdCap;
        string ImageFilesDirectory;
        System.Timers.Timer mtimer;
        bool saveFlag = false;

        /*Gerenciamento de disco*/
        private Boolean flagFreeSpaceWarning = false;
        float PercFreeLastCheck = float.NaN;

        //public event EventHandler<SysImage> NewFrame;
        public event EventHandler<SysImage> NewFrame;

        internal void AddFrameToVideo(object sender, SysImage e)
        {
            //try
            //{
            //    if (this.writer.IsOpened) this.writer.Write(e.ImageMat);
            //}

            //catch (Exception error)
            //{
            //    Console.WriteLine(error.Message + " " + error.StackTrace);
            //}
        }

        public void WriteVideo(int fps, Size size, bool isColor)
        {
            try
            {
                string timeNow = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                string currentPath = Directory.GetCurrentDirectory();
                string getToLocalSave = @"..\..\" + @"Modules\Camera\Common\VideoFiles\" + "ViSys-" + timeNow + ".mp4";
                string path = Path.GetFullPath(Path.Combine(currentPath, getToLocalSave));
                int fourcc = VideoWriter.Fourcc('M', 'P', '4', '2');
                this.writer = new VideoWriter(path, fourcc, fps, size, isColor);
                //this.NewFrame += new EventHandler<SysImage>(this.AddFrameToVideo);
            }

            catch (Exception error)
            {
                Console.WriteLine(error.Message + " " + error.StackTrace);
            }
        }

        public void StopWritingVideo()
        {
            //this.NewFrame -= new EventHandler<SysImage>(this.AddFrameToVideo);
            //if (this.writer.IsOpened)
            //{
            //    Thread.Sleep(500);
            //    this.writer.Dispose();
            //}
        }

        public void RaiseNewFrameEvent(SysImage si)
        {
            if (signal)
            {
                //if (NewFrame != null) NewFrame(this, new SysImage(bp));
                if (NewFrame != null) NewFrame(this, si);
            }
        }

        public void ProcessVideoFiles(string path)
        {
            if (this.mVideoProcessingVideo == null)
            {
                //string currentPath = Directory.GetCurrentDirectory();
                //string getToLocalSave = @"..\..\" + @"Modules\Camera\Common\VideoFiles\";
                //string path = Path.GetFullPath(Path.Combine(currentPath, getToLocalSave));

                this.mVideoProcessingVideo = new Thread(() =>
                {
                    try
                    {
                        //foreach (var fi in new DirectoryInfo(@"" + path).GetFiles().OrderBy(x => x.LastWriteTime))
                        //{
                        //Capture vdCap = new Capture(fi.FullName);  
                        while (true)
                        {
                            vdCap = new Capture(path);
                            bool isReading = true;
                            while (isReading)
                            {
                                m = vdCap.QueryFrame();
                                if (m != null)
                                {
                                    ////myMethodname(m.Bitmap);
                                    if (NewFrame != null) NewFrame(this, new SysImage(m.Bitmap));
                                    ////Thread.Sleep(50);
                                }
                                else isReading = false;
                            }
                        }
                        //}
                        this.mVideoProcessingVideo = null;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                });
                this.mVideoProcessingVideo.Name = "VideoAccess";
                this.mVideoProcessingVideo.IsBackground = true;
                this.mVideoProcessingVideo.Start();
            }
            else this.mVideoProcessingVideo.Resume();
        }

        public void StopProcessingVideoFiles()
        {
            if (this.mVideoProcessingVideo != null)
            {
                this.mVideoProcessingVideo.Suspend();
            }
        }

        public void SaveImageRoutine(string path, int ms)
        {
            this.ImageFilesDirectory = path;
            this.mtimer = new System.Timers.Timer();
            this.mtimer.Interval = ms;
            this.mtimer.AutoReset = true;
            this.mtimer.Elapsed += new System.Timers.ElapsedEventHandler(SetSaveFlag);
            this.mtimer.Enabled = true;
            //this.NewFrame += new EventHandler<SysImage>(SaveImageMethod);
            this.NewFrame += new EventHandler<SysImage>(SaveImageMethod);
        }

        private void SetSaveFlag(object sender, ElapsedEventArgs e)
        {
            this.saveFlag = true;
        }

        private void SaveImageMethod(object sender, SysImage e)
        //private void SaveImageMethod(object sender, SysImage e)
        {
            if (this.saveFlag)
            {
                try
                {
                    //CheckFreeSpace();
                    //saveImageCompression(this.ImageFilesDirectory, (Bitmap)e.Image.Clone(), 50L);
                    saveImageCompression(this.ImageFilesDirectory, e.DeepCopy(), 50L);
                    //saveImageCompression(this.ImageFilesDirectory, e.Image, 50L);
                    //e.Image.Dispose();
                    this.saveFlag = false;
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void StopSaveImageRoutine()
        {
            this.NewFrame -= new EventHandler<SysImage>(SaveImageMethod);
            this.mtimer.Elapsed -= new System.Timers.ElapsedEventHandler(SetSaveFlag);
            this.mtimer.Enabled = false;
        }


        public void saveImageCompression(string folderName, SysImage image, Int64 compressionQuality)
        {
            ImageCodecInfo myImageCodecInfo = GetEncoderInfo("image/jpeg");
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Compression;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, compressionQuality);
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            myEncoderParameters.Param[0] = myEncoderParameter;
            image.Image.Save(folderName + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".jpg", myImageCodecInfo, myEncoderParameters);
        }

        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private void CheckFreeSpace()
        {

            /* Variável com a percentagem de disco livre */
            float PercFree = 0.00f;

            try
            {
                /* Busca todos os discos do sistema*/
                DriveInfo[] allDrives = DriveInfo.GetDrives();

                /* Recupera caminho completo da pasta de imagens zipadas */
                string PathName = WPD_V1.Properties.Settings.Default.SavePath;

                /* Percorre todos os discos encontrados */
                foreach (DriveInfo d in allDrives)
                {
                    /* Encontra o disco onde as imagens são salvas */
                    if (d.Name[0].Equals(PathName[0]) && d.IsReady == true)
                    {
                        /* Calcula percentagem de disco livre */
                        PercFree = (100 * (float)d.TotalFreeSpace / d.TotalSize);

                        /* Atualiza flag para registro em log */
                        if (PercFreeLastCheck != float.NaN && PercFree > PercFreeLastCheck)
                        {
                            flagFreeSpaceWarning = false;
                        }

                        /* Realiza check de alarme de disco cheio */
                        if (PercFree > WPD_V1.Properties.Settings.Default.hdPFSOverwrite &&
                            PercFree < WPD_V1.Properties.Settings.Default.hdPFSWarning &&
                            flagFreeSpaceWarning == false)
                        {
                            //Logger.Warn("Capacidade do disco próxima do limite configurado.");
                            flagFreeSpaceWarning = true;
                        }

                        /* Deleta arquivos antigos */
                        if (PercFree < WPD_V1.Properties.Settings.Default.hdPFSOverwrite)
                        {
                            /* Deleta os arquivos mais antigos */
                            foreach (var fi in new DirectoryInfo(@"" + WPD_V1.Properties.Settings.Default.SavePath).GetFiles().OrderBy(x => x.LastWriteTime).Take(WPD_V1.Properties.Settings.Default.FilesToDelete))
                            {
                                fi.Delete();
                                //Logger.Warn("Quantidade de disco ocupada atingida. Foi deletada a quantidade de arquivos configurada.");
                            }
                        }

                        /* Atualiza última percentagem salva */
                        PercFreeLastCheck = PercFree;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
        }

    }
}
