﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xenics.Xeneth;
using ViSys.Modules.Camera.Common;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ViSys.Modules.Camera.XenethAPI
{
    class XenethAssistant : MediaManager, ICameraModule
    {
        private XCamera cam;
        private Bitmap bp;
        private static SemaphoreSlim signal;
        private bool waitingForSignal = false;
        bool NormalImages = false;

        ushort[] frameBuffer = null;  
        double[] tempLUT = null;
        UInt32 fltThermography = 0;
        Image<Gray, byte> im;
        SysImage ti;
        Bitmap bm;
        BitmapData bd;

        bool isLoopActive = false;

        public void Open(string id)
        {
            try
            {
                this.cam = new XCamera(id);                
            }
            catch (Exception ex)
            {

            }
        }

        public void GetFramesAsync()
        {
            Task.Run(() => this.LoopCapture());
        }

        private void LoopCapture()
        {
            try
            {
                isLoopActive = true;

                bm = new Bitmap((int)this.cam.Width, (int)this.cam.Height, PixelFormat.Format32bppRgb);
                bd = null;

                bool proc = false;

                double aux = 0;
                uint temp;
                int n;
                int pt;

                fltThermography = this.cam.QueueFilter("Thermography", "celsius");

                if (fltThermography > 0)
                {
                    // Determine native frame size and resolution
                    UInt32 frameSize = this.cam.FrameSize;
                    int width = (int)this.cam.Width;
                    int height = (int)this.cam.Height;
                    UInt32 mv = this.cam.MaxValue;

                    // Initialize the 16-bit buffer.
                    frameBuffer = new ushort[frameSize / 2];
                    ushort[] auxBuffer = new ushort[frameSize / 2];
                    int[] editBuffer = new int[frameSize / 2];


                    tempLUT = new double[mv + 1];

                    for (UInt32 x = 0; x < mv; x++)
                    {
                        this.cam.FLT_ADUToTemperature(fltThermography, x, ref tempLUT[x]);
                    }

                    ti = new SysImage(width, height, mv);

                    double
                        max_ = 65535,
                        min_i = 0,
                        ri = 1;

                    this.cam.StartCapture();

                    while (true)
                    {
                        if (this.cam.Capturing) // When the camera is capturing ...
                        {
                            this.cam.GetFrame(FrameType.Native, XGetFrameFlags.Blocking, frameBuffer, frameSize);

                            ti.Time = DateTime.Now.ToString();

                            temp = frameBuffer.Max();
                            aux = tempLUT[temp];

                            n = frameBuffer.Length;
                            //temp = (uint)ti.calcMediaPix(frameBuffer);

                            /* ao clicar no botão para normalizar, dee-se recalcular os parâmetros de normalização */
                            //if (this.NormalImages)
                            if (true)
                            {
                                ti.SetNormalParams(frameBuffer.Max(), frameBuffer.Min());

                                proc = true;

                                this.NormalImages = false;
                            }
                                                        
                            if (proc)
                            {
                                editBuffer = ti.NormalizeImage(frameBuffer);

                                bd = bm.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
                                Marshal.Copy(editBuffer, 0, bd.Scan0, n);
                                bm.UnlockBits(bd);
                                bd = null;

                                //Object Image to convert the created bitmap in grayscale
                                this.im = new Image<Gray, byte>(bm);

                                //Thermical Bitmap
                                ti.Image = im.Bitmap;
                                //Temperature value
                                ti.Temp = aux;

                                //if (NewFrame != null) NewFrame(this, bm);
                                //if (sample != null) sample(this, ti);

                                
                                while (true)
                                {
                                    try
                                    {
                                        this.cam.GetFrame(FrameType.Native, 0, IntPtr.Zero, 0);
                                    }
                                    catch (XenethException ex)
                                    {
                                        if (ex.ErrorCode == XErrorCodes.NoFrame)
                                            break;
                                    }
                                }
                                if (this.waitingForSignal)
                                {
                                    Task t = Task.Delay(500);
                                    t.Wait();
                                    this.bp = bm;
                                    break;
                                }
                                else
                                {
                                    RaiseNewFrameEvent(new SysImage(ti.Image));
                                }                                
                            }                            
                        }

                    }

                }
            }
            catch(Exception error)
            {
                Console.WriteLine(error.Message + " " + error.StackTrace);
            }
        }

        public Bitmap Snapshot()
        {
            if (isLoopActive)
            {
                return this.ti.Image;
            }
            else
            {
                this.waitingForSignal = true;
                Task t = Task.Run(() => this.LoopCapture());
                t.Wait();
                this.waitingForSignal = false;
                return this.ti.Image;
            }
        }

        public void LoadSettings(string path)
        {
            //this.cam.LoadSettings(path);
            this.cam.LoadCalibration(path, XLoadCalibrationFlags.StartSoftwareCorrection);            
        }
    }
}
