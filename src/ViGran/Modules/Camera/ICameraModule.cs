﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViSys.Modules.Camera.Common;

namespace ViSys.Modules.Camera
{
    /// <summary>
    /// Interface para as classes assistentes JAIAssistant, SaperaAssistant, VimbaAssistant, XenethAssistant e WebcamAssistant.
    /// </summary>
    public interface ICameraModule
    {
        /// <summary>
        /// Evento que é disparado quando há um novo frame capturado pela API da câmera em uso.
        /// </summary>
        event EventHandler<SysImage> NewFrame;

        /// <summary>
        /// Conecta-se a câmera passada como parâmetro.
        /// </summary>
        /// <param name="id"> Nome/ID da câmera. </param>
        void Open(string id);

        /// <summary> 
        /// Captura um frame. 
        /// </summary>
        /// <returns> 
        /// Retorna um frame no formato Bitmap. 
        /// </returns>
        Bitmap Snapshot();

        /// <summary>
        /// Captura frames de forma assíncrona e dispara o evento NewFrame.
        /// </summary>
        void GetFramesAsync();   

        /// <summary>
        /// Carrega o arquivo de configuração da câmera.
        /// </summary>
        /// <param name="path"> Caminho para o arquivo de configuração. </param>
        void LoadSettings(string path);

        /// <summary>
        /// Cria arquivo de vídeo no formato MP4. É necessário chamar o método StopWritingVideo() para concluir.
        /// </summary>
        /// <param name="fps"> FPS do arquivo de vídeo. </param>
        /// <param name="size"> Dimensão do frame. </param>
        /// <param name="isColor"> Vídeo com cor. </param>
        void WriteVideo(int fps, Size size, bool isColor);

        /// <summary>
        /// Conclui o arquivo de vídeo sendo gravado ao utilizar o método WriteVideo().
        /// </summary>
        void StopWritingVideo();

        /// <summary>
        /// Cria uma thread para processar cada frame dos arquivos de video. Se a thread for pausada com o método 
        /// StopProcessingVideoFiles(), basta usar este método novamente para retornar.
        /// </summary>
        /// <param name="myMethodname">
        /// Método que irá processar os frames. Deve receber como parâmetro um bitmap e retornar um booleano.
        /// </param>
        void ProcessVideoFiles(string path);

        /// <summary>
        /// Pausa a thread criada para processar frames dos arquivos de vídeo.
        /// </summary>
        void StopProcessingVideoFiles();
    }
}
