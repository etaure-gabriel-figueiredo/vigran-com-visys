﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Jai_FactoryDotNET;
using static Jai_FactoryDotNET.Jai_FactoryWrapper;
using ViSys.Modules.Camera.Common;

namespace ViSys.Modules.Camera.JAIAPI
{
    class JAIAssistant : MediaManager, ICameraModule
    {        
        private CCamera mcamera = null;
        private CFactory mfactory;
        Jai_FactoryWrapper.ImageInfo buffer16Nir, buffer32ArgbNir;
        Bitmap bp, bp2;
        private uint imageBufferSize = 10;
        private EFactoryError error;
        private static SemaphoreSlim signal;
        private bool waitingForSignal = false;  

        public JAIAssistant()
        {
            CFactory Factory = new CFactory();
            this.error = Factory.Open("");
            Factory.UpdateCameraList(CFactory.EDriverType.FilterDriver);
            this.mfactory = Factory; 
        }

        public void GetFramesAsync()
        {            
            this.mcamera.NewImageDelegate += new Jai_FactoryWrapper.ImageCallBack(NewFrameReceived);
            this.mcamera.StartImageAcquisition(false, imageBufferSize);
        }

        private void NewFrameReceived(ref ImageInfo ImageInfo)
        {  
            if (buffer16Nir.ImageBuffer == IntPtr.Zero)
            {
                Jai_FactoryWrapper.J_Image_Malloc(ref ImageInfo, ref buffer16Nir);
                Jai_FactoryWrapper.J_Image_MallocDIB(ref ImageInfo, ref buffer32ArgbNir);

                //this.bp2 = new Bitmap((int)ImageInfo.SizeX, //width
                //                        (int)ImageInfo.SizeY, //height
                //                        (int)(ImageInfo.SizeX * 2), //stride
                //                        PixelFormat.Format16bppGrayScale,
                //                        buffer16Nir.ImageBuffer
                //                        );

                this.bp = new Bitmap((int)ImageInfo.SizeX, //width
                                        (int)ImageInfo.SizeY, //height
                                        (int)(ImageInfo.SizeX * 4), //stride
                                        PixelFormat.Format32bppArgb,
                                        buffer32ArgbNir.ImageBuffer
                                        );
            }

            Jai_FactoryWrapper.EFactoryError error;

            if (ImageInfo.ImageSize > 0)
            {
                error = Jai_FactoryWrapper.J_Image_FromRawToImage(ref ImageInfo, ref buffer16Nir,
                         4096u, 4096u, 4096u);

                error = Jai_FactoryWrapper.J_Image_FromRawToDIB(ref ImageInfo, ref buffer32ArgbNir,
                        Jai_FactoryWrapper.EColorInterpolationAlgorithm.BayerFastMultiprocessor
                        , 4096u, 4096u, 4096u);
            }

            //Snap
            //this.tcs.SetResult(true);  
            if (this.waitingForSignal)
            {
                signal.Release();
            }
            else
            {
                RaiseNewFrameEvent(new SysImage(this.bp));
            }
        }

        public void LoadSettings(string path)
        {            
            while (this.mcamera.GetScriptProgress() != -1);
            Thread.Sleep(TimeSpan.FromSeconds(2));
            this.mcamera.RunScript(path);
        }

        public void Open(string id)
        {
            foreach (CCamera cam in this.mfactory.CameraList)
            {
                //if (cam.UserName.Equals(id))
                //    this.mcamera = cam;
                this.mcamera = cam;
            }
            this.error = this.mcamera.Open();
        }

        public Bitmap Snapshot()
        {
            signal = new SemaphoreSlim(0, 1);
            this.mcamera.NewImageDelegate += new Jai_FactoryWrapper.ImageCallBack(NewFrameReceived);
            this.mcamera.StartImageAcquisition(false, imageBufferSize);

            this.waitingForSignal = true;
            signal.Wait();
            this.waitingForSignal = false;
            return this.bp;
        }        
    }
}
