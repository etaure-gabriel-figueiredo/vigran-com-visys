﻿using DALSA.SaperaLT.SapClassBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViSys.Modules.Camera.SaperaAPI
{
    class MyProcessing : SapProcessing
    {
        private SapColorConversion m_ColorConv;
        // Constructor

        public MyProcessing(SapBuffer pBuffers, SapColorConversion pColorConv, SapProcessingDoneHandler pCallback)
        : base(pBuffers)
        {
            base.ProcessingDoneEnable = true;
            base.ProcessingDone += pCallback;
            m_ColorConv = pColorConv;

            //m_ColorConv.Method = SapColorConversion.ColorMethod.Method1;
            //m_ColorConv.Align = SapColorConversion.ColorAlign.RGGB;
            //m_ColorConv.OutputFormat = SapFormat.RGB8888;

            base.ProcessingDoneContext = pColorConv.OutputBuffer;

            //base.ProcessingDoneContext = pBuffers;
        }

        public override bool Run()
        {
            if (m_ColorConv != null && m_ColorConv.Initialized && m_ColorConv.SoftwareEnabled
                && m_ColorConv.SoftwareSupported && m_ColorConv.Enabled)
            {
                m_ColorConv.Convert(base.Index);
            }

            return true;
        }
    }
}
