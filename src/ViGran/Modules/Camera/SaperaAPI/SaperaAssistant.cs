﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DALSA.SaperaLT.SapClassBasic;
using Emgu.CV.Util;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using ViSys.Modules.Camera;
using ViSys.Modules.Camera.Common;

namespace ViSys.Modules.Camera.SaperaAPI
{
    class SaperaAssistant : MediaManager, ICameraModule
    {
        SapColorConversion m_ColorConv;
        SapProcessing m_Pro;

        SapAcqDevice acqDevice = null;
        SapBuffer buffers = null;

        SapTransfer xfer = null;
        SapLocation serverLoc = null;

        private Bitmap bp = null;

        Image<Gray, Byte> image = null;
        Image<Bgr, Byte> color = null;

        int index = 0;

        string configFil = "";

        private static SemaphoreSlim signal = new SemaphoreSlim(0, 1);
        private bool waitingForSignal = false;

        public void GetFramesAsync()
        {
            this.xfer.Grab();
        }

        private void NewFrameReceived(object sender, SapXferNotifyEventArgs e)
        {
            if (m_ColorConv.SoftwareSupported)
            {
                m_Pro.ExecuteNext();
            }

            else
            {
                SapBuffer buf = e.Context as SapBuffer;
                IntPtr address = new IntPtr();
                buf.GetAddress(out address);
                int bitsPerPixel = 8 * buf.BytesPerPixel;
                int stride = 4 * ((buf.Width * bitsPerPixel + 31) / 32);

                this.bp = new Bitmap(buf.Width, buf.Height, stride, System.Drawing.Imaging.PixelFormat.Format8bppIndexed, address);

                ColorPalette palette = this.bp.Palette;
                Color[] entries = palette.Entries;
                for (int i = 0; i < entries.Length; i++)
                {
                    int c = i * 256 / entries.Length;
                    entries[i] = Color.FromArgb(c, c, c);
                }
                this.bp.Palette = palette;

                RaiseNewFrameEvent(new SysImage(this.bp));
            }

            if (this.waitingForSignal)
            {
                signal.Release();
            }
        }

        public void LoadSettings(string path)
        {
            this.configFil = path;
        }

        public void Open(string id)
        {
            this.serverLoc = new SapLocation(id, this.index);
            this.acqDevice = new SapAcqDevice(this.serverLoc, this.configFil);
            if (SapBuffer.IsBufferTypeSupported(this.serverLoc, SapBuffer.MemoryType.ScatterGather))
                this.buffers = new SapBufferWithTrash(2, this.acqDevice, SapBuffer.MemoryType.ScatterGather);
            else
                this.buffers = new SapBufferWithTrash(2, this.acqDevice, SapBuffer.MemoryType.ScatterGatherPhysical);

            this.xfer = new SapAcqDeviceToBuf(this.acqDevice, this.buffers);
            //this.view = new SapView(this.buffers);

            m_ColorConv = new SapColorConversion(this.acqDevice, this.buffers);


            // Create acquisition object
            if (this.acqDevice != null && !this.acqDevice.Initialized)
            {
                if (this.acqDevice.Create() == false)
                {
                    DestroyObjects();
                }
            }

            // Create buffer object
            if (this.buffers != null && !this.buffers.Initialized)
            {
                if (this.buffers.Create() == false)
                {
                    DestroyObjects();
                }
                this.buffers.Clear();
            }


            // Create color conversion object
            if (m_ColorConv != null && !m_ColorConv.Initialized)
            {
                if (m_ColorConv.Create() == false)
                {
                    DestroyObjects();
                }

                m_ColorConv.Enable(true, false);
            }

            this.xfer.Pairs[0].EventType = SapXferPair.XferEventType.EndOfFrame;
            this.xfer.XferNotify += new SapXferNotifyHandler(NewFrameReceived);
            this.xfer.XferNotifyContext = this.buffers;

            m_Pro = new MyProcessing(this.buffers, m_ColorConv, new SapProcessingDoneHandler(ProCallback));

            if (!CreateObjects())
            {
                DisposeObjects();
            }

        }

        private void ProCallback(object sender, SapProcessingDoneEventArgs e)
        {
            SapBuffer buf = e.Context as SapBuffer;
            IntPtr address = new IntPtr();
            buf.GetAddress(out address);
            int bitsPerPixel = 8 * buf.BytesPerPixel;
            int stride = 4 * ((buf.Width * bitsPerPixel + 31) / 32);

            this.bp = new Bitmap(buf.Width, buf.Height, stride, System.Drawing.Imaging.PixelFormat.Format32bppRgb, address);

            RaiseNewFrameEvent(new SysImage(this.bp));

        }

        private bool CreateObjects()
        {
            //// Create acquisition object
            //if (this.acqDevice != null && !this.acqDevice.Initialized)
            //{
            //    if (this.acqDevice.Create() == false)
            //    {
            //        DestroyObjects();
            //        return false;
            //    }
            //}
            //// Create buffer object
            //if (this.buffers != null && !this.buffers.Initialized)
            //{
            //    if (this.buffers.Create() == false)
            //    {
            //        DestroyObjects();
            //        return false;
            //    }
            //    this.buffers.Clear();
            //}

            //// Create color conversion object
            //if (m_ColorConv != null && !m_ColorConv.Initialized)
            //{
            //    if (m_ColorConv.Create() == false)
            //    {
            //        DestroyObjects();
            //        return false;
            //    }

            //    m_ColorConv.Enable(true, false);
            //}

            // Create processing object
            if (m_Pro != null && !m_Pro.Initialized)
            {
                if (!m_Pro.Create())
                {
                    DestroyObjects();
                    return false;
                }

                m_Pro.AutoEmpty = true;
            }

            // Create view object
            //if (this.view != null && !this.view.Initialized)
            //{
            //    if (this.view.Create() == false)
            //    {
            //        DestroyObjects();
            //        return false;
            //    }
            //}

            if (this.xfer != null && this.xfer.Pairs[0] != null)
            {
                this.xfer.Pairs[0].Cycle = SapXferPair.CycleMode.NextWithTrash;
                if (this.xfer.Pairs[0].Cycle != SapXferPair.CycleMode.NextWithTrash)
                {
                    DestroyObjects();
                    return false;
                }
            }

            // Create Xfer object
            if (this.xfer != null && !this.xfer.Initialized)
            {
                if (this.xfer.Create() == false)
                {
                    DestroyObjects();
                    return false;
                }
            }
            return true;
        }

        private void DestroyObjects()
        {
            if (this.xfer != null && this.xfer.Initialized)
                this.xfer.Destroy();
            //if (this.view != null && this.view.Initialized)
            //    this.view.Destroy();
            if (this.buffers != null && this.buffers.Initialized)
                this.buffers.Destroy();
            if (this.acqDevice != null && this.acqDevice.Initialized)
                this.acqDevice.Destroy();
        }

        private void DisposeObjects()
        {
            if (this.xfer != null)
            { this.xfer.Dispose(); this.xfer = null; }
            //if (this.view != null)
            //{ this.view.Dispose(); this.view = null;}
            if (this.buffers != null)
            { this.buffers.Dispose(); this.buffers = null; }
            if (this.acqDevice != null)
            { this.acqDevice.Dispose(); this.acqDevice = null; }
        }

        public Bitmap Snapshot()
        {
            this.xfer.Snap();
            this.waitingForSignal = true;
            signal.Wait();
            return this.bp;
        }

        public SaperaAssistant()
        {
            // Não é necessário nenhuma inicialização.
        }
    }
}
