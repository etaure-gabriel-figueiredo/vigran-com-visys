﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelOne
{
    public enum SignalsLevelOne
    {
        /*Eventos do processo*/
        eventHardeningStart = 0,
        eventHardeningEnd,
        /*Valores lidos do processo*/
        valueMotherCoilIdFirstPart,
        valueMotherCoilIdSecondPart,
        valueMotherCoilIdThirdPart,
        valueDaughterCoilIdFirstPart,
        valueDaughterCoilIdSecondPart,
        valueDaughterCoilIdThirdPart,
        valueWindingLength,
        valueRealSpeed,
        valueStripWidth,
        /*Valores escritos*/
        /*Eventos gerados pelo sistema (ESCRITA)*/
        eventWaterDetected,
        /*Valores gerados pelo sistema (ESCRITA)*/
        valueWpdTreshold,
        valueIndexSuperiorLo,
        valueIndexSuperiorCenter,
        valueIndexSuperiorLm,
        valueIndexInferiorLo,
        valueIndexInferiorCenter,
        valueIndexInferiorLm,
        /*Estado do sistema*/
        valueSystemStatus
    }
}
