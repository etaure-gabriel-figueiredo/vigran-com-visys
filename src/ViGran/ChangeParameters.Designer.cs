﻿namespace WPD_V1
{
    partial class ChangeParametersView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxL2Gradient = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApertureSize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTL = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtParticleMinSize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtParticleMaxSize = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtImagesMaxCount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtImagesMinCount = new System.Windows.Forms.TextBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtImagesPath = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxL2Gradient);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtApertureSize);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtTH);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTL);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(255, 122);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Canny Edge Detector";
            // 
            // checkBoxL2Gradient
            // 
            this.checkBoxL2Gradient.AutoSize = true;
            this.checkBoxL2Gradient.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxL2Gradient.Location = new System.Drawing.Point(76, 97);
            this.checkBoxL2Gradient.Name = "checkBoxL2Gradient";
            this.checkBoxL2Gradient.Size = new System.Drawing.Size(81, 17);
            this.checkBoxL2Gradient.TabIndex = 34;
            this.checkBoxL2Gradient.Text = "L2 Gradient";
            this.checkBoxL2Gradient.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Aperture Size";
            // 
            // txtApertureSize
            // 
            this.txtApertureSize.Location = new System.Drawing.Point(143, 71);
            this.txtApertureSize.Name = "txtApertureSize";
            this.txtApertureSize.Size = new System.Drawing.Size(100, 20);
            this.txtApertureSize.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Threshold High";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(143, 19);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(100, 20);
            this.txtTH.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Threshold Low";
            // 
            // txtTL
            // 
            this.txtTL.Location = new System.Drawing.Point(143, 45);
            this.txtTL.Name = "txtTL";
            this.txtTL.Size = new System.Drawing.Size(100, 20);
            this.txtTL.TabIndex = 27;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtParticleMinSize);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtParticleMaxSize);
            this.groupBox2.Location = new System.Drawing.Point(12, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(255, 78);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Histogram";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Particle Min Size";
            // 
            // txtParticleMinSize
            // 
            this.txtParticleMinSize.Location = new System.Drawing.Point(143, 45);
            this.txtParticleMinSize.Name = "txtParticleMinSize";
            this.txtParticleMinSize.Size = new System.Drawing.Size(100, 20);
            this.txtParticleMinSize.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Particle Max Size";
            // 
            // txtParticleMaxSize
            // 
            this.txtParticleMaxSize.Location = new System.Drawing.Point(143, 19);
            this.txtParticleMaxSize.Name = "txtParticleMaxSize";
            this.txtParticleMaxSize.Size = new System.Drawing.Size(100, 20);
            this.txtParticleMaxSize.TabIndex = 38;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtImagesMaxCount);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtImagesMinCount);
            this.groupBox3.Location = new System.Drawing.Point(12, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(255, 78);
            this.groupBox3.TabIndex = 39;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Image Batch Control";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Images Max Count";
            // 
            // txtImagesMaxCount
            // 
            this.txtImagesMaxCount.Location = new System.Drawing.Point(143, 45);
            this.txtImagesMaxCount.Name = "txtImagesMaxCount";
            this.txtImagesMaxCount.Size = new System.Drawing.Size(100, 20);
            this.txtImagesMaxCount.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Images Min Count";
            // 
            // txtImagesMinCount
            // 
            this.txtImagesMinCount.Location = new System.Drawing.Point(143, 19);
            this.txtImagesMinCount.Name = "txtImagesMinCount";
            this.txtImagesMinCount.Size = new System.Drawing.Size(100, 20);
            this.txtImagesMinCount.TabIndex = 38;
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(111, 364);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 40;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(192, 364);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnBrowse);
            this.groupBox4.Controls.Add(this.txtImagesPath);
            this.groupBox4.Location = new System.Drawing.Point(13, 308);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(255, 50);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Offline Images";
            // 
            // btnBrowse
            // 
            this.btnBrowse.AutoSize = true;
            this.btnBrowse.Location = new System.Drawing.Point(216, 17);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(26, 23);
            this.btnBrowse.TabIndex = 41;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtImagesPath
            // 
            this.txtImagesPath.Location = new System.Drawing.Point(8, 19);
            this.txtImagesPath.Name = "txtImagesPath";
            this.txtImagesPath.Size = new System.Drawing.Size(202, 20);
            this.txtImagesPath.TabIndex = 40;
            // 
            // ChangeParametersView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 393);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeParametersView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ChangeParameters";
            this.Shown += new System.EventHandler(this.ChangeParametersView_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTL;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtParticleMinSize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtParticleMaxSize;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtImagesMaxCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtImagesMinCount;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtImagesPath;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.CheckBox checkBoxL2Gradient;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApertureSize;
    }
}
