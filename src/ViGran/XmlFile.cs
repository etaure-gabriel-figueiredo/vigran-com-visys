﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WPD_V1
{
    class XmlFile
    {
        public string SavePath { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public Periodicity periodicity { get; set; }
        public string FileName
        {
            get
            {
                string fileName = SavePath + "GRAN_" + Type + "_" + Date + ".xml";
                return fileName;
            }
        }

        public XmlFile(string savePath, string type, Periodicity periodicity, DateTime date)
        {
            SavePath = savePath;
            Type = type;
            this.periodicity = periodicity;
            Date = DateTimeToString(date);
        }

        public string DateTimeToString(DateTime datetime)
        {
            if (periodicity == Periodicity.Daily)
            {
                return datetime.ToString("yyyy'-'MM'-'dd");
            }
            else
            {
                return datetime.GetDateTimeFormats('y').GetValue(0).ToString();
            }
        }

        private void CreateFile()
        {
            if (!File.Exists(FileName))
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    string xmlStart = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\n";
                    xmlStart += "<granData>\n</granData>";
                    byte[] xmlStartByte = new UTF8Encoding(true).GetBytes(xmlStart);
                    fs.Write(xmlStartByte, 0, xmlStartByte.Length);
                    fs.Close();
                }
            }
        }

        public void AppendNode(string nodeData, DateTime dateTime)
        {
            nodeData += "\n</granData>";
            Date = DateTimeToString(dateTime);
            CreateFile();
            using (FileStream fs = new FileStream(FileName, FileMode.Open))
            {
                fs.Seek(-11, SeekOrigin.End);
                byte[] info = new UTF8Encoding(true).GetBytes(nodeData);
                fs.Write(info, 0, info.Length);
                fs.Close();
            }
        }
    }
    public enum Periodicity
    {
        Daily,
        Monthly
    }
}
