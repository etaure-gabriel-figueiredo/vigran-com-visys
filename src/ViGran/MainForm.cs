﻿
namespace WPD_V1
{
    using AsynchronousGrab;
    using AVT.VmbAPINET;
    using BasicUtil;
    using BO;
    using LevelOne;
    using SignalProcessing;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using System.Windows.Forms.DataVisualization.Charting;
    using System.Xml;
    using ViSys.Modules.Camera;
    using ViSys.Modules.Camera.Common;
    using WPD_V1;

    /// <summary>
    /// The MainForm (GUI) Class implementation
    /// </summary>
    public partial class MainForm : Form
    {
        public CameraModuleFactory mcamfactory = null;
        public ICameraModule mcamface = null;




        /// <summary>
        /// Vimba API
        /// </summary

        //private Vimba sys = new Vimba();
        private VimbaHelper m_VimbaHelper = null;
        //private Camera camFG;
        CameraInfo camFG = null;
        //private Camera camFNG;

        /*Image Processing*/
        private ImageProcessing imageProc;

        /*level one*/
        private CommunicationLevelOne levelOneCommunication;
        private CommunicationLevelOne levelOneCommunicationPrimary;
        private CommunicationLevelOne levelOneCommunicationSecondary;

        /**** Iner system ****/
        private Bitmap imageRGB;
        private double FPPSRAW = 0;
        private double FPPS = 0;
        internal static bool running = false;
        
        private List<double[]> HistogramListXAdjusted = new List<double[]>();
        private List<double[]> HistogramListYAdjusted = new List<double[]>();
        private List<double[]> HistogramListXNotAdjusted = new List<double[]>();
        private List<double[]> HistogramListYNotAdjusted = new List<double[]>();
        private List<DateTime> samplesTimestamp = new List<DateTime>();

        private GranulometryEstimatorBO granBO;

        private DateTime batchStart;
        private DateTime batchEnd;

        private int offlineImageCount = 0;
        private int batchImageCount = 0;
        private int batchImageMinCount = 20;
        private int batchImageMaxCount = 60;
        private string[] imagesOffline;

        ChangeParametersView parameterView;

        private bool cameraOk = false;

        /*Configuracao do sistema*/
        private string imageFolder = "images";
        private string imageRunFolder = @"images\run";
        private string imageZippedFolder = @"images\zipadas";
        private string adjustedFolder = "ajustados";
        private string notAdjustedFolder = "não ajustados";
        private String imageFolderName;
        private String imageRunFolderName;
        private String imageZippedFolderName;
        private String adjustedFolderName;
        private String notAdjustedFolderName;
        private String logFolderName;

        private LoadSystemConfiguration systemConfiguration;

        private DateTime systemTimeInitial;

        private SaveGranulometryLocalData localData;

        private Boolean flagImagesCompressed = true;

        /*Gerenciamento de disco*/
        private Boolean flagFreeSpaceWarning = false;
        float PercFreeLastCheck = float.NaN;

        public MainForm()
        {
            try
            {
                InitializeComponent();

                System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;

                // Set current thread culture to pt-BR.
                //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("pt-BR");

                /* Log de inicialização de sistema */
                Logger.Info(HMIMessages.SystemStarted());

                systemConfiguration = new LoadSystemConfiguration();
                this.Text = systemConfiguration.systemName + " - " + systemConfiguration.systemType + " - v" + systemConfiguration.buildNumber;
                this.logFolderName = this.systemConfiguration.systemPath + this.systemConfiguration.logFolder;
                this.imageFolderName = this.logFolderName + @"\" + imageFolder;
                this.imageRunFolderName = this.logFolderName + @"\" + imageRunFolder;
                this.imageZippedFolderName = this.logFolderName + @"\" + imageZippedFolder;
                this.adjustedFolderName = this.logFolderName + @"\" + adjustedFolder;
                this.notAdjustedFolderName = this.logFolderName + @"\" + notAdjustedFolder;

                /* Criando diretórios do sistema */
                Directory.CreateDirectory(imageFolderName);
                Directory.CreateDirectory(imageRunFolderName);
                Directory.CreateDirectory(imageZippedFolderName);
                Directory.CreateDirectory(adjustedFolderName);
                Directory.CreateDirectory(notAdjustedFolderName);

                /* Gerenciamento de disco */
                CheckFreeSpace();                

                systemTimeInitial = DateTime.Now;

                //this.levelOneCommunication = new CommunicationLevelOne();
                //this.levelOneCommunicationPrimary = new CommunicationLevelOne(systemConfiguration.systemPath, systemTimeInitial, systemConfiguration.oPCNodeNamePrimary);
                //this.levelOneCommunicationSecondary = new CommunicationLevelOne(systemConfiguration.systemPath, systemTimeInitial, systemConfiguration.oPCNodeNameSecondary);

                //if (levelOneCommunicationPrimary.isOk())
                //{
                //    this.levelOneCommunication = levelOneCommunicationPrimary;
                //}
                //else
                //{
                //    this.levelOneCommunication = levelOneCommunicationSecondary;
                //}

            localData = new SaveGranulometryLocalData(systemConfiguration.systemPath + systemConfiguration.logFolder, adjustedFolder, notAdjustedFolder, systemConfiguration.systemName, systemConfiguration.GrainSizeRangeList);

            /* Create Granulometry Data */
            granBO = new GranulometryEstimatorBO();
            granBO.ThresholdHigh = systemConfiguration.thresholdHigh;
            granBO.ThresholdLow = systemConfiguration.thresholdLow;
            granBO.ApertureSize = systemConfiguration.apertureSize;
            granBO.L2Gradient = systemConfiguration.l2Gradient;
            granBO.ParticleMinSize = systemConfiguration.particleMinSize;
            granBO.ParticleMaxSize = systemConfiguration.particleMaxSize;
            granBO.PixelPerMm = systemConfiguration.PixelPerMm;
            granBO.GrainSizeRangeList = systemConfiguration.GrainSizeRangeList;
            granBO.GrainSizeRangeWeightList = systemConfiguration.GrainSizeRangeWeightList;

            /* Create Parameters change windows */
            parameterView = new ChangeParametersView();
            parameterView.ThresholdHigh = systemConfiguration.thresholdHigh;
            parameterView.ThresholdLow = systemConfiguration.thresholdLow;
            parameterView.ApertureSize = systemConfiguration.apertureSize;
            parameterView.L2Gradient = systemConfiguration.l2Gradient;
            parameterView.ParticleMinSize = systemConfiguration.particleMinSize;
            parameterView.ParticleMaxSize = systemConfiguration.particleMaxSize;
            // Image control cycle. Change for Lv One read
            parameterView.ImagesMinCount = batchImageMinCount;
            parameterView.ImagesMaxCount = batchImageMaxCount;

            //  open cameras
            //ConnectCamera();
            //ViSys
            if (!systemConfiguration.assistantSelector.Equals("FILES")) ViSysCapture();

                UpdateSystemInformation();
            }
            catch (Exception ex)
            {
                ShowError(HMIMessages.ApplictaionLoadError(ex.Message));
                Logger.Fatal(HMIMessages.ApplictaionLoadError(ex.Message), ex);
                this.Close();
            }
        }        

        /// <summary>
        /// Função para verificar se o disco onde as imagens são armazedas está cheio
        /// Se estiver cheio o sistema deleta pastas zipadas antigas no sistema
        /// </summary>
        private void CheckFreeSpace()
        {
            /* Variável com a percentagem de disco livre */
            float PercFree = 0.00f;

            try
            {
                /* Busca todos os discos do sistema*/
                DriveInfo[] allDrives = DriveInfo.GetDrives();

                /* Recupera caminho completo da pasta de imagens zipadas */
                string PathName = System.IO.Path.GetFullPath(this.imageZippedFolderName);

                /* Percorre todos os discos encontrados */
                foreach (DriveInfo d in allDrives)
                {
                    /* Encontra o disco onde as imagens são salvas */
                    if (d.Name[0].Equals(PathName[0]) && d.IsReady == true)
                    {
                        /* Calcula percentagem de disco livre */
                        PercFree = (100 * (float)d.TotalFreeSpace / d.TotalSize);

                        Console.WriteLine(@"Perc = {0}%", PercFree.ToString("0.0000"));

                        /* Atualiza flag para registro em log */
                        if (PercFreeLastCheck != float.NaN && PercFree > PercFreeLastCheck)
                        {
                            flagFreeSpaceWarning = false;
                        }

                        /* Realiza check de alarme de disco cheio */
                        if (PercFree > systemConfiguration.hdPercentageFreeSpaceOverwrite && 
                            PercFree < systemConfiguration.hdPercentageFreeSpaceWarning && 
                            flagFreeSpaceWarning == false)
                        {
                            //MessageBox.Show(string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}", DateTime.Now, Message), 
                                //"Espaço em Disco", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            Logger.Warn(HMIMessages.DiskSpaceWarning(PercFree, systemConfiguration.hdPercentageFreeSpaceOverwrite));
                            flagFreeSpaceWarning = true;
                        }

                        /* Realiza check de deleção de arquivos antigos */
                        if (PercFree < systemConfiguration.hdPercentageFreeSpaceOverwrite)
                        {
                            //MessageBox.Show(string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}", DateTime.Now, Message), 
                            //    "Espaço em Disco", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            Logger.Warn(HMIMessages.DiskDeleteData());

                            /* Deleta os arquivos mais antigos */
                            foreach (var fi in new DirectoryInfo(@""+this.imageZippedFolderName).GetFiles().OrderBy(x => x.LastWriteTime).Take(systemConfiguration.numberOfFilesToBeDeleted))
                            {
                                fi.Delete();
                                Logger.Warn(HMIMessages.DeletedOldFolder(fi.Name));
                            }
                        }

                        /* Atualiza última percentagem salva */
                        PercFreeLastCheck = PercFree;
                        break;
                    }                    
                }                
            }
            catch (Exception e)
            {
                Logger.Error(e.Message.ToString());
            }           
        }

        private void ConnectCamera()
        {
            try
            {
                /*Inicia camera NOK */
                cameraOk = false;

                //  start VimbaNET API
                //sys.Startup();
                // Start up Vimba SDK
                m_VimbaHelper = new VimbaHelper();
                m_VimbaHelper.Startup(this.OnCameraListChanged);

                //CameraCollection cameras = sys.Cameras;
                List<CameraInfo> cameras = m_VimbaHelper.CameraList;

                //Via arquivo de configuração
                string nameCamFG = systemConfiguration.nameCameraClr;

                foreach (CameraInfo camAux in cameras)
                {
                    if (camAux.ID.Equals(nameCamFG))
                    {
                        camFG = camAux;
                        Logger.Info(HMIMessages.CameraFound());
                    }
                    else
                    {
                        ShowError(HMIMessages.CameraNotAvailable());
                        Logger.Error(HMIMessages.CameraNotAvailable());
                        return;
                    }
                }

                try
                {
                    m_VimbaHelper.OpenCamera(camFG.ID);
                    Logger.Info(HMIMessages.CameraOpenned(camFG.ID));
                }
                catch
                {
                    ShowError(HMIMessages.CameraCouldNotOpenned(nameCamFG));
                    Logger.Error(HMIMessages.CameraCouldNotOpenned(nameCamFG));
                    return;
                }


                string fileName = systemConfiguration.systemPath + systemConfiguration.confFolder + @"\" + systemConfiguration.parameterCameraClr;

                try
                {
                    m_VimbaHelper.loadCameraSettings(fileName);
                }
                catch (Exception exception)
                {
                    Logger.Error(HMIMessages.CameraSettingsLoadFail(exception.Message));
                    ShowError(HMIMessages.CameraSettingsLoadFail(exception.Message));
                    return;
                }

                Logger.Info(HMIMessages.VimbaStarted());

                cameraOk = true;

            }
            catch (Exception ex)
            {
                ShowError(HMIMessages.CameraLoadError(ex.Message));
                Logger.Error(HMIMessages.CameraLoadError(ex.Message));
            }
        }

        private void UpdateSystemInformation()
        {
            /*Escreve as informações correspondente ao estado do sistema*/

            /*Estado de conexão com a câmera*/
            if (this.cameraOk)
            {
                this.txtCameraLinkStatus.Text = HMIMessages.CameraLinkOk();
                this.txtCameraLinkStatus.BackColor = Color.LimeGreen;
            }
            else
            {
                this.txtCameraLinkStatus.Text = HMIMessages.CameraLinkNOK();
                this.txtCameraLinkStatus.BackColor = Color.Red;
            }

            ///*Estado do servidor OPC*/
            //if (levelOneCommunication.serverIsOK())
            //{
            //    this.txtOPCServerStatus.Text = HMIMessages.OPCServerOK(levelOneCommunication.getOPCNodeName());
            //    this.txtOPCServerStatus.BackColor = Color.LimeGreen;
            //}
            //else
            //{
            //    this.txtOPCServerStatus.Text = HMIMessages.OPCServerNOK(levelOneCommunication.getOPCNodeName());
            //    this.txtOPCServerStatus.BackColor = Color.Red;
            //}

            ///*Estado do grupo OPC*/
            //if (levelOneCommunication.groupIsConnected())
            //{
            //    this.txtOPCGroupStatus.Text = HMIMessages.OPCGroupOK();
            //    this.txtOPCGroupStatus.BackColor = Color.LimeGreen;
            //}
            //else
            //{
            //    this.txtOPCGroupStatus.Text = HMIMessages.OPCGroupNOK();
            //    this.txtOPCGroupStatus.BackColor = Color.Red;
            //}

            /*Atualiza frequência de tratamento das imagens*/
            if (FPPSRAW > 0 & FPPSRAW < 1000)
            {
                FPPS = FPPSRAW;
                txtFPS.Text = @"Max FPPS: " + FPPS.ToString("0");
            }

            /*Atualiza data e hora*/
            txtActDateTime.Text = HMIMessages.DataHora(DateTime.Now);

            /*Atualiza data e hora*/
            txtUser.Text = HMIMessages.LoggedUser("Criar funcionalidade de login");
        }

        private void UpdateScreen(object sender, EventArgs args)
        {
            try
            {
                int i, j;

                SetTxtSystemStatusRunning(running);
                UpdateSystemInformation();

                if (Form.ActiveForm == null || !Form.ActiveForm.Equals(parameterView))
                {
                    //if (!m_VimbaHelper.getImageInUse())
                    if ((m_VimbaHelper != null && !m_VimbaHelper.getImageInUse()) || m_VimbaHelper == null)
                    {
                        pictureBoxFG.Image = imageProc.getBitmap(Channel.NIR);
                        picCannyEdge.Image = imageProc.getBitmap(Channel.BIN);
                        //imageProc.processRedChanel();
                        //picCannyEdge.Image = imageProc.getBitmap(Channel.RED);ca
                    }

                    chartWpdFG.Series[0].Points.Clear();
                    for (i = 0; i < granBO.HistogramXAdjusted.Length; i++)
                    {
                        chartWpdFG.Series[0].Points.Add(new DataPoint(i, granBO.HistogramXAdjusted[i]));
                        if (i == 0)
                        {
                            chartWpdFG.Series[0].Points[i].AxisLabel = string.Format("< {0}", granBO.GrainSizeRangeList[i]);
                        }
                        else if (i == granBO.HistogramYAdjusted.Length - 1)
                        {
                            chartWpdFG.Series[0].Points[i].AxisLabel = string.Format(">= {0}", granBO.GrainSizeRangeList[i - 1]);
                        }
                        else
                        {
                            chartWpdFG.Series[0].Points[i].AxisLabel = string.Format("{0} - {1}", granBO.GrainSizeRangeList[i - 1], granBO.GrainSizeRangeList[i]);
                        }
                    }

                    chartWpdFG.Series[1].Points.Clear();
                    for (i = 0; i < granBO.HistogramYAdjusted.Length; i++)
                    {
                        chartWpdFG.Series[1].Points.Add(new DataPoint(i, granBO.HistogramYAdjusted[i]));
                        if (i == 0)
                        {
                            chartWpdFG.Series[1].Points[i].AxisLabel = string.Format("< {0}", granBO.GrainSizeRangeList[i]);
                        }
                        else if (i == granBO.HistogramYAdjusted.Length - 1)
                        {
                            chartWpdFG.Series[1].Points[i].AxisLabel = string.Format(">= {0}", granBO.GrainSizeRangeList[i - 1]);
                        }
                        else
                        {
                            chartWpdFG.Series[1].Points[i].AxisLabel = string.Format("{0} - {1}", granBO.GrainSizeRangeList[i - 1], granBO.GrainSizeRangeList[i]);
                        }
                    }

                    pictureBoxFG.Focus();

                    //UpdateImageInUse(true);
                }
            }
            catch (Exception ex)
            {
                Logger.Warn(HMIMessages.UpdateScreenError(ex.Message), ex);
                //UpdateImageInUse(true);
            }
        }

        private void SetTxtSystemStatusRunning(Boolean flag)
        {
            if (flag)
            {
                txtProcessStatus.Text = "Running";
                txtProcessStatus.BackColor = Color.Red;
                txtProcessStatus.ForeColor = Color.White;
                txtProcessStatus.BringToFront();
            }
            else
            {
                txtProcessStatus.Text = "Waiting";
                txtProcessStatus.BackColor = Color.Green;
                txtProcessStatus.ForeColor = Color.White;
                txtProcessStatus.BringToFront();
            }
        }

        private void CheckProcessStatus()
        {
            DateTime actDateTime = DateTime.Now;
            String fileName;

            //if(levelOneCommunication.isHardeningStart())
            if(batchImageCount >= batchImageMinCount) //Running
            {
                if (!running)
                {
                    HistogramListXAdjusted.Clear();
                    HistogramListYAdjusted.Clear();
                    HistogramListXNotAdjusted.Clear();
                    HistogramListYNotAdjusted.Clear();
                    samplesTimestamp.Clear();
                    batchStart = actDateTime;
                }
                running = true;
                flagImagesCompressed = false;
                fileName = "" +
                        actDateTime.TimeOfDay.Hours.ToString("00") +
                        actDateTime.TimeOfDay.Minutes.ToString("00") +
                        actDateTime.TimeOfDay.Seconds.ToString("00") + "_" +
                        actDateTime.TimeOfDay.Milliseconds.ToString("000");
                SaveMedia.saveImageQuality(imageRunFolderName + @"\RGB_" + fileName, imageProc.getBitmap(Channel.NIR), 75L);
                SaveMedia.saveImageQuality(imageRunFolderName + @"\BIN_" + fileName, imageProc.getBitmap(Channel.BIN), 75L);
                
                samplesTimestamp.Add(actDateTime);
                HistogramListXAdjusted.Add(granBO.HistogramXAdjusted);
                HistogramListYAdjusted.Add(granBO.HistogramYAdjusted);
                HistogramListXNotAdjusted.Add(granBO.HistogramXNotAdjusted);
                HistogramListYNotAdjusted.Add(granBO.HistogramYNotAdjusted);
            }
            //if(levelOneCommunication.isHardeningEnd())
            else
            {
                running = false;
                if (!flagImagesCompressed)
                {
                    /* Salva horário de batch end */
                    batchEnd = actDateTime;

                    /* Calcula a granulometria media da batelada */
                    double[] meanHorizontalSizeDistributionAdjusted = Statistics.SumHistogramsNormalized(HistogramListXAdjusted).ToArray();
                    double[] meanVerticalSizeDistributionAdjusted = Statistics.SumHistogramsNormalized(HistogramListYAdjusted).ToArray();
                    double[] meanHorizontalSizeDistributionNotAdjusted = Statistics.SumHistogramsNormalized(HistogramListXNotAdjusted).ToArray();
                    double[] meanVerticalSizeDistributionNotAdjusted = Statistics.SumHistogramsNormalized(HistogramListYNotAdjusted).ToArray();

                    /* Salva media diaria se dia mudou */
                    localData.CalculateAndSaveDailyIfNewDay(batchEnd, HistogramListXAdjusted, HistogramListYAdjusted, HistogramListXNotAdjusted, HistogramListYNotAdjusted);

                    /*Adiciona os dados de indice WPD ao arquivo xml de log*/
                    localData.AddBatchInfoInstantaneous(samplesTimestamp, HistogramListXAdjusted, HistogramListYAdjusted, HistogramListXNotAdjusted, HistogramListYNotAdjusted, batchStart, batchEnd);
                    localData.AddBatchInfoEvaluation(meanHorizontalSizeDistributionAdjusted, meanVerticalSizeDistributionAdjusted, meanHorizontalSizeDistributionNotAdjusted, meanVerticalSizeDistributionNotAdjusted, batchStart, batchEnd);

                    localData.SaveTemporaryData();

                    /* Gerenciamento de disco */
                    CheckFreeSpace();

                    /*Compacta as imagens da bobina e salva em um único arquivo*/
                    fileName = "" +
                        actDateTime.Year.ToString("0000") +
                        actDateTime.Month.ToString("00") +
                        actDateTime.Day.ToString("00") +
                        actDateTime.TimeOfDay.Hours.ToString("00") +
                        actDateTime.TimeOfDay.Minutes.ToString("00") +
                        actDateTime.TimeOfDay.Seconds.ToString("00");
                    SaveMedia.zipFiles(this.imageRunFolderName,
                        this.imageZippedFolderName + @"\" + fileName + @".zip");

                    /*Limpa a pasta das imagens*/
                    SaveMedia.deleteFiles(this.imageRunFolderName);

                    flagImagesCompressed = true;
                }
            }

            /* Batch Control cycle: Change for level one read */
            batchImageCount++;
            if (batchImageCount >= batchImageMaxCount) batchImageCount = 0;

        }

        private void MainCalculation()
        {
            try
            {
                /* Recupera data de início de cálculo para calcular FPPS*/
                DateTime startFrameProc = System.DateTime.Now;

                /* Prepara a imagem de Entrada */
                if (imageProc == null)
                {
                    imageProc = new ImageProcessing(ref imageRGB, ref imageRGB, ref imageRGB, 
                        ref imageRGB, new Rectangle(0,systemConfiguration.initialRoi.Y, imageRGB.Width, 
                        imageRGB.Height- systemConfiguration.initialRoi.Y));
                    granBO.ImageProc = imageProc;
                }
                else
                {
                    imageProc.InputImage = imageRGB;
                }

                /* Calcula a granulometria */
                granBO.CalculateGranulometry();

                /* Verifica estado do processo */
                CheckProcessStatus();

                /* Calcula FPPS */
                DateTime endFrameProc = System.DateTime.Now;
                FPPSRAW = 1000.0 / endFrameProc.Subtract(startFrameProc).TotalMilliseconds;

                /* Invoca a thread da GUI para atualização de tela */
                if (!this.IsDisposed)
                {
                    //BeginInvoke(new EventHandler(UpdateScreen));
                    Invoke(new EventHandler(UpdateScreen));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(HMIMessages.MainCalculationError(ex.Message), ex);
                //UpdateImageInUse(true);
            }
        }

        /// <summary>
        /// Recebe frame da câmera e prepara para ser utilizado na aplicação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HandleNewFrame(object sender, SysImage args)
        {
            try
            {
                if (args.Image != null)
                {
                    /*Atribuição da imgem bruta*/
                    imageRGB = new Bitmap(args.Image);

                    /* Starts a new thread for calculation */
                    //System.Threading.Tasks.Task.Run(() => MainCalculation());
                    MainCalculation();
                    VimbaHelper.ImageInUse = true;
                }
                else
                {
                    Logger.Error("Erro ao capturar frame.");
                    //Logger.Error(args.Exception.Message, args.Exception);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(HMIMessages.HandleFrameError(ex.Message), ex);
                UpdateImageInUse(true);
            }
        }

        /// <summary>
        /// Add log message to logging list box
        /// </summary>
        /// <param name="message">The message</param>
        private void ShowMessage(string message)
        {
            if (null == message)
            {
                throw new ArgumentNullException("message");
            }

            System.Windows.Forms.MessageBox.Show(string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}", DateTime.Now, message), systemConfiguration.systemName);
        }

        /// <summary>
        /// Add an error log message and show an error message box
        /// </summary>
        /// <param name="message">The message</param>
        private void ShowError(string message)
        {
            if (null == message)
            {
                throw new ArgumentNullException("message");
            }

            /*TODO: Encontrar outra solução*/
            //MessageBox.Show(string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}", DateTime.Now, message), systemConfiguration.systemName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            MessageBox.Show(string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}: {1}", DateTime.Now, message), "ViGran", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Handles the CameraListChanged event
        /// </summary>
        /// <param name="sender">The Sender</param>
        /// <param name="args">The EventArgs</param>
        private void OnCameraListChanged(object sender, EventArgs args)
        {
            //LogMessage("Camera list updated.");
        }

        /// <summary>
        /// Handles the Form_Load event of this (MainForm)
        /// </summary>
        /// <param name="sender">The Sender (not used)</param>
        /// <param name="e">The EventArgs (not used)</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //m_VimbaHelper.StartContinuousImageAcquisition(HandleNewFrame);
            }
            catch (Exception ex)
            {
                ShowError(HMIMessages.ContinuousAquisitionError(ex.Message));
                Logger.Error(HMIMessages.ContinuousAquisitionError(ex.Message), ex);
            }
        }

        /// <summary>
        /// Handles the FormClosing event of the MainForm
        /// </summary>
        /// <param name="sender">The Sender (not used)</param>
        /// <param name="e">The EventArgs (not used)</param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (null != m_VimbaHelper)
            {
                try
                {
                    //sys.Shutdown();
                    // Shutdown Vimba SDK when application exits
                    m_VimbaHelper.StopContinuousImageAcquisition();
                    m_VimbaHelper.Shutdown();
                    //levelOneCommunication.terminateCommunication();
                }
                catch (Exception ex)
                {
                    ShowError(HMIMessages.VimbaShutdownError(ex.Message));
                    Logger.Error(HMIMessages.VimbaShutdownError(ex.Message), ex);
                }
            }
            //this.parameterView.Dispose();
            Logger.Info(HMIMessages.SystemStopped());
        }

        private void btnChangeParameters_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateImageInUse(false);
                parameterView.ShowDialog(this);

                granBO.ThresholdHigh = parameterView.ThresholdHigh;
                granBO.ThresholdLow = parameterView.ThresholdLow;
                granBO.ApertureSize = parameterView.ApertureSize;
                granBO.L2Gradient = parameterView.L2Gradient;
                granBO.ParticleMinSize = parameterView.ParticleMinSize;
                granBO.ParticleMaxSize = parameterView.ParticleMaxSize;
                batchImageMaxCount = parameterView.ImagesMaxCount;
                batchImageMinCount = parameterView.ImagesMinCount;
                imagesOffline = parameterView.ImagesOffline;

                if (imagesOffline == null || imagesOffline.Length <= 0)
                {
                    btnSendOfflineImage.Enabled = false;
                }
                else
                {
                    btnSendOfflineImage.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowError(HMIMessages.ChangeParameterError(ex.Message));
                Logger.Error(HMIMessages.ChangeParameterError(ex.Message), ex);
            }
            finally
            {
                UpdateImageInUse(true);
            }
        }

        private void btnSendOfflineImage_Click(object sender, EventArgs e)
        {
            //if (imagesOffline == null)
            //{
            //    MessageBox.Show("Select a folder in parameters.", "ViGran", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //if (imagesOffline.Length <= 0)
            //{
            //    MessageBox.Show("No images found.", "ViGran", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //if (offlineImageCount >= imagesOffline.Length) offlineImageCount = 0;
            //VimbaHelper.ImageInUse = false;
            //HandleNewFrame(this, new FrameEventArgs(new Bitmap(imagesOffline[offlineImageCount++])));            

        }
        private void ViSysCapture()
        {
            this.mcamfactory = new CameraModuleFactory();
            CameraModuleFactory.CameraAPIProvider tempEnum = (CameraModuleFactory.CameraAPIProvider)
                            Enum.Parse(typeof(CameraModuleFactory.CameraAPIProvider), systemConfiguration.assistantSelector);
            this.mcamface = mcamfactory.Create(tempEnum);
            if (!tempEnum.Equals("FILES")) this.mcamface.Open(systemConfiguration.nameCameraClr);
            this.mcamface.NewFrame += new EventHandler<SysImage>(HandleNewFrame);
            this.mcamface.GetFramesAsync();
        }

        private void UpdateImageInUse(Boolean imageInUse)
        {
            if (imageInUse)
            {
                if ((Form.ActiveForm == null || !Form.ActiveForm.Equals(parameterView)) && !VimbaHelper.ImageInUse)
                {
                    VimbaHelper.ImageInUse = true;
                }
            }
            else
            {
                VimbaHelper.ImageInUse = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileAccess = new OpenFileDialog();
                if (fileAccess.ShowDialog() == DialogResult.OK)
                {
                    this.mcamfactory = new CameraModuleFactory();
                    this.mcamface = mcamfactory.Create(CameraModuleFactory.CameraAPIProvider.FILES);
                    this.mcamface.ProcessVideoFiles(fileAccess.FileName);
                    this.mcamface.NewFrame += new EventHandler<SysImage>(HandleNewFrame);
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Erro ao Abrir Imagem", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
