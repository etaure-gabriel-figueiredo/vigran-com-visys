﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    public class SlagIndex
    {
            float temperatureMin;
            float temperatureMax;

            public double value;
            public double initialIndex;
            public DateTime dateTime;

        public SlagIndex(double value, DateTime date_time)
        {
            temperatureMin = 1200;
            temperatureMax = 1500;

            this.value = value;
            this.dateTime = date_time;
        }

        public double getIndexAdjustedByTemperature(float t)
        {
            double indAdj;
            float tMean = (this.temperatureMin + this.temperatureMax) / 2;
            float adjCoef = t / tMean;

            if (t != 0)
            {
                indAdj = this.value * adjCoef;
            }
            else
            {
                /*Caso não tenha medição considera o meio da faixa de temperatura*/
                indAdj = this.value;
            }

            return indAdj > 1 ? 1 : indAdj;

        }
    }
}
