﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    public static class Statistics
    {
        public static IEnumerable<double> HistogramNormalized(IEnumerable<int> intData, int binSize)
        {
            return NormalizeHistogram(HistogramAbsolute(intData, binSize));
        }

        public static IEnumerable<double> HistogramNormalized(IEnumerable<int> intData)
        {
            return NormalizeHistogram(HistogramAbsolute(intData));
        }

        public static IEnumerable<double> HistogramAbsolute(IEnumerable<int> intData)
        {
            return HistogramAbsolute(intData, 1);
        }

        public static IEnumerable<double> HistogramAbsolute(IEnumerable<int> intData, int binSize)
        {
            double[] histogram = new double[intData.Max() + 1];
            List<double> normalizedHistrogram = new List<double>();

            /* Verifica bin size inválido */
            if (binSize <= 0) return null;

            /* Monta histograma */
            if (intData.Count() > 0)
            {
                foreach (int item in intData)
                {
                    histogram[item]++;
                }
            }

            /* Prepara histograma com número correto de bins */
            for (int i = 1; i < histogram.Length; i = i + binSize)
            {
                normalizedHistrogram.Add(histogram.Skip(i).Take(binSize).Sum());
            }

            return normalizedHistrogram;
        }

        public static IEnumerable<double> NormalizeHistogram(IEnumerable<double> histogram)
        {
            double sum;

            /* Verifica elemento nulo */
            if (histogram == null) return null;

            /* Normaliza histograma */
            sum = histogram.Sum();
            return histogram.Select(x => x / sum * 100);
        }

        public static IEnumerable<double> SumHistogramsNormalized(IEnumerable<IEnumerable<double>> histograms)
        {
            /* Verifica elemento nulo */
            if (histograms == null) return null;

            return NormalizeHistogram(SumHistogramsAbsolute(histograms));
        }

        public static IEnumerable<double> SumHistogramsAbsolute(IEnumerable<IEnumerable<double>> histograms)
        {
            List<double> resultHistogram = new List<double>();

            /* Verifica elemento nulo */
            if (histograms == null) return null;

            resultHistogram = histograms.ElementAt(0).ToList();
            for (int i = 1; i < histograms.Count(); i++)
            {
                resultHistogram = SumHistograms(resultHistogram, histograms.ElementAt(i)).ToList();
            }
            return resultHistogram;
        }

        public static IEnumerable<double> SumHistograms(IEnumerable<double> histogram1, IEnumerable<double> histogram2)
        {
            int maxLength, minLength;
            List<double> resultHistogram = new List<double>();

            maxLength = Math.Max(histogram1.Count(), histogram2.Count());
            minLength = Math.Min(histogram1.Count(), histogram2.Count());

            /* Soma os histogramas */
            resultHistogram = Enumerable.Zip(histogram1, histogram2, (first, second) => first + second).ToList();

            /* Somar final dos histogramas */
            if(maxLength > minLength)
            {
                if(histogram1.Count() == maxLength)
                {
                    resultHistogram.AddRange(histogram1.Skip(minLength));
                }
                else
                {
                    resultHistogram.AddRange(histogram2.Skip(minLength));
                }
            }

            return resultHistogram;
        }

        internal static double[] GenerateHistogram(List<double> sizes, List<double> grainSizeRangeList)
        {
            double[] histogram = new double[grainSizeRangeList.Count];
            for (int i = 0; i<histogram.Length; i++)
            {
                histogram[i] = 0;
            }
            foreach (var size in sizes)
            {
                for (var i = 0; i < grainSizeRangeList.Count; i++)
                {
                    if (size < grainSizeRangeList[i])
                    {
                        histogram[i]++;
                        break;
                    }
                }
            }
            return histogram;
        }

        public static void AdjustmentFunction(double[] histogram, double[] grainSizeRangeWeightList)
        {
            for (int i = 0; i< histogram.Length; i++)
            {
                histogram[i] *= grainSizeRangeWeightList[i];
            }
        }

        public static double[] NormalizeHistograma(double[] histogram)
        {
            double total = histogram.Sum();

            //Caso histograma vazio, retorna histograma zerado
            if (total == 0)
                return histogram;

            double[] normalizedHistogram = new double[histogram.Length];
            for (int i = 0; i < histogram.Length; i++)
            {
                normalizedHistogram[i] = (float)histogram[i] / total * 100;
            }
            return normalizedHistogram;
        }

        public static IEnumerable<double> HistogramAutoRuleNormalized(IEnumerable<int> intData)
        {
            return NormalizeHistogram(HistogramAutoRuleAbsolute(intData));
        }

        public static IEnumerable<double> HistogramAutoRuleAbsolute(IEnumerable<int> enumerable)
        {
            int i;
            if (enumerable.Count() > 0)
            {
                int[] edges = Autorule(enumerable);
                double[] histogram = new double[edges.Length];
                foreach (int item in enumerable)
                {
                    for (i = 0; i < edges.Length && edges[i] < item; i++) ;
                    histogram[i]++;
                }
                return histogram;
            }
            return null;
        }

        private static int[] Autorule(IEnumerable<int> enumerable)
        {
            double xmin = enumerable.Min();
            double xmax = enumerable.Max();
            double range = xmax - xmin;
            double binWidth = 3.5 * StandardDeviation(enumerable) / (Math.Pow(enumerable.Count(), 1 / 3));
            double powOfTen = Math.Pow(10, Math.Floor(Math.Log10(binWidth)));
            double relSize = binWidth / powOfTen; // guaranteed in [1, 10)

            // Automatic rule specified
            if (relSize < 1.5)
                binWidth = 1 * powOfTen;
            else if (relSize < 2.5)
                binWidth = 2 * powOfTen;
            else if (relSize < 4)
                binWidth = 3 * powOfTen;
            else if (relSize < 7.5)
                binWidth = 5 * powOfTen;
            else
                binWidth = 10 * powOfTen;

            // Put the bin edges at multiples of the bin width, covering x.The
            // actual number of bins used may not be exactly equal to the requested
            // rule.
            int leftEdge = (int)Math.Min(binWidth * Math.Floor(xmin / binWidth), xmin);
            int nbinsActual = (int)Math.Max(1, Math.Ceiling((xmax - leftEdge) / binWidth));
            int rightEdge = (int)Math.Max(leftEdge + nbinsActual * binWidth, xmax);

            int[] edges = new int[nbinsActual];
            int binEdge = (int)binWidth;
            for (int i = 0; i < nbinsActual; i++)
            {
                edges[i] = binEdge;
                binEdge += (int)binWidth;
            }

            return edges;
        }

        private static double StandardDeviation(IEnumerable<int> enumerable)
        {
            double average = enumerable.Average();

            double var = enumerable.Sum(x => Math.Pow(x - average, 2)) / (enumerable.Count() - 1);

            return Math.Sqrt(var);
        }

        internal static double[] DailyMeanFromPartial(int meanOfTheDayCounter, double[] partialMeanOfTheDay, List<double[]> histogramListAdjusted)
        {
            foreach(double[] singleHistogram in histogramListAdjusted)
            {
                for (int i = 0; i < singleHistogram.Length; i++)
                {
                    partialMeanOfTheDay[i] = (singleHistogram[i] - partialMeanOfTheDay[i]) / (meanOfTheDayCounter + 1) + partialMeanOfTheDay[i];
                }
                meanOfTheDayCounter++;
            }
            return partialMeanOfTheDay;
        }
    }
}
