﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OPCAutomation;
using System.Collections;
using Xml;

namespace LevelOne
{
    public enum WriteTagLevelOne
    {
        /*Eventos gerados pelo sistema (ESCRITA)*/
        eventWaterDetected=0,
        /*Valores gerados pelo sistema (ESCRITA)*/
        valueWpdTreshold,
        valueIndexSuperiorLo,
        valueIndexSuperiorCenter,
        valueIndexSuperiorLm,
        valueIndexInferiorLo,
        valueIndexInferiorCenter,
        valueIndexInferiorLm,
        /*Estado do sistema*/
        valueSystemStatus
    }
    class CommunicationLevelOne
    {
        private HandleXml confXmlHandler;

        private bool comunicationState = false;
        OPCAutomation.OPCServer opcServer;
        OPCAutomation.OPCGroup readGroup;
        OPCAutomation.OPCGroup writeGroup;

        string OPCServerName;
        string OPCNodeName;
        const string OPCReadGroupName = "READ";
        const string OPCWriteGroupName = "WRITE";
        int GroupUpdateRate; //ms
        OPCItem[] writeTAG;
        
        int TAGS_NUMBER_LEVEL_TWO = Enum.GetNames(typeof(SignalsLevelOne)).GetLength(0);
        int WRITE_TAGS_NUMBER_LEVEL_ONE = Enum.GetNames(typeof(WriteTagLevelOne)).GetLength(0);

        Tag[] tag;
        bool hardeningStart = false;
        bool hardeningEnd = false;
        DateTime systemTimeInitial;

        public int transactionID;

        /// <summary>
        /// Dummy constructor
        /// </summary>
        public CommunicationLevelOne()
        {

        }

        public CommunicationLevelOne(string systemPath, DateTime systemTimeInitial, string OPCNodeName)
        {
            this.confXmlHandler = new HandleXml(System.IO.Path.GetFullPath(systemPath + @"conf"), "configuracaoSistema");
            this.confXmlHandler.LoadRequiredFile();

            this.OPCServerName = confXmlHandler.getNodeValue("OPCServerName");
            this.OPCNodeName = OPCNodeName;
            this.GroupUpdateRate = Int32.Parse(confXmlHandler.getNodeValue("GroupUpdateRate")); //ms

            this.systemTimeInitial = systemTimeInitial;

            /*Cria a área de memória das TAGs no sistema de nível 2 que serão alimentadas pelas tags do nível 1 via OPC*/
            tag = new Tag[TAGS_NUMBER_LEVEL_TWO];
            /*Cria a área de memória das tags de escrita no nível 1*/
            writeTAG = new OPCItem[WRITE_TAGS_NUMBER_LEVEL_ONE];

            try
            {
                for (int i = 0; i < tag.Length; i++ )
                    tag[i] = new Tag();

                //Create a new OPC Server object
                opcServer = new OPCAutomation.OPCServer();

                this.init();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                comunicationState = false;
            }
        }

        public bool isOk()
        {
            return this.comunicationState;
        }

        public string getOPCNodeName()
        {
            //return this.OPCNodeName;
            return "Ajustar Classe OPC";
        }

        public void init()
        {
            //Attempt to connect with the server
            try
            {
                opcServer.Connect(OPCServerName, OPCNodeName);

                //                 MessageBox.Show("Não foi possível conectado ao servidor " + remoteServerName + ".", "Erro");

                opcServer.OPCGroups.DefaultGroupIsActive = true;

                //Set the desired percent deadband
                opcServer.OPCGroups.DefaultGroupDeadband = 0;

                //Add the group and set its update rate
                readGroup = opcServer.OPCGroups.Add(OPCReadGroupName);

                //Set the update rate for the group
                readGroup.UpdateRate = GroupUpdateRate;

                readGroup.IsSubscribed = true;

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("eventHardeningStart"), (int)SignalsLevelOne.eventHardeningStart);
                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("eventHardeningEnd"), (int)SignalsLevelOne.eventHardeningEnd);

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueMotherCoilIdFirstPart"), (int)SignalsLevelOne.valueMotherCoilIdFirstPart);
                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueMotherCoilIdSecondPart"), (int)SignalsLevelOne.valueMotherCoilIdSecondPart);
                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueMotherCoilIdThirdPart"), (int)SignalsLevelOne.valueMotherCoilIdThirdPart);

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueDaughterCoilIdFirstPart"), (int)SignalsLevelOne.valueDaughterCoilIdFirstPart);
                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueDaughterCoilIdSecondPart"), (int)SignalsLevelOne.valueDaughterCoilIdSecondPart);
                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueDaughterCoilIdThirdPart"), (int)SignalsLevelOne.valueDaughterCoilIdThirdPart);

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueWindingLength"), (int)SignalsLevelOne.valueWindingLength);

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueRealSpeed"), (int)SignalsLevelOne.valueRealSpeed);

                readGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueStripWidth"), (int)SignalsLevelOne.valueStripWidth);
                

                readGroup.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(DataChangeHandler);
                /*TAG de escrita no nível 2*/

                writeGroup = opcServer.OPCGroups.Add(OPCWriteGroupName);

                //Set the update rate for the group
                writeGroup.UpdateRate = GroupUpdateRate;

                writeGroup.IsSubscribed = true;

                writeTAG[(int)WriteTagLevelOne.eventWaterDetected] = writeGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("eventWaterDetected"), (int)WriteTagLevelOne.eventWaterDetected);
                writeTAG[(int)WriteTagLevelOne.valueWpdTreshold] = writeGroup.OPCItems.AddItem(confXmlHandler.getNodeValue("valueWpdTreshold"), (int)WriteTagLevelOne.valueWpdTreshold);

                for (int itemId = 0; itemId < confXmlHandler.getNodeValues("valueIndex").Length; itemId++)
                {
                    string tagName = confXmlHandler.getNodeValues("valueIndex").ElementAt(itemId);

                    if (confXmlHandler.getAttributeValue("valueIndex", itemId, "face").Equals("superior"))
                    {
                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("lo"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorLo] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexSuperiorLo);
                        }

                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("center"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorCenter] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexSuperiorCenter);
                        }

                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("lm"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorLm] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexSuperiorLm);
                        }
                    }

                    if (confXmlHandler.getAttributeValue("valueIndex", itemId, "face").Equals("inferior"))
                    {
                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("lo"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexInferiorLo] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexInferiorLo);
                        }

                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("center"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexInferiorCenter] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexInferiorCenter);
                        }

                        if (confXmlHandler.getAttributeValue("valueIndex", itemId, "position").Equals("lm"))
                        {
                            writeTAG[(int)WriteTagLevelOne.valueIndexInferiorLm] = writeGroup.OPCItems.AddItem(tagName, (int)WriteTagLevelOne.valueIndexInferiorLm);
                        }
                    }
                }
                 

                comunicationState = true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                comunicationState = false;
            }
        }

        public Boolean groupIsConnected()
        {
            //return readGroup.IsActive;
            return false;
        }

        public Boolean serverIsOK()
        {
            ///*Verifica primeiramente se a conexão foi estabelecida, ou seja, o servidor está na rede*/
            //if (this.isOk())
            //{

            //    /*Caso esteja na rede avalia seu estado*/
            //    if (opcServer.ServerState.Equals(OPCServerState.OPCFailed))
            //    {
            //        return false;
            //    }

            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        /*Função chamada pelo servidor OPC*/
        private void DataChangeHandler(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            transactionID = TransactionID;

            /*Elimina as atualizações dos eventos por causa da criação do callback*/
            for (int i = 1; i <= NumItems; i++)
            {
                int hc = (int)ClientHandles.GetValue(i);
                   
                tag[hc].value = ItemValues.GetValue(i);

                tag[hc].timeStamp = (DateTime)TimeStamps.GetValue(i);
                tag[hc].quality = (int)Qualities.GetValue(i);

                /*Esta checagem elimina o problema de duas transições na tag de inicio da dessulfiração*/
                if (DateTime.Now > (systemTimeInitial + TimeSpan.FromSeconds(1)))
                {
                    /*início de encruamento*/
                    if (hc == (int)SignalsLevelOne.eventHardeningStart)
                    {
                        if ((Boolean)tag[hc].value)
                        {
                            setHardeningStart(true);
                            tag[(int)SignalsLevelOne.eventHardeningStart].timeStamp = DateTime.Now;
                        }
                        //else
                        //{
                        //    setHardeningStart(false);
                        //}
                    }

                    /*fim do encruamento*/
                    if (hc == (int)SignalsLevelOne.eventHardeningEnd)
                    {
                        if ((Boolean)tag[hc].value)
                        {
                            setHardeningEnd(true);
                            tag[(int)SignalsLevelOne.eventHardeningEnd].timeStamp = DateTime.Now;
                        }
                        //else
                        //{
                        //    setHardeningEnd(false);
                        //}
                    }
                }
            }
        }

        public void terminateCommunication()
        {
            opcServer.Disconnect();                               // disconnect the server
        }

        public bool isHardeningStart()
        {
            return true;
            //return hardeningStart;
        }

        public bool isHardeningEnd()
        {
            return false;
            //return hardeningEnd;
        }

        public DateTime getProcessTimeStampInitial()
        {
                return tag[(int)SignalsLevelOne.eventHardeningStart].timeStamp;
        }

        public DateTime getProcessTimeStampFinal()
        {
                return tag[(int)SignalsLevelOne.eventHardeningEnd].timeStamp;
        }

        public Boolean tagIsGood(object quality)
        {
            return (OPCQuality)quality != OPCQuality.OPCQualityBad;
        }
        /*Não funciona. Após várias tentativas não conseguimos fazer leitura síncrona. Deve ser algum parâmetro*/
        public void syncGroupRead()
        {
            int NumItems = 2;//; tag.Length;
            Array ClientHandles = new int[NumItems];
            Array ItemValues;// = new object[NumItems];
            Array Errors;// = new int[NumItems]; //opc server will store any errors in this array
            object Qualities;// = new object[NumItems];//opc server will store the quality of the item 
            object TimeStamps;// = new object[NumItems]; ; //store the timestamp of the read
            int CancelId;

            //for(int i=0; i< readGroup.OPCItems.Count; i++)
            //{
            //    ((int[])ClientHandles)[i+1] = readGroup.OPCItems.Item(i+1).ClientHandle;
            //}
            ((int[])ClientHandles)[0] = 1;

            ((int[])ClientHandles)[1] = readGroup.OPCItems.Item(3).ClientHandle;

            //read directly from device
            readGroup.SyncRead((short)OPCDataSource.OPCDevice, NumItems, ref ClientHandles, out ItemValues, out Errors, out Qualities, out TimeStamps);
            //readGroup.SyncRead(NumItems, ref ClientHandles, out Errors, transactionID, out CancelId);
            /*Elimina as atualizações dos eventos por causa da criação do callback*/
            //for (int i = 1; i <= NumItems; i++)
            //{
            //    int hc = (int)ClientHandles.GetValue(i);
                   
            //    tag[hc].value = ItemValues.GetValue(i);

            //    Array TimeStampsAux = (Array)TimeStamps;
            //    Array QualitiesAux = (Array)Qualities;

            //    tag[hc].timeStamp = (DateTime)TimeStampsAux.GetValue(i);
            //    tag[hc].quality = (int)QualitiesAux.GetValue(i);
            //}
        }

        public string getMotherCoilId()
        {
            string p1, p2, p3;

            //syncGroupRead();
            
            p1 = this.tag[(int)SignalsLevelOne.valueMotherCoilIdFirstPart].value.ToString();
            p2 = this.tag[(int)SignalsLevelOne.valueMotherCoilIdSecondPart].value.ToString();
            p3 = this.tag[(int)SignalsLevelOne.valueMotherCoilIdThirdPart].value.ToString();

            return p1 + p2 + p3;
        }

        public string getDaughterCoilId()
        {
            string p1, p2, p3;

            //syncGroupRead();

            p1 = this.tag[(int)SignalsLevelOne.valueDaughterCoilIdFirstPart].value.ToString();
            p2 = this.tag[(int)SignalsLevelOne.valueDaughterCoilIdSecondPart].value.ToString();
            p3 = this.tag[(int)SignalsLevelOne.valueDaughterCoilIdThirdPart].value.ToString();

            return p1 + p2 + p3; 
        }

        public double getWindingLength()
        {
            if (tagIsGood(this.tag[(int)SignalsLevelOne.valueWindingLength].quality))
            {
                return (float)this.tag[(int)SignalsLevelOne.valueWindingLength].value;
            }
            else
            {
                return -1;
            }
        }

        public double getStripWidth()
        {
            /*Qualidade forçada pois em vários momentos onde a qualidade da TAG no quick opc client aparece como good*/
            /*utilizado a interface OPC dentro do sistema essa mesma qualidade aparece como bad */
            if (true)//tagIsGood(this.tag[(int)SignalsLevelOne.valueStripWidth].quality))
            {
                return (float)this.tag[(int)SignalsLevelOne.valueStripWidth].value;
            }
            else
            {
                return -1;
            }
        }

        public double getRealSpeed()
        {
            if (tagIsGood(this.tag[(int)SignalsLevelOne.valueRealSpeed].quality))
            {
                return (float)this.tag[(int)SignalsLevelOne.valueRealSpeed].value;
            }
            else
            {
                return -1;
            }
        }

        public void setHardeningStart(bool state)
        {
            this.hardeningStart = state;
            this.hardeningEnd = !state;
        }

        public void setHardeningEnd(bool state)
        {
            this.hardeningEnd = state;
            this.hardeningStart = !state;
        }

        public void setWaterDetected(bool state)
        {
            if (state)
            {
                writeTAG[(int)WriteTagLevelOne.eventWaterDetected].Write(1);
                tag[(int)SignalsLevelOne.eventWaterDetected].value = 1;
            }
            else
            {
                writeTAG[(int)WriteTagLevelOne.eventWaterDetected].Write(0);
                tag[(int)SignalsLevelOne.eventWaterDetected].value = 0;
            }

        }

        public void setIndexesPerFace(string faceName, double value)
        {
            setIndex(value, faceName, "all", "lo");
            setIndex(value, faceName, "all", "center");
            setIndex(value, faceName, "all", "lm");
        }

        public void setWpdTreshold(double value)
        {
            writeTAG[(int)WriteTagLevelOne.valueWpdTreshold].Write((float)value);
        }

        public void setIndex(double value, string face, string camera, string position)
        {
            if(face.Equals("superior"))
            {
                if(position.Equals("lo"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorLo].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexSuperiorLo].value = value;
                }

                if (position.Equals("center"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorCenter].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexSuperiorCenter].value = value;
                }

                if (position.Equals("lm"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexSuperiorLm].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexSuperiorLm].value = value;
                }
            }

            if (face.Equals("inferior"))
            {
                if (position.Equals("lo"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexInferiorLo].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexInferiorLo].value = value;
                }

                if (position.Equals("center"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexInferiorCenter].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexInferiorCenter].value = value;
                }

                if (position.Equals("lm"))
                {
                    writeTAG[(int)WriteTagLevelOne.valueIndexInferiorLm].Write((float)value);
                    tag[(int)SignalsLevelOne.valueIndexInferiorLm].value = value;
                }
            }

        }


        //public void setSlagIndexNormalizedSulfur(double value)
        //{
        //    writeTAG[(int)WriteTagLevelOne.VALUE_INDEX_NORM_SULFUR].Write((float)value);
        //}

        //public void setSlagIndexTresholdUpperBoundNormalizedSulfur(double value)
        //{
        //    writeTAG[(int)WriteTagLevelOne.VALUE_TRESHOLD_UPPER_BOUND_INDEX_NORM_SULFUR].Write((float)value);
        //}

        //public void setSulfurIndex(bool state)
        //{
        //    if (state)
        //    {
        //        writeTAG[(int)WriteTagLevelOne.VALUE_ESCUMAGEM_OK].Write(1);
        //        tag[(int)SignalsLevelOne.VALUE_ESCUMAGEM_OK].value = 1;
        //    }
        //    else
        //    {
        //        writeTAG[(int)WriteTagLevelOne.VALUE_ESCUMAGEM_OK].Write(0);
        //        tag[(int)SignalsLevelOne.VALUE_ESCUMAGEM_OK].value = 0;
        //    }
        //}

        public void setSystemStatus(short value)
        {
            writeTAG[(int)WriteTagLevelOne.valueSystemStatus].Write(value);
        }

        //public bool getQualityOk()
        //{
        //    if((int)tag[(int)SignalsLevelOne.VALUE_ESCUMAGEM_OK].value == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}
