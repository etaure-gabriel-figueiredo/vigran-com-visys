﻿using AsynchronousGrab;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cameraUtil
{
    class AsynchronousAquisition
    {
        private VimbaHelper m_VimbaHelper = null;
        
        private CameraInfo camFG = null;

        private Bitmap imageRGB;

        private int FrameCount = 0;

        private void OnCameraListChanged(object sender, EventArgs args)
        {
            //throw new NotImplementedException();
        }
        public AsynchronousAquisition()
        {
            //  start VimbaNET API

            // Start up Vimba SDK
            m_VimbaHelper = new VimbaHelper();
            m_VimbaHelper.Startup(this.OnCameraListChanged);

            //  open cameras
            List<CameraInfo> cameras = m_VimbaHelper.CameraList;

            //Vira de arquiv9o de configuração
            string nameCamFG ="DEV_000F315B948E";

            foreach (CameraInfo camAux in cameras)
            {
                if (camAux.ID.Equals(nameCamFG))
                {
                    camFG = camAux;
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            try
            {
                m_VimbaHelper.OpenCamera(camFG.ID);
            }
            catch
            {
                throw new NotImplementedException();
            }

            try
            {
                m_VimbaHelper.StartContinuousImageAcquisition(handleNewFrame);
                //previousSampleInstant = DateTime.Now;
            }
            catch (Exception exception)
            {
                //LogError("Could not start continuous image aquisition. Reason: " + exception.Message);
            }

            while (true) { };
        }

        private void handleNewFrame(object sender, FrameEventArgs args)
        {
            FrameCount++;

            System.Console.WriteLine(FrameCount.ToString());
        }
    }
}
