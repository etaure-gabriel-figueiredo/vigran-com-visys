﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelOne
{
    class Tag
    {
        public string name = (string)"";
        public object value  = 0f;
        public DateTime timeStamp = new DateTime(0);
        public object quality = OPCQuality.OPCQualityBad;

    }
}
