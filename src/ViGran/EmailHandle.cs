﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BasicUtil
{
    public class EmailHandling
    {
        MailMessage mail;
        SmtpClient client;
        string fromAddress;
        string toAddress;

        public EmailHandling(string fromAddress, string toAddress)
        {
            this.fromAddress = fromAddress;
            this.toAddress = toAddress;

            mail = new MailMessage(fromAddress, toAddress);

            client = new SmtpClient();

            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "smtp.tubarao.com.br";
        }

        public void sendEmail(string subject, string body)
        {
            mail.Subject = subject;
            mail.Body = body;
            try
            {
                client.Send(mail);
            }catch(Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Email não enviado. Erro: " + e.ToString(), "Erro"); ;
            }
            
        }
    }
}
