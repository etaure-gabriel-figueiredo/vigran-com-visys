﻿/*=============================================================================
  Copyright (C) 2012 Allied Vision Technologies.  All Rights Reserved.

  Redistribution of this file, in original or modified form, without
  prior written consent of Allied Vision Technologies is prohibited.

-------------------------------------------------------------------------------

  File:        Program.cs

  Description: The main entry point of the AsynchronousGrab example of VimbaNET.

-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

namespace WPD_V1
{
    using BasicUtil;
    using cameraUtil;
    using LevelOne;
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Windows.Forms;

    /// <summary>
    /// Initializes a new
    /// </summary>
    internal static class Program
    {
        public static void threadLevelOneCommunication(LoadSystemConfiguration systemConfiguration)
        {
            DateTime systemTimeInitial = DateTime.Now;

            CommunicationLevelOne levelOneCommunication;
            CommunicationLevelOne levelOneCommunicationPrimary = new CommunicationLevelOne(systemConfiguration.systemPath, systemTimeInitial, systemConfiguration.oPCNodeNamePrimary);
            CommunicationLevelOne levelOneCommunicationSecondary = new CommunicationLevelOne(systemConfiguration.systemPath, systemTimeInitial, systemConfiguration.oPCNodeNameSecondary);

            while (true)
            {
                if (levelOneCommunicationPrimary.isOk())
                {
                    levelOneCommunication = levelOneCommunicationPrimary;
                }
                else
                {
                    levelOneCommunication = levelOneCommunicationSecondary;
                }
            }
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
            //LoadSystemConfiguration systemConfiguration = new LoadSystemConfiguration();

            /*level one*/
            //Thread levelOneCommunicationThread = new Thread(() => threadLevelOneCommunication(systemConfiguration));

            //levelOneCommunicationThread.Start();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

            //Application.Run(new Form());
        }
    }
}