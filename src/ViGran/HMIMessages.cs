﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    public static class HMIMessages
    {
        private static readonly String SYSTEM_START = "System Started";
        private static readonly String CAMERA_FOUND = "FG camera found";
        private static readonly String CAMERA_NOT_AVAILABLE = "FG camera not available. Check camera's power and server ethernet connection";
        private static readonly String CAMERA_OPENED = "Camera with id '{0}' has been opened";
        private static readonly String CAMERA_COULD_NOT_OPEN = "Camera with id '{0}' could not be opened";
        private static readonly String CAMERA_SETTINGS_LOAD_FAIL = "Could not load camera settings. Reason: {0}";
        private static readonly String VIMBA_STARTED = "VimbaNET has been started";
        private static readonly String CAMERA_LINK_OK = "Camera Link: OK";
        private static readonly String CAMERA_LINK_NOK = "Camera Link: NOK";
        private static readonly String OPC_SERVER_OK = "Servidor OPC - {0}: OK";
        private static readonly String OPC_SERVER_NOK = "Servidor OPC - {0}: NOK";
        private static readonly String OPC_GROUP_OK = "Grupo OPC: OK";
        private static readonly String OPC_GROUP_NOK = "Grupo OPC: NOK";
        private static readonly String DATA_HORA = "Data e Hora: {0}";
        private static readonly String LOGGED_USER = "Usuário: {0}";
        private static readonly String APPLICATION_LOAD_ERROR = "Erro desconhecido ao iniciar aplicação. Erro: {0}";
        private static readonly String CAMERA_LOAD_ERROR = "Erro desconhecido ao iniciar câmera. Erro: {0}";
        private static readonly String UPDATE_SYSTEM_INFO_ERROR = "Erro desconhecido ao atualizar informações do sistema. Erro: {0}";
        private static readonly String UPDATE_SCREEN_ERROR = "Erro desconhecido ao atualizar tela do sistema. Erro: {0}";
        private static readonly String CONTINUOUS_AQUISITION_ERROR = "Could not start continuous image aquisition. Reason: {0}";
        private static readonly String VIMBA_SHUTDOWN_ERROR = "Could not shutdown Vimba API. Reason: {0}";
        private static readonly String MAIN_CALC_ERROR = "Main Calculation unknow error. Reason: {0}";
        private static readonly String HANDLE_FRAME_ERROR = "Handle Error unknow error. Reason: {0}";
        private static readonly String CHANGE_PARAMETER_ERROR = "Change Parameter unknow error. Reason: {0}";
        private static readonly String SYSTEM_STOP = "System Stopped";
        private static readonly String DISK_SPACE_WARN = @"Disk Space Warning! Only {0}% of disk available. System will automatically delete data when free space is less than {1}%.";
        private static readonly String DISK_DELETE_WARN = @"Disk Space Warning! Deleting Old Data!";
        private static readonly String OLD_FOLDER_DELETED = @"Zipped folder deleted: {0}";


        /// <summary>
        /// Mensagem de Sistema Inicializado
        /// </summary>
        /// <returns></returns>
        public static String SystemStarted()
        {
            return SYSTEM_START;
        }

        /// <summary>
        /// Mensagem de Sistema Desligado
        /// </summary>
        /// <returns></returns>
        public static String SystemStopped()
        {
            return SYSTEM_STOP;
        }

        /// <summary>
        /// Exception ao alterar os parâmetros da aplicação
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static String ChangeParameterError(string errorMessage)
        {
            return string.Format(CHANGE_PARAMETER_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao processar frame vindo da câmera
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static String HandleFrameError(string errorMessage)
        {
            return string.Format(HANDLE_FRAME_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao realizar cálculo principal
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static String MainCalculationError(string errorMessage)
        {
            return string.Format(MAIN_CALC_ERROR, errorMessage);
        }

        /// <summary>
        /// Excetion ao encerrar o Vimba
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static String VimbaShutdownError(String errorMessage)
        {
            return String.Format(VIMBA_SHUTDOWN_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao iniciar aquisição de imagens
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static String ContinuousAquisitionError(string errorMessage)
        {
            return string.Format(CONTINUOUS_AQUISITION_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao atualizar tela do Sistema
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static string UpdateScreenError(string errorMessage)
        {
            return string.Format(UPDATE_SCREEN_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao atualizar informações do Sistema
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static string UpdateSystemInfoError(string errorMessage)
        {
            return string.Format(UPDATE_SYSTEM_INFO_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao inicar câmera
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static string CameraLoadError(string errorMessage)
        {
            return string.Format(CAMERA_LOAD_ERROR, errorMessage);
        }

        /// <summary>
        /// Exception ao iniciar aplicação
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static string ApplictaionLoadError(string errorMessage)
        {
            return string.Format(APPLICATION_LOAD_ERROR, errorMessage);
        }

        /// <summary>
        /// Mensagem de Link de Câmera NOK
        /// </summary>
        /// <returns></returns>
        public static string CameraLinkNOK()
        {
            return CAMERA_LINK_NOK;
        }

        /// <summary>
        /// Mensagem de Server OPC OK
        /// </summary>
        /// <param name="opcServerName">Server OPC</param>
        /// <returns></returns>
        public static string OPCServerOK(String opcServerName)
        {
            return string.Format(OPC_SERVER_OK, opcServerName);
        }

        /// <summary>
        /// Mensagem de server OPC NOK
        /// </summary>
        /// <param name="opcServerName">Server OPC</param>
        /// <returns></returns>
        public static string OPCServerNOK(String opcServerName)
        {
            return string.Format(OPC_SERVER_NOK, opcServerName);
        }

        /// <summary>
        /// Mensagem de Grupo OPC NOK
        /// </summary>
        /// <returns></returns>
        public static string OPCGroupOK()
        {
            return OPC_GROUP_OK;
        }

        /// <summary>
        /// Mensagem de grupo OPC OK
        /// </summary>
        /// <returns></returns>
        public static string OPCGroupNOK()
        {
            return OPC_GROUP_NOK;
        }

        /// <summary>
        /// Mensagem de Data e hora Atual do Sistema
        /// </summary>
        /// <param name="date">Data atual</param>
        /// <returns></returns>
        public static string DataHora(DateTime date)
        {
            return string.Format(DATA_HORA, date.ToShortDateString() + " " + date.ToShortTimeString());
        }

        /// <summary>
        /// Mensagem de Usuário Logado
        /// </summary>
        /// <param name="userName">Nome do usuário logado</param>
        /// <returns></returns>
        public static string LoggedUser(String userName)
        {
            return string.Format(LOGGED_USER, userName);
        }

        /// <summary>
        /// Mensagem de Câmera Encontrada
        /// </summary>
        /// <returns></returns>
        public static String CameraFound()
        {
            return CAMERA_FOUND;
        }

        /// <summary>
        /// Mensagem de câmera não disponível
        /// </summary>
        /// <returns></returns>
        public static String CameraNotAvailable()
        {
            return CAMERA_NOT_AVAILABLE;
        }

        /// <summary>
        /// Mensagem de Câmera Aberta
        /// </summary>
        /// <param name="camID">ID da câmera</param>
        /// <returns></returns>
        public static String CameraOpenned(String camID)
        {
            return string.Format(CAMERA_OPENED, camID);
        }

        /// <summary>
        /// Mensagem de Câmera não encontrada
        /// </summary>
        /// <param name="camID">ID da Câmera</param>
        /// <returns></returns>
        public static String CameraCouldNotOpenned(String camID)
        {
            return string.Format(CAMERA_COULD_NOT_OPEN, camID);
        }

        /// <summary>
        /// Mensagem de Falha ao abrir câmera
        /// </summary>
        /// <param name="reason">Razão da câmera não ser aberta</param>
        /// <returns></returns>
        public static String CameraSettingsLoadFail(String reason)
        {
            return string.Format(CAMERA_SETTINGS_LOAD_FAIL, reason);
        }

        /// <summary>
        /// Mensagem de Câmera Encontrada
        /// </summary>
        /// <returns></returns>
        public static String VimbaStarted()
        {
            return VIMBA_STARTED;
        }

        /// <summary>
        /// Mensagem de Link de Câmera OK
        /// </summary>
        /// <returns></returns>
        public static String CameraLinkOk()
        {
            return CAMERA_LINK_OK;
        }

        /// <summary>
        /// Mensagem de alerta de disco cheio
        /// </summary>
        /// <param name="warningSpace"></param>
        /// <param name="deletingSpace"></param>
        /// <returns></returns>
        public static String DiskSpaceWarning(float warningSpace, float deletingSpace)
        {
            return string.Format(DISK_SPACE_WARN, warningSpace.ToString("0.0000"), deletingSpace.ToString("0.0000"));
        }

        /// <summary>
        /// Mensagem de alerta de deleção de pastas
        /// </summary>
        /// <returns></returns>
        public static String DiskDeleteData()
        {
            return DISK_DELETE_WARN;
        }

        /// <summary>
        /// Mensagem contendo pasta deletada do sistema
        /// </summary>
        /// <param name="zippedFolderName"></param>
        /// <returns></returns>
        public static String DeletedOldFolder(string zippedFolderName)
        {
            return string.Format(OLD_FOLDER_DELETED, zippedFolderName);
        }

    }
}
