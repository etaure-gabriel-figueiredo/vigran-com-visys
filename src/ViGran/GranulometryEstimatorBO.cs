﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    class GranulometryEstimatorBO
    {
        private ImageProcessing imageProc;

        // Parameters
        private float thresholdHigh;
        private float thresholdLow;
        private int apertureSize;
        private bool l2Gradient;
        private int particleMaxSize;
        private int particleMinSize;

        // Results
        private double[] histx = new double[10];
        private double[] histy = new double[10];

        public float ThresholdHigh { get; set; }
        public float ThresholdLow { get; set; }
        public int ApertureSize { get; set; }
        public bool L2Gradient { get; set; }
        public double[] Histx { get; set; }
        public double[] Histy { get; set; }
        public int ParticleMaxSize { get; set; }
        public int ParticleMinSize { get; set; }

        internal ImageProcessing ImageProc { get; set; }

        public double PixelPerMm { get; set; }
        public List<double> GrainSizeRangeList { get; set; }
        public double[] GrainSizeRangeWeightList { get; set; }

        public double[] HistogramXAdjusted { get; set; }
        public double[] HistogramXNotAdjusted { get; set; }
        public double[] HistogramYAdjusted { get; set; }
        public double[] HistogramYNotAdjusted { get; set; }

        public GranulometryEstimatorBO()
        {
        }

        public GranulometryEstimatorBO(ImageProcessing imageProc)
        {
            this.ImageProc = imageProc;
        }

        public void CalculateGranulometry()
        {
            ImageProc.CannyEdgeDetector(ThresholdHigh, ThresholdLow, ApertureSize, L2Gradient);

            List<int> sizeHorizontal = ExtractHorizontalSizeInPixel(ImageProc.getBitmap(Channel.BIN));
            List<int> sizeVertical = ExtractVerticalSizeInPixel(ImageProc.getBitmap(Channel.BIN));

            //Transformar de pixel para mm
            List<double> sizeHorizontalMm = TransformPixelToMm(sizeHorizontal);
            List<double> sizeVerticalMm = TransformPixelToMm(sizeVertical);

            //Limitar segundo ParticleMinSize e ParticleMaxSize
            sizeHorizontalMm = sizeHorizontalMm.Where(x => x >= ParticleMinSize && x <= ParticleMaxSize).ToList();
            sizeVerticalMm = sizeVerticalMm.Where(x => x >= ParticleMinSize && x <= ParticleMaxSize).ToList();

            //Gera histogramas normalizados ajustado e não ajustado em X
            double[] histogramaX = Statistics.GenerateHistogram(sizeHorizontalMm, GrainSizeRangeList);
            HistogramXNotAdjusted = Statistics.NormalizeHistograma(histogramaX);
            Statistics.AdjustmentFunction(histogramaX, GrainSizeRangeWeightList);
            HistogramXAdjusted = Statistics.NormalizeHistograma(histogramaX);
            
            //Gera histogramas normalizados ajustado e não ajustado em Y
            double[] histogramaY = Statistics.GenerateHistogram(sizeVerticalMm, GrainSizeRangeList);
            HistogramYNotAdjusted = Statistics.NormalizeHistograma(histogramaY);
            Statistics.AdjustmentFunction(histogramaY, GrainSizeRangeWeightList);
            HistogramYAdjusted = Statistics.NormalizeHistograma(histogramaY);
        }

        private List<int> ExtractHorizontalSizeInPixel(Bitmap bitmap)
        {
            int particleSize;

            List<int> sizeHorizontal = new List<int>();

            for (int y = 0; y < bitmap.Height; y++)
            {
                particleSize = 0;
                for (int x = 0; x < bitmap.Width; x++)
                {
                    if (bitmap.GetPixel(x, y).R == 0)
                    {
                        particleSize++;
                    }
                    else
                    {
                        if (particleSize > 0) sizeHorizontal.Add(particleSize);
                        particleSize = 0;
                    }
                }
                if (particleSize > 0) sizeHorizontal.Add(particleSize);
            }
            return sizeHorizontal;
        }

        private List<int> ExtractVerticalSizeInPixel(Bitmap bitmap)
        {
            int particleSize;

            List<int> sizeVertical = new List<int>();

            for (int x = 0; x < bitmap.Width; x++)
            {
                particleSize = 0;
                for (int y = 0; y < bitmap.Height; y++)
                {
                    if (bitmap.GetPixel(x, y).R == 0)
                    {
                        particleSize++;
                    }
                    else
                    {
                        if(particleSize > 0 ) sizeVertical.Add(particleSize);
                        particleSize = 0;
                    }
                }
                if (particleSize > 0) sizeVertical.Add(particleSize);
            }
            return sizeVertical;
        }

        private List<double> TransformPixelToMm(List<int> histPx)
        {
            List<double> histMm = new List<double>();
            foreach (var item in histPx)
            {
                histMm.Add(item / PixelPerMm);
            }
            return histMm;
        }
    }
}
