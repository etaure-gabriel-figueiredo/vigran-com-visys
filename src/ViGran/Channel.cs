﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPD_V1
{
    /*------------------ Auxiliar numeration ------------------*/

    /*In the system an image has 4 channels as follows
                 
        Channel 0 -> Red Channel         (R)   from colored image   (CLR)
        Channel 1 -> Green Channel       (G)   from colored image   (CLR)
        Channel 2 -> Blue Channel        (B)   from colored image   (CLR)
        Channel 3 -> Infrared Channel    (NIR) from infrared image  (NIR)
                 
    */

    /*Image channels mapping*/
    public enum Channel
    {
        RED     = 0,
        GREEN   = 1,
        BLUE    = 2,
        NIR     = 3,
        /*Virtual Channels*/
        CLR     = 4,     /*This is not a real channel. Just used to mean the whole colored image*/
        SEG     = 5,     /*This is not a real channel. Just used to mean the segmented image*/
        BIN     = 6,     /*This is not a real channel. Just used to mean the binarized image*/
        SLG     = 7,     /*This is not a real channel. Just used to mean the slag image*/
        ROI     = 8,     /*This is not a real channel. Just used to mean the ROI image*/  
        /*Visualisation Channels*/
        VCLR     = 9,    /*This is not a real channel. */
        VNIR     = 10,    /*This is not a real channel. */
        CENTROI     = 11,     /*This is not a real channel. Just used to mean the ROI image*/ 
    };
}
