﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xml;

namespace BasicUtil
{
    public class LoadSystemConfiguration
    { 

        private HandleXml confXmlHandler;

        public string assistantSelector { get; set; }
        public string systemPath { get; set; }
        public string systemType { get; set; }
        public string systemName { get; set; }
        public string confFolder { get; set; }
        public string roiName { get; set; }
        public string logFolder { get; set; }
        public string parameterCameraClr { get; set; }
        public string parameterCameraNir { get; set; }
        public string patternCleanNirImage { get; set; }
        public string patternDurtyNirImage { get; set; }
        public Rectangle  initialRoi { get; set;}
        public string buildNumber { get; set; }

        public string smtpHost { get; set; }
        public string smtpPort { get; set; }
        public string emailAddressSystem { get; set; }
        public string[] alarmEmailAddressClient { get; set; }
        public string[] eventEmailAddressClient { get; set; }
        public string[] eventCompleteEmailAddressClient { get; set; }

        public string hourToSendDayReport { get; set; }

        public string[] hourToSendShiftReport { get; set; }

        public string oPCNodeNamePrimary { get; set; }
        public string oPCNodeNameSecondary { get; set; }

        public string[] eventShiftEmailAddressClient { get; set; }

        public string nameCameraClr { get; set; }
        public string nameCameraNir { get; set; }

        public string[] rangeIndexAdjSulfur { get; set; }
        public string processStationId { get; set; }

        public string valueStripWidth { get; set; }

        public string eventWaterDetected { get; set; }
        public string valueWpdTreshold { get; set; }

        public string[] valueIndex { get; set; }

        public string valueIndexSuperiorLo { get; set; }
        public string valueIndexSuperiorCenter { get; set; }
        public string valueIndexSuperiorLm { get; set; }

        public string systemFace { get; set; }

        /* Granulometry Parameters */
        public int thresholdHigh { get; set; }
        public int thresholdLow { get; set; }
        public int gaussianMaskSize { get; set; }
        public float sigmaforGaussianKernel { get; set; }
        public int apertureSize { get; set; }
        public bool l2Gradient { get; set; }
        public int particleMinSize { get; set; }
        public int particleMaxSize { get; set; }
        public double PixelPerMm { get; set; }
        public List<double> GrainSizeRangeList { get; set; }
        public double[] GrainSizeRangeWeightList { get; set; }

        /* Gerenciamento de disco */
        public float hdPercentageFreeSpaceWarning { get; set; }
        public float hdPercentageFreeSpaceOverwrite { get; set; }
        public int numberOfFilesToBeDeleted { get; set; }

        public LoadSystemConfiguration()
        {      
            this.systemPath = System.IO.Path.GetFullPath(@"..\..\..\..\..\");

            try
            {
                this.confXmlHandler = new HandleXml(systemPath + @"conf", @"configuracaoSistema");
                this.confXmlHandler.LoadRequiredFile();

                assistantSelector = confXmlHandler.getNodeValue("AssistantSelector");
                systemType = confXmlHandler.getNodeValue("system-type");
                systemName = confXmlHandler.getNodeValue("system-name");
                confFolder = confXmlHandler.getNodeValue("conf-folder");
                roiName    = confXmlHandler.getNodeValue("conf-roi-name");
                logFolder = confXmlHandler.getNodeValue("log-folder");
                parameterCameraClr = confXmlHandler.getNodeValue("conf-cam-clr");
                parameterCameraNir = confXmlHandler.getNodeValue("conf-cam-nir");
                patternCleanNirImage = confXmlHandler.getNodeValue("conf-clean-nir");
                patternDurtyNirImage = confXmlHandler.getNodeValue("conf-dirty-nir");

                initialRoi = new Rectangle(int.Parse(confXmlHandler.getAttributeValue("calculationRoi", "x")), int.Parse(confXmlHandler.getAttributeValue("calculationRoi", "y")), int.Parse(confXmlHandler.getAttributeValue("calculationRoi", "width")), int.Parse(confXmlHandler.getAttributeValue("calculationRoi", "heigth")));

                buildNumber = confXmlHandler.getNodeValue("versao");

                smtpHost = confXmlHandler.getNodeValue("SmtpHost");
                smtpPort = confXmlHandler.getNodeValue("SmtpPort");
                emailAddressSystem = confXmlHandler.getNodeValue("EmailAddressSystem");
                alarmEmailAddressClient = confXmlHandler.getNodeValues("AlarmEmailAddressClient");
                eventEmailAddressClient = confXmlHandler.getNodeValues("EventEmailAddressClient");
                eventCompleteEmailAddressClient = confXmlHandler.getNodeValues("EventCompleteEmailAddressClient");

                oPCNodeNamePrimary = confXmlHandler.getNodeValue("OPCNodeNamePrimary");
                oPCNodeNameSecondary = confXmlHandler.getNodeValue("OPCNodeNameSecondary");

                hourToSendDayReport = confXmlHandler.getNodeValue("HourToSendDayReport");
                hourToSendShiftReport = confXmlHandler.getNodeValues("HourToSendShiftReport");

                eventShiftEmailAddressClient = confXmlHandler.getNodeValues("EventShiftEmailAddressClient");

                nameCameraClr = confXmlHandler.getNodeValue("NameCameraColored");
                nameCameraNir = confXmlHandler.getNodeValue("NameCameraNearInfrared");

                rangeIndexAdjSulfur = confXmlHandler.getNodeValues("rangeIndexAdjSulfur");

                processStationId = confXmlHandler.getNodeValue("processStationId");
                valueStripWidth = confXmlHandler.getNodeValue("valueStripWidth");

                eventWaterDetected = confXmlHandler.getNodeValue("eventWaterDetected");

                valueWpdTreshold = confXmlHandler.getNodeValue("valueWpdTreshold");

                valueIndex = confXmlHandler.getNodeValues("valueIndex");

                valueIndexSuperiorLo = confXmlHandler.getNodeValue("valueIndexSuperiorLo");
                valueIndexSuperiorCenter = confXmlHandler.getNodeValue("valueIndexSuperiorCenter");
                valueIndexSuperiorLm = confXmlHandler.getNodeValue("valueIndexSuperiorLm");

                systemFace = confXmlHandler.getNodeValue("systemFace");

                thresholdHigh = int.Parse(confXmlHandler.getNodeValue("thresholdHigh"));
                thresholdLow = int.Parse(confXmlHandler.getNodeValue("thresholdLow"));
                gaussianMaskSize = int.Parse(confXmlHandler.getNodeValue("gaussianMaskSize"));
                sigmaforGaussianKernel = float.Parse(confXmlHandler.getNodeValue("sigmaforGaussianKernel"));
                apertureSize = int.Parse(confXmlHandler.getNodeValue("apertureSize"));
                l2Gradient = bool.Parse(confXmlHandler.getNodeValue("l2Gradient"));

                particleMinSize = int.Parse(confXmlHandler.getNodeValue("particleMinSize"));
                particleMaxSize = int.Parse(confXmlHandler.getNodeValue("particleMaxSize"));

                PixelPerMm = double.Parse(confXmlHandler.getNodeValue("pixelPerMm"));

                /* Gerenciamento de disco */
                hdPercentageFreeSpaceWarning = float.Parse(confXmlHandler.getNodeValue("hdPercentageFreeSpaceWarning"));
                hdPercentageFreeSpaceOverwrite = float.Parse(confXmlHandler.getNodeValue("hdPercentageFreeSpaceOverwrite"));
                if (hdPercentageFreeSpaceOverwrite >= hdPercentageFreeSpaceWarning)
                    throw new XmlException("Disk Free Space Overwrite limit 'hdPercentageFreeSpaceOverwrite' should be less than Disk Free Space Warning limit 'hdPercentageFreeSpaceWarning'");

                numberOfFilesToBeDeleted = int.Parse(confXmlHandler.getNodeValue("numberOfFilesToBeDeleted"));
                if (numberOfFilesToBeDeleted <= 0)
                    throw new XmlException("Number of zipped files to be deleted 'numberOfFilesToBeDeleted' should be more than 0 ");


                double[] faixas = Array.ConvertAll(confXmlHandler.getNodeValues("grainSizeRange"), delegate (string s) { return double.Parse(s); });
                if (faixas.Length == 0)
                    throw new XmlException("Faixas de granulometria não definidas.");
                GrainSizeRangeList = new List<double>(faixas);
                GrainSizeRangeList.Add(double.MaxValue);

                GrainSizeRangeWeightList = Array.ConvertAll(confXmlHandler.getNodeValues("grainSizeRangeWeight"), delegate (string s) { return double.Parse(s); });
                if (GrainSizeRangeWeightList.Length != GrainSizeRangeList.Count)
                    throw new XmlException("Número de elementos 'grainSizeRangeWeight' deve ser igual ao número de elementos de 'grainSizeRange' mais 1.");
            }
            catch (XmlException ex)
            {
                string errorMessage = "Erro de leitura no arquivo de configuração.\n\n";

                errorMessage += ex.Message;

                throw new XmlException(errorMessage, ex);
            }
        }


    }
}
