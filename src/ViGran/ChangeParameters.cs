﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WPD_V1
{
    public partial class ChangeParametersView : Form
    {

        public float ThresholdHigh { get; set; }
        public float ThresholdLow { get; set; }
        public int ApertureSize { get; set; }
        public bool L2Gradient { get; set; }
        public int ParticleMinSize { get; set; }
        public int ParticleMaxSize { get; set; }
        public int ImagesMinCount { get; set; }
        public int ImagesMaxCount { get; set; }
        public string[] ImagesOffline { get; set; }
        public string ImagesOfflinePath { get; set; }

        public ChangeParametersView()
        {
            InitializeComponent();
        }

        private void ChangeParametersView_Shown(object sender, EventArgs e)
        {
            txtTH.Text = ThresholdHigh.ToString();
            txtTL.Text = ThresholdLow.ToString();
            txtApertureSize.Text = ApertureSize.ToString();
            checkBoxL2Gradient.Checked = L2Gradient;
            txtParticleMaxSize.Text = ParticleMaxSize.ToString();
            txtParticleMinSize.Text = ParticleMinSize.ToString();
            txtImagesMaxCount.Text = ImagesMaxCount.ToString();
            txtImagesMinCount.Text = ImagesMinCount.ToString();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (!IsMandatoryFieldsOK())
            {
                return;
            }

            if (MainForm.running)
            {
                System.Windows.Forms.MessageBox.Show("Não é possível a alteração dos parâmetros pois o sistema está em estado 'Running'.");
            }
            else
            {
                try
                {
                    ThresholdHigh = float.Parse(txtTH.Text);
                    ThresholdLow = float.Parse(txtTL.Text);
                    ApertureSize = int.Parse(txtApertureSize.Text);
                    L2Gradient = checkBoxL2Gradient.Checked;
                    ParticleMaxSize = int.Parse(txtParticleMaxSize.Text);
                    ParticleMinSize = int.Parse(txtParticleMinSize.Text);
                    ImagesMaxCount = int.Parse(txtImagesMaxCount.Text);
                    ImagesMinCount = int.Parse(txtImagesMinCount.Text);

                    if (String.IsNullOrEmpty(txtImagesPath.Text))
                    {
                        ImagesOffline = null;
                    }
                    else
                    {
                        ImagesOffline = Directory.GetFiles(txtImagesPath.Text);
                    }
                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ViGran", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool IsMandatoryFieldsOK()
        {
            bool error = false;
            StringBuilder errorMessage = new StringBuilder("Mandatory Fields:");

            /* Verificando se campos foram informados */
            if (string.IsNullOrEmpty(txtTH.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Threshold High");
            }

            if (string.IsNullOrEmpty(txtTL.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Threshold Low");
            }

            if (string.IsNullOrEmpty(txtApertureSize.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Aperture Size");
            }

            if (string.IsNullOrEmpty(txtParticleMinSize.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Particle Min Size");
            }

            if (string.IsNullOrEmpty(txtParticleMaxSize.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Particle Max Size");
            }

            if (string.IsNullOrEmpty(txtParticleMinSize.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Particle Min Size");
            }

            if (string.IsNullOrEmpty(txtImagesMaxCount.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Images Max Count");
            }

            if (string.IsNullOrEmpty(txtImagesMinCount.Text.Trim()))
            {
                error = true;
                errorMessage.Append("\n-Images Min Count");
            }

            if (error)
            {
                MessageBox.Show(errorMessage.ToString(), "ViGran", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return !error;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog(this.Owner);

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtImagesPath.Text = fbd.SelectedPath;
                }
            }
        }
    }
}
