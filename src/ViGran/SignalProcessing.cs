﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalProcessing
{
    class SignalProcessingFilter
    {
        List<double> dataToFilt;
        int windowSize;

        public SignalProcessingFilter(int windowSize)
        {
            this.windowSize = windowSize;

            dataToFilt = new List<double>();
        }

        public void feedData(double newSample)
        {  
            dataToFilt.Add(newSample);
        }

        public void restartFilter()
        {
            dataToFilt.Clear();
        }

        private bool funcCriteria(double x, double y)
        {
            return x > y ? true : false;
        }
        public double medianFit()
        {
            double filteredData;

            if (dataToFilt.Count >= windowSize)
            {
                List<double> croppedDataToFilt = dataToFilt.GetRange(dataToFilt.Count - windowSize, windowSize);
                croppedDataToFilt.Sort();

                if (windowSize % 2 == 0)
                {
                    double midleFirst = croppedDataToFilt.ElementAt<double>(windowSize / 2 - 1);
                    double midleSecond = croppedDataToFilt.ElementAt<double>(windowSize / 2);

                    return filteredData = (midleFirst + midleSecond) / 2;
                }
                else
                {
                    return filteredData = croppedDataToFilt.ElementAt<double>(windowSize / 2);
                }
            }
            else
            {
                return filteredData = dataToFilt.Last();
            }
        }
    }
}
